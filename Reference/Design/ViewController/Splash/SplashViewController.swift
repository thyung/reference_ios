//
//  SplashViewController.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/04.
//

import UIKit
import SnapKit


class SplashViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named: "")
        self.view.addSubview(imageView)
        
        imageView.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.view)
            view.centerX.equalTo(self.view)
            view.width.equalTo(162.0)
            view.height.equalTo(60.0)
        }
        
        let label: UILabel = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14.0)
        label.textAlignment = .center
        label.text = "To watch 4D Live, please ensure that you have a 5G smartphone and your mobile carrier offers 5G network service in your area."
        self.view.addSubview(label)
        
        label.snp.makeConstraints { view -> Void in
            view.left.equalTo(self.view).offset(20.0)
            view.right.equalTo(self.view).offset(-20.0)
            view.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-46.0)
            view.height.equalTo(51.0)
        }
    }

}
