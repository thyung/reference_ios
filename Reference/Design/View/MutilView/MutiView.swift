//
//  MutiView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/17.
//

import RxCocoa
import RxSwift

class MultiView: BaseView {
    
    lazy var player01 = PlayerView()
    lazy var player02 = PlayerView()
    lazy var player03 = PlayerView()
    lazy var player04 = PlayerView()
    
    lazy var horizonLineView = UIView()
    lazy var verticalLineView = UIView()
    
    var divide: Int = 0
    var isPDControl = false
    
    public init(divide: Int = 1) {
        super.init(frame: .zero)
        self.divide = divide
        initVariable()
        setupLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
    }
    
    override func initVariable() {
        
    }
    
    override func setupLayout() {
        switch divide {
        case 0, 1:
            defaultForm()
            break
        case 2:
            defaultForm()
            break
        case 3:
            threeDividedForm()
            break
        case 4:
            fourDividedForm()
            break
        default:
            defaultForm()
            break
        }
    }
    
    
    func defaultForm() {
        self.addSubview(player01)
        player01.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.top.bottom.left.right.equalTo(weakSelf)
            }
        }
    }
    
    func twoDividedForm() {
//        self.addSubview(player01)
//        self.addSubview(player02)
//        verticalLineView.backgroundColor = .black
//        self.addSubview(verticalLineView)
//        
//        player01.backgroundColor = .gray
//        player02.backgroundColor = .darkGray
    }
    
    func threeDividedForm() {
        player01.backgroundColor = .gray
        player02.backgroundColor = .darkGray
        player03.backgroundColor = .lightGray
        self.addSubview(player01)
        self.addSubview(player02)
        self.addSubview(player03)
        
        horizonLineView.backgroundColor = .black
        verticalLineView.backgroundColor = .black
        self.addSubview(horizonLineView)
        self.addSubview(verticalLineView)
                
        player02.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (9.0/16.0) * 0.5
                let width = UIScreen.width * 0.5
                view.top.left.equalTo(weakSelf)
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        player03.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (9.0/16.0) * 0.5
                let width = UIScreen.width * 0.5
                view.left.bottom.equalTo(weakSelf)
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        player01.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.top.bottom.equalTo(weakSelf)
                let offset = UIScreen.width * 0.25
                view.left.equalTo(offset)
                view.right.equalTo(offset)
            }
        }
        horizonLineView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let width = UIScreen.width * 0.5
                view.height.equalTo(2.0)
                view.width.equalTo(width)
                view.left.equalTo(weakSelf)
                view.centerY.equalTo(weakSelf)
            }
        }
        verticalLineView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.width.equalTo(2.0)
                view.top.bottom.equalTo(weakSelf)
                view.centerX.equalTo(weakSelf)
            }
        }
    }
    
    func fourDividedForm() {
        player01.backgroundColor = .gray
        player02.backgroundColor = .darkGray
        player03.backgroundColor = .lightGray
        player03.backgroundColor = .white
        self.addSubview(player01)
        self.addSubview(player02)
        self.addSubview(player03)
        self.addSubview(player04)
        
        horizonLineView.backgroundColor = .black
        verticalLineView.backgroundColor = .black
        self.addSubview(horizonLineView)
        self.addSubview(verticalLineView)
        
        player01.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (9.0/16.0) * 0.5
                let width = UIScreen.width * 0.5
                view.top.left.equalTo(weakSelf)
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        player02.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (9.0/16.0) * 0.5
                let width = UIScreen.width * 0.5
                view.left.bottom.equalTo(weakSelf)
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        player03.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (9.0/16.0) * 0.5
                let width = UIScreen.width * 0.5
                view.left.bottom.equalTo(weakSelf)
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        player04.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (9.0/16.0) * 0.5
                let width = UIScreen.width * 0.5
                view.right.bottom.equalTo(weakSelf)
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        
        horizonLineView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.height.equalTo(2.0)
                view.left.right.equalTo(weakSelf)
                view.centerY.equalTo(weakSelf)
            }
        }
        verticalLineView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.width.equalTo(2.0)
                view.top.bottom.equalTo(weakSelf)
                view.centerX.equalTo(weakSelf)
            }
        }
    }
    
    func changePDStreamChannel(_ channel: Int) {
        if isPDControl == true,
           (player01.fdPlayer.state == FD_STATE_PLAY || player01.fdPlayer.state == FD_STATE_PAUSE) {
            player01.fdPlayer.setTargetChannel(channel)
        }
    }
}
