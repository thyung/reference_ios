//
//  PlayerTitleView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/23.
//

import SnapKit

class LandscapeTitleView: BaseView {
    
    private var ivTop = UIImageView()
    private var ivBottom = UIImageView()
    var lbTitle = UILabel()
    var lbDscript = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
    }
    
    override func initVariable() {
        self.backgroundColor = .clear
        self.isUserInteractionEnabled = false
        
        if let image = UIImage(named: "img_portrait_player_shadow") {
            ivTop.image = image
            ivTop.backgroundColor = .clear
            self.addSubview(ivTop)
        }
        if let image = UIImage(named: "img_landscape_player_shadow_bottom") {
            ivBottom.image = image
            ivBottom.backgroundColor = .clear
            self.addSubview(ivBottom)
        }
        
        lbTitle.font = UIFont.systemFont(ofSize: 20.0)
        lbTitle.textColor = .white
        lbTitle.text = "Gymnasitc Paralle Bars"
        lbTitle.setLayerBorder()
        self.addSubview(lbTitle)

        lbDscript.font = UIFont.systemFont(ofSize: 15.0)
        lbDscript.textColor = .lightGray
        lbDscript.text = "JUNE 14"
        lbDscript.setLayerBorder()
        self.addSubview(lbDscript)
    }
    
    override func setupLayout() {
        ivTop.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (168.0/390.0)
                view.left.top.right.equalTo(weakSelf)
                view.height.equalTo(height)
            }
        }
        ivBottom.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                let height = UIScreen.width * (143.0/390.0)
                view.left.bottom.right.equalTo(weakSelf)
                view.height.equalTo(height)
            }
        }
        lbTitle.snp.makeConstraints { [weak self] label in
            if let weakSelf = self {
                label.left.equalTo(weakSelf).offset(20)
                label.right.equalTo(weakSelf).offset(-60)
                label.top.equalTo(weakSelf).offset(16)
            }
        }
        lbDscript.snp.makeConstraints { [weak self] label in
            if let weakSelf = self {
                label.left.equalTo(weakSelf).offset(20)
                label.right.equalTo(weakSelf).offset(-60)
                label.top.equalTo(lbTitle.snp.bottom).offset(8)
            }
        }
    }    
}
