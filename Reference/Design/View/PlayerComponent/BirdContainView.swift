//
//  LiveSwitchView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/17.
//

import UIKit
import RxCocoa
import RxSwift

enum SwitchViewType {
  case bird
  case multi
}

class BirdContainhHeaderView: BaseView {
    var btBird = UIButton()
    var btMulti = UIButton()
    var btRefresh = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
    }
    
    override func initVariable() {        
        btBird.backgroundColor = .clear
        if let image = UIImage(named: "menu_focus") {
            btBird.setBackgroundImage(image, for: .selected)
        }
        btBird.setTitle("BIRDVIEW", for: .normal)
        btBird.titleLabel?.font = UIFont.PoppinsBold(ofSize: 16.0)
        btBird.setTitleColor(.black, for: .selected)
        btBird.setTitleColor(UIColor.colorRGB(178.0, 178.0, 178.0), for: .normal)
        self.addSubview(btBird)
        btBird.setLayerBorder()
        
        btMulti.backgroundColor = .clear
        if let image = UIImage(named: "menu_focus") {
            btMulti.setBackgroundImage(image, for: .selected)
        }
        btMulti.setTitle("MULTIVIEW", for: .normal)
        btMulti.titleLabel?.font = UIFont.PoppinsBold(ofSize: 16.0)
        btMulti.setTitleColor(.black, for: .selected)
        btMulti.setTitleColor(UIColor.colorRGB(178.0, 178.0, 178.0), for: .normal)
        self.addSubview(btMulti)
        btMulti.setLayerBorder()
        
        if let image = UIImage(named: "icon_refresh_highlights") {
            btRefresh.setBackgroundImage(image, for: .normal)
        }
        self.addSubview(btRefresh)
        btRefresh.setLayerBorder()
    }
    
    override func setupLayout() {
        //16, 22
        btBird.snp.makeConstraints { btn in
            btn.left.equalTo(self).offset(16.0)
            btn.centerY.equalTo(self)
        }
        btMulti.snp.makeConstraints { btn in
            btn.left.equalTo(btBird.snp.right).offset(22.0)
            btn.centerY.equalTo(self)
        }
        btRefresh.snp.makeConstraints { btn in
            btn.right.equalTo(self).offset(-9)
            btn.centerY.equalTo(self)
        }
    }
    
    func hideBirdButton() {
        btBird.isHidden = true
        btMulti.isUserInteractionEnabled = false
        btMulti.isSelected = true
        btMulti.snp.remakeConstraints { btn in
            btn.left.equalTo(self).offset(16.0)
            btn.centerY.equalTo(self)
        }
    }
}

class BirdContainView: BaseView {
    var header = BirdContainhHeaderView()    
    var type: ContentBirdType = .normal
    lazy var birdView: BirdView = {
        var view: BirdView
        switch self.type {
        case .normal:
            view = BirdView()
          break
        case .baseball:
            view = BaseballBirdView()
            break
        case .ufc:
            view = UFCBirdView()
            break
        case .basket:
            view = BasketBirdView()
            break
        case .soccer:
            view = SoccerBirdView()
            break
        }
        view.backgroundColor = .black
        self.addSubview(view)
        view.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.top.equalTo(header.snp.bottom).offset(0)
                view.bottom.left.right.equalTo(weakSelf)
            }
        }
        return view
    }()
    
    lazy var multiView: MultiView = {
        var view = MultiView(divide: 3)
        view.backgroundColor = .darkGray
        view.setupLayout()
        self.addSubview(view)
        view.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.top.equalTo(header.snp.bottom)
                view.bottom.left.right.equalTo(weakSelf)
            }
        }
        return view
    }()
    
    private let disposeBag: DisposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
        setRXFunction()
    }
    
    public init(type: ContentBirdType) {
        super.init(frame: .zero)
        self.type = type
        initVariable()
        setupLayout()
        setRXFunction()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
        setRXFunction()
    }
        
    override func initVariable() {
        self.addSubview(header)
        header.setLayerBorder()
        header.btBird.isSelected = true
        self.addSubview(birdView)
        if type == .baseball {
            header.hideBirdButton()
        }
    }
    
    override func setupLayout() {
        header.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.top.left.right.equalTo(weakSelf)
                view.height.equalTo(56)
            }
        }
    }
    
    func setRXFunction() {
        
        let buttons = [header.btBird, header.btMulti].map{ $0! }
        let selectedButton = Observable.from(
            buttons.map { button in button.rx.tap.map { button } }
        ).merge()
        buttons.reduce(Disposables.create()) {disposable, button in
            let subscription = selectedButton.map { $0 == button }
            .bind(to: button.rx.isSelected)
            return Disposables.create(disposable, subscription)
        }
        .disposed(by: self.disposeBag)
        Observable.merge(
            header.btBird.rx.tap.map { _ in SwitchViewType.bird },
            header.btMulti.rx.tap.map { _ in SwitchViewType.multi }
        ).subscribe(onNext: { [weak self] in
          switch $0 {
          case .bird:
            self?.birdView.fadeIn()
            self?.multiView.fadeOut()
            Log.message(to: "Selected bird.")
            break
          case .multi:
            self?.birdView.fadeOut()
            self?.multiView.fadeIn()
            Log.message(to: "Selected mulit.")
            break
          }
        }).disposed(by: disposeBag)
        
        header.btRefresh.rx.tap.bind { [weak self] in
            self?.reOpenStream()
        }.disposed(by: self.disposeBag)
    }
        
    func reOpenStream() {
        if header.btBird.isSelected == true {
            birdView.playerView.streamReopen()
            Log.message(to: "\(#function) bird reopen")
        }
        if header.btMulti.isSelected == true {
            Log.message(to: "\(#function) multi reopen")
        }
    }
}
