//
//  MarkerView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/18.
//

import UIKit

class MarkerView: BaseView {
    
    var imageView = UIImageView()
    var scale: CGFloat = 1.0
    var positionLenght: CGFloat = 0.0
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
    }
    
    override func initVariable() {
        self.isUserInteractionEnabled = false
        if let image = UIImage(named: "icon_4dlive_portrait") {
            imageView.image = image
        }
        self.addSubview(imageView)
    }
    
    /*
    override func draw(_ rect: CGRect) {
        
        let xPoint = self.frame.width/2;
        let yPoint = self.frame.height/2
        
        let xPath = UIBezierPath()
        xPath.move(to: CGPoint(x: xPoint - (10 * scale), y: yPoint - (5 * scale)))
        xPath.addLine(to: CGPoint(x: xPoint + (10 * scale), y: yPoint + (5 * scale)))
        xPath.move(to: CGPoint(x: xPoint - (10 * scale), y: yPoint + (5 * scale)))
        xPath.addLine(to: CGPoint(x: xPoint + (10 * scale), y: yPoint - (5 * scale)))
        xPath.lineWidth = scale
        UIColor.red.setStroke()
        xPath.stroke()
    }
    */
    private func setTargetPosition(_ length: CGFloat) {
        var rect: CGRect = imageView.frame
        if length > 0.0 {
            let ptY = (self.frame.height - rect.height) / 2
            rect.origin.y = ptY + length
        } else {
            //let ptX = (self.frame.width - rect.width) / 2
            let ptY = (self.frame.height - rect.height) / 2
            rect.origin.y = ptY
        }
        imageView.frame = rect
    }
            
    func setPostionLengthWithScale(_ scale: CGFloat, length: CGFloat) {
        self.scale = scale
        setTargetPosition(length)
    }
}
