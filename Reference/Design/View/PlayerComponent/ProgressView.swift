/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class FDThumbSlider : UISlider {
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        let unadjustedRect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
        //unadjustedRect.size.width = 31.0
        //unadjustedRect.size.height = 31.0
        let offset: CGFloat = unadjustedRect.size.width / 2.0
        let minOffset = -offset
        let maxOffset = offset
        let value = minOffset + (maxOffset - minOffset) * CGFloat(value / (self.maximumValue - self.minimumValue))
        var origin = unadjustedRect.origin
        origin.x += value

        return CGRect(origin: origin, size: unadjustedRect.size)
    }
}

@objc protocol ProgressViewDelegate {
    @objc optional func sliderMoveBegin(_ slider: UISlider)
    @objc optional func sliderMoveEnd(_ slider: UISlider, time: Int, base: Float)
    @objc optional func progressFinish(_ slider: UISlider)
}

class ProgressView: BaseView {
    
    weak var delegate: ProgressViewDelegate?

    var slider: FDThumbSlider!
    var ivBackground = UIImageView()
    var btPause = UIButton()
    var btLandscape = UIButton()
    var lbProgress = UILabel()
    /* live */
    var btNow = UIButton()
    var ivLive = UIImageView()
    var btSwitch: SwitchButton?
    /* vod */
    var lbDuration = UILabel()
    lazy var btRepeat: UIButton = {
        let button = UIButton()
        let normal = UIImage(named: "icon_loop_on")
        let selected = UIImage(named: "icon_loop_off")
        button.backgroundColor = UIColor.clear
        button.setBackgroundImage(normal, for: UIControl.State.normal)
        button.setBackgroundImage(selected, for: UIControl.State.selected)
        self.addSubview(button)
        button.snp.makeConstraints { btn in
            btn.right.equalTo(self).offset(-14)
            btn.top.equalTo(self).offset(0)
            btn.width.height.equalTo(40)
        }
        return button
    }()
        
    private var min: Int64 = 0
    private var max: Int64 = 0
    var atStartTime: Int64 = 0
    var isTouch: Bool = false
    
    @Atomic private var _duration: Int64 = 0
    var duration: Int64 {
        get { return _duration }
        set(newValue){
            _duration = newValue
            DispatchQueue.main.async {
                let time: String = self.convertTime(newValue) as String
                self.lbDuration.text = "/ " + time
            }
        }
    }
    
    private var _isLive: Bool = false
    var isLive: Bool {
        get {
            return _isLive
        }
        set(newValue) {
            _isLive = newValue
            if newValue == true {
                /* live */
                slider.value = 1.0
                //btNow.isHidden = false
                ivLive.isHidden = false
                lbDuration.isHidden = true
                lbProgress.isHidden = true
            } else {
                /* vod */
                ivLive.isHidden = true
                btNow.isHidden = true
                if let btn = btSwitch {btn.isHidden = true}
                lbProgress.isHidden = false
                lbDuration.isHidden = false
                //btRepeat.isHidden = false
            }
        }
    }
        
    // MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
    }
    
    override func initVariable() {
        if let image = UIImage(named: "imgPortraitPlayerShadowBtm") {
            ivBackground.image = image
        }
        self.insertSubview(ivBackground, at: 0)
        
        if let image = UIImage(named: "icon_4dlive_portrait") {
            ivLive = UIImageView.init(image: image)
        }
        self.addSubview(ivLive)
        
        lbProgress.font = UIFont.systemFont(ofSize: 12.0)
        lbProgress.text = "00:00"
        lbProgress.textColor = UIColor.white
        lbProgress.isHidden = true
        self.addSubview(lbProgress)
        
        /* duration time - only vod */
        lbDuration.font = UIFont.systemFont(ofSize: 12.0)
        lbDuration.text = "/ 00:00"
        lbDuration.textColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1)
        lbDuration.isHidden = true
        self.addSubview(lbDuration)
        
        /* nowToplay - only live */
        if let normal = UIImage(named: "icon_now_portrait") {
            btNow.setBackgroundImage(normal, for: UIControl.State.normal)
        }
        btNow.backgroundColor = .clear
        btNow.addTarget(self, action: #selector(nowButttonTouchUpInside(_:)), for: UIControl.Event.touchUpInside)
        btNow.isHidden = true
        self.addSubview(btNow)
        
        /* slider */
        slider = FDThumbSlider.init()
        slider.minimumTrackTintColor = #colorLiteral(red: 0.5137254902, green: 0.07843137255, blue: 0.07843137255, alpha: 1)
        slider.maximumTrackTintColor = UIColor.white
        slider.minimumValue = 0.0
        slider.maximumValue = 1.0
        slider.thumbTintColor = UIColor.clear
        slider.isContinuous = true
        slider.addTarget(self, action: #selector(sliderTouchBegan(_:)), for: UIControl.Event.touchDown)
        slider.addTarget(self, action: #selector(sliderTouchEnd(_:)), for: UIControl.Event.touchUpInside)
        slider.addTarget(self, action: #selector(sliderTouchDrag(_:)), for: UIControl.Event.touchDragInside)
        self.addSubview(slider)
        
        /* play, pause */
        if let normal = UIImage(named: "icon_pause_landscape"),
           let selected = UIImage(named: "icon_play_portrait") {
            btPause.setBackgroundImage(normal, for: UIControl.State.normal)
            btPause.setBackgroundImage(selected, for: UIControl.State.selected)
        }
        btPause.backgroundColor = UIColor.clear
        btPause.addTarget(self, action: #selector(playButtonTouchUpInside(_:)), for: UIControl.Event.touchUpInside)
        self.addSubview(btPause)
        
        /* landscape, portrait */
        if let normal = UIImage(named: "icon_maximize"),
           let selected = UIImage(named: "icon_minimize_maximize_landscape") {
            btLandscape.setBackgroundImage(normal, for: UIControl.State.normal)
            btLandscape.setBackgroundImage(selected, for: UIControl.State.selected)
        }
        btLandscape.backgroundColor = UIColor.clear
        btLandscape.addTarget(self,
                              action: #selector(rotateButtonTouchUpInside(_:)),
                              for: UIControl.Event.touchUpInside)
        self.addSubview(btLandscape)
        
        /* Auto traking switch */
        btSwitch = SwitchButton.init(frame: CGRect.init(x: 0, y: 0, width: 46, height: 26))
        self.addSubview(btSwitch!)
        btSwitch?.isHidden = true
    }
    
    override func setupLayout() {
        ivBackground.snp.makeConstraints { view in
            view.top.bottom.left.right.equalTo(self).offset(0)
        }
        lbProgress.snp.makeConstraints { [weak self] label in
            guard let self = self else { return }
            label.left.equalTo(self).offset(16)
            label.top.equalTo(self).offset(12)
        }
        ivLive.snp.makeConstraints { [weak self] view in
            guard let self = self else { return }
            view.left.equalTo(self).offset(16)
            view.top.equalTo(self).offset(4)
        }
        lbDuration.snp.makeConstraints { [weak self] label in
            guard let self = self else { return }
            label.left.equalTo(lbProgress.snp.right).offset(4)
            label.top.equalTo(self).offset(12)
        }
        btNow.snp.makeConstraints { btn in
            btn.left.equalTo(lbProgress.snp.right).offset(4)
            btn.centerYWithinMargins.equalTo(lbProgress)
        }
        slider.snp.makeConstraints { [weak self] ctrl in
            guard let self = self else { return }
            ctrl.bottom.equalTo(self).offset(-12)
            ctrl.left.equalTo(self).offset(62)
            ctrl.right.equalTo(self).offset(-62)
        }
        btPause.snp.makeConstraints { [weak self] btn in
            guard let self = self else { return }
            btn.left.equalTo(self).offset(14)
            btn.centerYWithinMargins.equalTo(slider)
            btn.width.height.equalTo(40)
        }
        btLandscape.snp.makeConstraints { [weak self] btn in
            guard let self = self else { return }
            btn.right.equalTo(self).offset(-14)
            btn.centerYWithinMargins.equalTo(slider)
            btn.width.height.equalTo(40)
        }
        btSwitch?.snp.makeConstraints { [weak self] btn in
            guard let self = self else { return }
            btn.right.equalTo(self).offset(-14)
            btn.top.equalTo(self).offset(0)
            btn.width.equalTo(46)
            btn.height.equalTo(26)
        }
    }

    // MARK: - Private method
    @discardableResult
    private func convertTime(_ value: Int64, label: UILabel? = nil) -> NSMutableString {
        let sign = Int(value).signum()
        let seconds = abs(value)/1000
        var ms = NSMutableString.init(capacity: 8)
        let h = seconds / 3600
        let m = seconds / 60 % 60
        let s = seconds % 60

        if seconds < 0 {
            ms = "∞"
        } else {
            if sign < 0 { ms = "- " }
            if h > 0 { ms.appendFormat("%d:", h) }
            if m < 10 { ms.append("0") }
            ms.appendFormat("%d:", m)
            if s < 10 { ms.append("0") }
            ms.appendFormat("%d", s)
        }
        DispatchQueue.main.async { //[weak self] in
            if let lb = label {
                lb.text = ms as String
            }
        }
        
        return ms
    }
    
    @objc func sliderTouchBegan(_ sender: UISlider) {
        isTouch = true
        if let target = delegate, target.sliderMoveBegin != nil {
            target.sliderMoveBegin?(slider)
        }
        /* add noti */
    }
    
    @objc func sliderTouchDrag(_ sender: UISlider) {
        //print("\(#function) value: \(sender.value)")
        if isLive == true {
            let time = (1 - slider.value) * Float(_duration)
            convertTime(Int64(time * -1), label: lbProgress)
        } else {
            let value = sender.value * Float(_duration)
            convertTime(Int64(value), label: lbProgress)
        }
    }
    
    @objc func sliderTouchEnd(_ sender: UISlider) {
        isTouch = false
        var value: Float = 0.0
        if isLive == true {
            let time = (1 - slider.value) * Float(_duration)
            convertTime(Int64(time * -1), label: lbProgress)
            lbProgress.isHidden = (time == 0.0)
            value = Float(max) - time
        } else {
            value = slider.value * Float(_duration)
        }

        Log.message(to: "\(#function) duration: \(_duration) value: \(Int(value)) time: \(String(describing: time))")
        if let target = delegate, target.sliderMoveEnd != nil {
            target.sliderMoveEnd!(slider, time: Int(value), base: Float(_duration))
        }
    }
    
    // MARK: - Button Action
    @objc func playButtonTouchUpInside(_ sender: Any?) {
        if let btn = sender as? UIButton {
            btn.isSelected = !btn.isSelected
            print("\(#function)>>>>>> \(btn.isSelected)")
        }
    }
    
    @objc func rotateButtonTouchUpInside(_ sender: Any?) {
        if let btn = sender as? UIButton {
            btn.isSelected = !btn.isSelected
            /* add noti */
        }
    }
    
    @objc func nowButttonTouchUpInside(_ sender: Any) {
        setTimeShiftMode(false)
        btPause.isSelected = false
    }
    
    // MARK: - Public method
    func setLiveDuration(_ time: Int64) {
        let date = NSDate()
        let seconds = date.timeIntervalSince1970
        // TODO: 테스트중 주석 제거할 것
        //_duration = (Int64(seconds) - atStartTime) * 1000
        _duration = Int64(60 * 1000)
        max = time
        min = time - _duration
        Log.message(to: "\(#function) duration: \(duration) min: \(min) max: \(max)")
        if min < 0 { min = 0 }
    }
    
    func updatePlayTime(_ time: Int64) {
        if isTouch == true { return }
        if time > 0 {
            if _isLive == true {
                let ret = Float(max) - Float(time)
                if ret <= 0 {
                    slider.value = 1
                    lbProgress.text = "00:00"
                    if let target = delegate, target.progressFinish != nil {
                        target.progressFinish!(slider)
                    }
                } else {
                    let temp = time - min
                    let value = Float(temp) / Float(_duration)
                    slider.value = value
                    let aTime = time - max
                    convertTime(Int64(aTime), label: lbProgress)
                }
            } else {
                convertTime(time, label: lbProgress)
                let ret = Float(time) / Float(_duration)
                slider.value = ret
            }
        } else {
            slider.value = 0
            lbProgress.text = "00:00"
        }
    }
        
    func setTimeShiftMode(_ isTimeShift: Bool) {    
        btNow.isHidden = !isTimeShift
        ivLive.isHidden = isTimeShift
        lbProgress.isHidden = !isTimeShift
        if isTimeShift != true {
            slider.value = 1
            _duration = 0
            convertTime(0, label: lbProgress)
        } else {
            //beginTimer()
        }
    }
    
    func setPauseMode(_ isPause: Bool) {
        
    }
    
    func onMutilMode(_ isMuti: Bool) {
        ivBackground.isHidden = isMuti
        slider.isHidden = isMuti
        lbDuration.isHidden = isMuti
        btPause.isHidden = isMuti
        lbProgress.isHidden = isMuti
        //btNow?.isHidden = isMuti
    }
}
