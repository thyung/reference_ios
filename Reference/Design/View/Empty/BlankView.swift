//
//  BlankView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/11.
//

import UIKit

enum BlankType: String {
    case notFound = "notFound"
    case networkErr = "networkErr"
}

class BlankView: UIView {
    
    lazy var notice: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    lazy var btRetry: UIButton = {
        let button = UIButton()
        button.setTitle("RETRY", for: .normal)
        button.setTitleColor(UIColor.colorRGB(26.0, 146.0, 253.0), for: .normal)
        button.layer.borderWidth = 2.0
        button.layer.borderColor = UIColor.colorRGB(26.0, 146.0, 253.0).cgColor
        button.layer.cornerRadius = 4.0
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    public init(type: BlankType) {
        super.init(frame: .zero)
        self.backgroundColor = UIColor.colorRGB(238, 238, 238)
        draw(type: type)
    }
}

extension BlankView {
    
    public func draw(type: BlankType) {

        self.addSubview(notice)
        notice.setLayerBorder()
        
        notice.snp.makeConstraints { [weak self] view -> Void in
            guard let self = self else { return }
            view.centerX.equalTo(self)
            view.centerY.equalTo(self).offset(-50.0)
            view.width.equalTo(UIScreen.width)
            let height = type == .notFound ? 124.0: 215.0
            view.height.equalTo(height)
        }

        setContent(type: type)
        
    }
    
    func setContent(type: BlankType) {
        
        let imageView = UIImageView()
        imageView.image = type == .notFound ? UIImage(named: "imgNoResult") : UIImage(named: "imgNetworkError")
        notice.addSubview(imageView)
        imageView.setLayerBorder()
        
        imageView.snp.makeConstraints { view -> Void in
            view.top.equalTo(notice)
            view.width.equalTo(82.0)
            view.centerX.equalTo(notice)
        }
        
        let message = UILabel()
        message.font = UIFont.OpenSansSemiBold(ofSize: 16.0)
        message.textAlignment = .center
        message.textColor = .black
        message.numberOfLines = 2
        let msg = type == .notFound ? "No results found" : "A network error has occurred. \n plase try again."
        message.text = msg
        notice.addSubview(message)
        
        message.setLayerBorder()
        message.snp.makeConstraints { view -> Void in
            //view.height.equalTo(22.0)
            view.top.equalTo(imageView.snp.bottom).offset(20.0)
            view.left.right.equalTo(notice)
        }
        
        if type == .networkErr {
            notice.addSubview(btRetry)
            btRetry.snp.makeConstraints { btn in
                btn.centerX.equalTo(notice)
                btn.width.equalTo(78.0)
                btn.height.equalTo(35.0)
                btn.bottom.equalTo(notice)
            }
        }
    }
    
}

