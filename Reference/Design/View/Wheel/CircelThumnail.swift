//
//  CircelThumnail.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/23.
//

import UIKit

enum CGGradientPosition: Int {
    case vertical = 1
    case horizontal = 2
}

enum CDCircleThumbsSeparator: Int {
    case none = 0
    case basic
}

func degreeToRadian(_ x: CGFloat) -> CGFloat {
    return (CGFloat(Double.pi) * x) / 180.0
}

func drawLinearGradient(_ context: CGContext,
                        path: CGPath,
                        colors: CFArray,
                        position: CGGradientPosition,
                        locations:UnsafeMutablePointer<CGFloat>,
                        rect: CGRect) {
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    let gradient: CGGradient = CGGradient.init(colorsSpace: colorSpace, colors: colors, locations: locations)!
    let startPoint: CGPoint
    let endPoint: CGPoint
    
    switch position {
    case .vertical:
        startPoint = CGPoint.init(x: rect.midX, y: rect.minY)
        endPoint = CGPoint.init(x: rect.midX, y: rect.maxY)
        break
    case .horizontal:
        startPoint = CGPoint.init(x: rect.minX, y: rect.midY)
        endPoint = CGPoint.init(x: rect.midX, y: rect.maxY)
        break
     }
    
    context.saveGState()
    context.addPath(path)
    context.clip()
    context.drawLinearGradient(gradient,
                               start: startPoint,
                               end: endPoint,
                               options:[CGGradientDrawingOptions(rawValue: 0)])
    context.restoreGState()
}

class CircleThumnail: UIView {
    
    var sRadius: CGFloat = 0
    var lRadius: CGFloat = 0
    var numberOfSegment: Int = 0
    
    var arcColor: UIColor?
    var arc: UIBezierPath?
    var centerPoint: CGPoint?
    var gradientColors: [CGColor]?
    var gradientFill: Bool = true
    var colorsLocations: [CGFloat]?
    var separateStyle: CDCircleThumbsSeparator?
    var separatorColor: UIColor?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isOpaque = false
        self.arcColor = UIColor.clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let color = self.arcColor {
            color.setStroke()
            color.setFill()
        }
        
        let temp = (360 / numberOfSegment) / 2
        let clockwiseStart = degreeToRadian(CGFloat(270 - temp))
        let clockwiseEnd = degreeToRadian(CGFloat(270 + temp))
        
        let center = CGPoint.init(x: rect.midX, y: lRadius)
        self.arc = UIBezierPath(arcCenter: center,
                                radius: lRadius,
                                startAngle: clockwiseStart,
                                endAngle: clockwiseEnd,
                                clockwise: true)
        let beginPoint = arc?.currentPoint
        
        arc?.addArc(withCenter: center,
                    radius: sRadius,
                    startAngle: clockwiseStart,
                    endAngle: clockwiseEnd,
                    clockwise: false)
        let endPoint = arc?.currentPoint
        arc?.close()
        let context = UIGraphicsGetCurrentContext()!
        
        if gradientFill == true {
            arc?.fill()
        } else {
            ///var la: [CGFloat]?
            let colorCount = gradientColors?.count ?? 0
            let locations = UnsafeMutablePointer<CGFloat>.allocate(capacity: colorCount)
            let path = arc?.cgPath;
            
            if colorCount == 2 {
                locations[0] = 0.0
                locations[1] = 1.0
            } else {
                if colorsLocations == nil {
                    var index = 0
                    for _ in 0..<colorCount {
                        let fi = colorCount - 1
                        let point: CGFloat = 1.0 / CGFloat(fi)
                        locations[index] = point * CGFloat(index)
                        index = index + 1
                    }
                } else {
                    for idx in 0..<colorsLocations!.count {
                        locations[idx] = colorsLocations![idx]
                    }
                }
            }
            drawLinearGradient(context,
                               path: path!,
                               colors: gradientColors! as CFArray,
                               position: CGGradientPosition.horizontal,
                               locations: locations,
                               rect: rect)
            locations.deallocate()
        }
        
        if separateStyle != CDCircleThumbsSeparator.none {
            let line = UIBezierPath()
            if separateStyle == CDCircleThumbsSeparator.basic {
                line.lineWidth = 1.0
            }
            line.move(to: beginPoint!)
            line.addLine(to: CGPoint.init(x: beginPoint!.x - endPoint!.x, y: endPoint!.y))
            if let color = self.separatorColor {
                color.setStroke()
                color.setFill()
            } else {
                UIColor.lightGray.setStroke()
                UIColor.lightGray.setFill()
            }
            line.stroke(with: CGBlendMode.copy, alpha: 1.0)
            line.fill(with: CGBlendMode.copy, alpha: 1.0)
        }
    }
}
