//
//  RotaryWheel.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/23.
//

import UIKit
import RxCocoa
import RxSwift

let __rotatinDegreee: CGFloat = 90.0

@objc protocol RotaryWheelDelegate: AnyObject {
    @objc optional func wheelBeginTracking(_ wheel: RotaryWheel)
    @objc optional func wheelValueChanged(_ wheel: RotaryWheel, index: Int, direct: Int)
}

class Clove: NSObject {
    var min: CGFloat = 0.0
    var max: CGFloat = 0.0
    var mid: CGFloat = 0.0
    var value: Int = 0 // rename index
    
    override init() {
        super.init()
    }
    
    init(min: CGFloat, max: CGFloat, mid: CGFloat, index: Int) {
        super.init()
        self.min = min
        self.max = max
        self.mid = mid
        self.value = index
    }
}

class RotaryWheel: UIControl {
    
    var vContain = UIView()
    
    var direct: Int = 0  // 1 시계 -1 반시계
    private var ringWidth: CGFloat = 80.0 //thickness
    private var minAlpah: CGFloat = 0.6
    private var thumbs: Array<CircleThumnail> = []
    private var cloves: Array<Clove> = []
    private var delta: Float = 0.0
    private var startTransform: CGAffineTransform?
    //private var firstTransform: CGAffineTransform?
    //private var lastTransform: CGAffineTransform?
    
    private(set) public var current: Int = 0
    private var buttons: Array<UIButton> = []
    weak var delegate: RotaryWheelDelegate?
    var section = 8
    
    var index = 0
    var rxIndex = PublishSubject<Int>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
        setThumbItem()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
        setThumbItem()
    }
    
    private func initVariable() {
        //vContain.isUserInteractionEnabled = false
        startTransform = vContain.transform
        self.addSubview(vContain)
    }
    
    private func setupLayout() {
        vContain.snp.makeConstraints { view in
            view.left.right.top.bottom.equalTo(self)
        }
    }
    
    private func setThumbItem() {
        let h = self.height > 0 ? Int(self.height) : 200 /* xxx fix dynamic */
        let w = self.width > 0 ? Int(self.width) : 200
        let rect = CGRect(x: 0, y: 0, width: h - (2 * Int(ringWidth)), height: w - (2 * Int(ringWidth)))
        let shortRadius = rect.height / 2
        let longRadius = CGFloat(h / 2)
        
        var frame: CGRect = CGRect.zero
        let startValue: CGFloat = 270.0 - (360/CGFloat(section)) / 2
        let endValue: CGFloat = 270.0 + (360/CGFloat(section)) / 2
        let startAngle = degreeToRadian(startValue)
        let endAngle = degreeToRadian(endValue)
        let point = CGPoint.init(x: longRadius, y: longRadius)
        let bigArc = UIBezierPath.init(arcCenter: point,
                                       radius: longRadius,
                                       startAngle: startAngle,
                                       endAngle: endAngle,
                                       clockwise: true)
        let smallArc = UIBezierPath.init(arcCenter: point,
                                        radius: shortRadius,
                                        startAngle: shortRadius,
                                        endAngle: endAngle,
                                        clockwise: true)
        if endValue - startValue <= 180  {
            let width = bigArc.bounds.width
            let height = smallArc.currentPoint.y
            frame = CGRect.init(x: 0, y: 0, width: width, height: height)
        }
        if endValue - startValue > 269 {
            frame = CGRect.init(x: 0, y: 0, width: bigArc.bounds.width, height: bigArc.bounds.size.height);
        }
                
        let temp = (longRadius - shortRadius) / 2.0
        let retX: CGFloat = 0.5
        let retY: CGFloat = temp / frame.height
                
        for i in 0..<section {
            let thumb: CircleThumnail = CircleThumnail.init(frame: frame)
            thumb.sRadius = shortRadius
            thumb.lRadius = longRadius
            thumb.numberOfSegment = section
            thumb.layer.anchorPoint = CGPoint.init(x: retX, y: retY)
            thumb.centerPoint = CGPoint.init(x: frame.midX, y: temp)
            thumb.gradientColors = [UIColor.white.cgColor, UIColor.gray.cgColor]
            thumb.separateStyle = CDCircleThumbsSeparator.none
            thumb.tag = i
            
            if i == 0 {
                let image = UIImage(named: "icon_camera")
                let imageView = UIImageView.init(image: image)
                let rect = CGRect.init(x: (frame.width - 36) / 2, y: 0, width: 36, height: 36)
                imageView.frame = rect
                thumb.addSubview(imageView)
            } else {
                let aRect = CGRect.init(x: (frame.width - 36) / 2, y: 0, width: 36, height: 36)
                let button = UIButton(frame: aRect)
                button.backgroundColor = .clear
                button.addTarget(self, action: #selector(buttonTouchUpInside(sender:)), for: .touchUpInside)
                button.tag = i
                thumb.addSubview(button)
                buttons.append(button)
                //button.setLayerBorder()
            }
            thumbs.append(thumb)
        }
    }
        
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawClove()
        
        let width = rect.height - (2 * ringWidth)
        let height = rect.width - (2 * ringWidth)
        let px = rect.width / 2 - width / 2
        let py = rect.height / 2 - height / 2
        let aRect = CGRect.init(x: px, y: py, width: width, height: height)
        
        var degree: CGFloat = 292.5// / CGFloat(section)
        let totalRotation = 360.0 / CGFloat(section)
        let center = CGPoint.init(x: rect.width / 2, y: rect.height / 2)
        
        for i in 0..<section {
            let thumb = thumbs[i]
            thumb.tag = i
            let radius: CGFloat = aRect.height / 2 + (rect.height / 2 - aRect.height / 2 ) / 2
            let x: CGFloat = center.x + radius * cos(degreeToRadian(degree))
            let y: CGFloat = center.y + radius * sin(degreeToRadian(degree))
            thumb.transform = CGAffineTransform.init(rotationAngle: degreeToRadian(degree + __rotatinDegreee))
            if i == 0 {
                delta = Float(degreeToRadian(360.0 - __rotatinDegreee)
                                        + atan2(thumb.transform.a, thumb.transform.b))
            }
            thumb.layer.position = CGPoint.init(x: x, y: y)
            degree = degree + totalRotation
            vContain.addSubview(thumb)
        }
    }
    
    // MARK: - Private Method
    private func drawClove() {
        let angle: CGFloat = CGFloat(2.0 * Double.pi / Double(section))
        for i in 0..<section {
            let view = UIView.init()
            view.layer.anchorPoint = CGPoint.init(x: 1.0, y: 0.5)
            view.layer.position = CGPoint.init(x: vContain.bounds.width / 2.0 - vContain.frame.origin.x,
                                               y: vContain.bounds.height / 2.0 - vContain.frame.origin.y)
            view.transform.rotated(by: angle * CGFloat(i))
            view.alpha = minAlpah
            vContain.addSubview(view)
        }
        _ = (section % 2 == 0) ? buildEvenCloves() : buildOddCloves()
    }
    
    private func buildEvenCloves() {
        let width: CGFloat = CGFloat(2.0 * Double.pi / Double(section))
        var mid: CGFloat = 0.0
        for i in 0..<section {
            let value = i
            let clove = Clove.init(min: mid - width / 2, max: mid + width / 2, mid: mid, index: value)
            //print("\(#function)>>>>>> i: \(i) min: \(clove.min) mid: \(clove.mid) max: \(clove.max)");
            if clove.max - width < -(CGFloat(Double.pi)) {
                mid = CGFloat(Double.pi)
                clove.mid = mid
                clove.min = CGFloat(fabsf(Float(clove.max)))
            }
            mid = mid - width
            cloves.append(clove)
        }
    }
    
    private func buildOddCloves() {
        let width: CGFloat = CGFloat(2.0 * Double.pi / Double(section))
        var mid: CGFloat = 0.0
        for i in 0..<section {
            let clove = Clove.init(min: mid - width / 2, max: mid + width / 2, mid: mid, index: i)
            mid = mid - width
            if clove.min < -(CGFloat(Double.pi)) {
                mid = -mid
                mid = mid - width
            }
            cloves.append(clove)
        }
    }
    
    @objc func buttonTouchUpInside(sender: Any?) {
        if let btn = sender as? UIButton {
            print("Button tapped \(btn.tag)")
            rotateWithIndex(btn.tag)
            index += btn.tag
            if index > 8 {
                index = index - 8
            } else if index == 8 {
                index = 0
            }
            rxIndex.onNext(index)
        }
    }
    
    // MARK: - Override Tracking
    #if true
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        //print("\(#function)")
        let touchPoint: CGPoint = touch.location(in: self)
        if touch.tapCount == 2 {
            for thumb in buttons {
                let frame = self.convert(thumb.frame, from: thumb.superview)
                if frame.contains(touchPoint) {
                    rotateWithIndex(thumb.tag)
                    return false
                }
            }
        }
        let dx: Float = Float(touchPoint.x - vContain.center.x)
        let dy: Float = Float(touchPoint.y - vContain.center.y)
        delta = atan2(dy, dx)
        startTransform = vContain.transform
        if let target = delegate {
            if target.wheelBeginTracking != nil {
                target.wheelBeginTracking?(self)
            }
        }
        
        return true
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        //print("\(#function)")
        let pt = touch.location(in: self)
        let prePt = touch.previousLocation(in: self)
        let dist = distanceFromCenter(pt)
        let ret = calculateMoveItem()
        if current != ret.clove && pt.x != prePt.x && pt.y != prePt.y {
            current = ret.clove
            if let target = delegate {
                if target.wheelValueChanged != nil {
                    let value = ret.clove > 0 ?  (8 - ret.clove) : 0
                    target.wheelValueChanged?(self, index: value, direct: ret.direct)
                }
            }
        }
        if dist > 40 {
            self.direct = ret.direct
            //current = ret.clove
            vContain.transform = startTransform!
            let dx: CGFloat = pt.x - vContain.center.x
            let dy: CGFloat = pt.y - vContain.center.y
            let ang = atan2f(Float(dy), Float(dx))
            let diff = delta - ang
            vContain.transform = vContain.transform.rotated(by: -CGFloat(diff))
        }
        
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        print("\(#function)")
        let ret = calculateMoveItem()
        //current = ret.clove
        UIView.animate(withDuration: 0.2) { [self] in
            vContain.transform = vContain.transform.rotated(by: -ret.value)
        }
    }
    #endif
    
    private func distanceFromCenter(_ point: CGPoint) -> CGFloat {
        let center: CGPoint = CGPoint.init(x: self.bounds.width / 2, y: self.bounds.height / 2)
        let dx = point.x - center.x
        let dy = point.y - center.y
        return CGFloat(sqrtf(Float((dx * dx) + (dy * dy))))
    }
    
    private func calculateMoveItem() -> (direct: Int, value: CGFloat, clove: Int) {
        var aDirect = 0
        var aValue: CGFloat = 0.0
        var aClove = 0
        
        let b = Float(vContain.transform.b)
        let a = Float(vContain.transform.a)
        let radian: CGFloat = CGFloat(atan2f(b, a))
        var idx = 0
        for clove in cloves {
            if clove.min > 0.0 && clove.max < 0.0 {
                if clove.max > radian || clove.min < radian {
                    aValue = radian - clove.mid
                    let ret = setItemValue(clove)
                    aDirect = ret.direct
                    aClove = ret.clove
                }
            } else if radian > clove.min && radian < clove.max {
                aValue = radian - clove.mid
                let ret = setItemValue(clove)
                aDirect = ret.direct
                aClove = ret.clove
            }
            idx += 1
        }
            
        return (aDirect, aValue, aClove)
    }
        
    private func setItemValue(_ clove: Clove) -> (direct: Int, clove: Int) {
        var aDirect = 0
        var aClove = current
        var temp = current
        
        if temp == 0 && clove.value == 7 {
            temp = 8
        }
        if temp == 7 && clove.value == 0 {
            temp = -1
        }
        if temp > clove.value {
            aClove = clove.value
            aDirect = 1
        }
        if temp < clove.value {
            aClove = clove.value
            aDirect = -1
        }
        if aDirect == 0 {
            aDirect = direct
        }
        
        return (aDirect, aClove)
    }

    // MARK: - Public Method
    func clockwise() {
        current += 1
        if current >= section {
            current = 0
        }
        Log.message(to: " right >>>>>> \(current)");
        rotateWithIndex(1)
    }
    
    func counterclockwise() {
        current -= 1
        if current < 0 {
            current = section - 1
        }
        Log.message(to: " left >>>>>> \(current)");
        rotateWithIndex(7)
    }
    
    func rotateWithIndex(_ index: Int) {
        let clove = cloves[index]
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2) { [self] in
                vContain.transform = vContain.transform.rotated(by: -clove.mid)
            }
        }
    }
    
    func udateDegree(_ degree: CGFloat) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2) { [self] in
                vContain.transform = vContain.transform.rotated(by: degree)
            }
        }
    }
}
