//
//  TitleView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit

enum TitleType: String {
    case line = "line"
    case middle = "middle"
    case normal = "normal"
}

class TitleView: UIView {
    
    lazy var line: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let title: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    public init(type: TitleType) {
        super.init(frame: .zero)
        draw(type: type)
    }

}

extension TitleView {
    
    public func draw(type: TitleType) {

        self.addSubview(title)
        title.setLayerBorder()
        
        switch type {
        case .line:
            setLine()
            break
        case .middle:
            setMiddle()
            break
        case .normal:
            setNormal()
            break
        }
    }
    
    func setLine() {
        
        line.roundCorners(.allCorners, radius: 2.0)
        self.addSubview(line)
        line.setLayerBorder()
        
        title.font = .PoppinsBold(ofSize: 16.0)
        title.textAlignment = .left
        title.textColor = .black
        
        line.snp.makeConstraints { [weak self] view -> Void in
            if let weakSelf = self {
                view.top.equalTo(weakSelf).offset(1.5)
                view.left.equalTo(weakSelf)
                view.bottom.equalTo(weakSelf).offset(-1.5)
                view.width.equalTo(5.0)
            }
        }
        
        title.snp.makeConstraints { [weak self] view -> Void in
            if let weakSelf = self {
                view.top.equalTo(weakSelf)
                view.left.equalTo(line.snp.right).offset(11.0)
                view.right.equalTo(weakSelf)
                view.height.equalTo(23.0)
            }
        }
    }
    
    func setMiddle() {
        
        title.font = .PoppinsBold(ofSize: 18.0)
        title.textAlignment = .center
        title.textColor = .colorRGB(212, 0, 16)
        
        title.snp.makeConstraints { [weak self] view -> Void in
            if let weakSelf = self {
                view.top.equalTo(weakSelf).offset(24.0)
                view.left.right.equalTo(weakSelf)
                view.height.equalTo(25.0)
            }
        }

    }
    
    func setNormal() {
        
        title.font = .PoppinsBold(ofSize: 16.0)
        title.textAlignment = .left
        title.textColor = .black
        
        title.snp.makeConstraints { [weak self] view -> Void in
            if let weakSelf = self {
                view.bottom.equalTo(weakSelf).offset(-10.0)
                view.left.equalTo(weakSelf).offset(15.0)
                view.right.equalTo(weakSelf).offset(-15.0)
                view.height.equalTo(23.0)
            }
        }
    }
    
//    func setComponent() {
//
//        let label = UILabel()
//        label.font = .systemFont(ofSize: 12.0)
//        label.textAlignment = .left
//        label.textColor = .white
//        return label
//
//        self.addSubview(imageView)
//        imageView.setLayerBorder()
//
//        title.font = UIFont.systemFont(ofSize: 22.0, weight: .medium)
//        title.textAlignment = .left
//        title.textColor = .black
//        self.addSubview(title)
//        title.setLayerBorder()
//
//        message.font = UIFont.systemFont(ofSize: 12.0, weight: .medium)
//        message.textAlignment = .left
//        message.textColor = .gray
//        self.addSubview(message)
//        message.setLayerBorder()
//    }
//
//
//    func setAutoLayOuy() {
//        imageView.snp.makeConstraints { view -> Void in
//            view.top.equalTo(self).offset(10.0)
//            view.left.equalTo(self).offset(25.0)
//            view.width.equalTo(40.0)
//            view.height.equalTo(20.0)
//        }
//
//        title.snp.makeConstraints { view -> Void in
//            view.top.equalTo(self).offset(8.0)
//            view.left.equalTo(imageView.snp.right).offset(7.0)
//            view.right.equalTo(self).offset(-25.0)
//            view.height.equalTo(25.0)
//        }
//
//        message.snp.makeConstraints { view -> Void in
//            view.top.equalTo(imageView.snp.bottom).offset(8.0)
//            view.left.equalTo(self).offset(25.0)
//            view.right.equalTo(self).offset(-25.0)
//            view.height.equalTo(15.0)
//        }
//    }
}
