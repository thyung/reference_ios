//
//  InfoView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit

enum InfoViewType {
    case clip(_ state: Clip)
    case list(_ state: List)
        
    enum Clip: String {
        case live = "live"
        case vod = "vod"
    }
    enum List: String {
        case live = "live"
        case vod = "vod"
    }
}


class InfoView: UIView {
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
        
    let title: UILabel = UILabel()
    let message: UILabel = UILabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public init() {
        super.init(frame: .zero)
        setComponent()
    }
    
    public init(type: InfoViewType) {
        super.init(frame: .zero)
        setComponent()
        draw(type: type)
    }

}

extension InfoView {
    
    func setComponent() {
        title.textAlignment = .left
        title.textColor = .black
        self.addSubview(title)
        title.setLayerBorder()
        
        message.textAlignment = .left
        message.textColor = .colorRGB(170, 170, 170)
        self.addSubview(message)
        message.setLayerBorder()
    }
    
    
    func draw(type: InfoViewType) {
                
        switch type {
        case .clip(.live):
            setClip(live: true)
            break
        case .clip(.vod):
            setClip(live: false)
            break
        case .list(.live):
            setList(live: true)
            break
        case .list(.vod):
            setList(live: false)
            break
        }
    }


    func setClip(live: Bool) {
        if live {
            self.addSubview(imageView)
            imageView.image = UIImage(named: "imgLive")
            imageView.setLayerBorder()
            
            imageView.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(weakSelf).offset(18.0)
                    view.left.equalTo(weakSelf).offset(25.0)
                    view.width.equalTo(33.0)
                    view.height.equalTo(16.0)
                }
            }

            title.font = .OpenSansSemiBold(ofSize: 16.0)
            title.textColor = .black
            
            message.font = .OpenSansSemiBold(ofSize: 10.0)
            message.textColor = .colorRGB(170, 170, 170)            
            
            title.snp.makeConstraints {[weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(weakSelf).offset(14.0)
                    view.left.equalTo(imageView.snp.right).offset(8.0)
                    view.right.equalTo(weakSelf).offset(-25.0)
                    view.height.equalTo(22.0)
                }
            }
            
            message.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(imageView.snp.bottom).offset(6.0)
                    view.left.equalTo(weakSelf).offset(25.0)
                    view.right.equalTo(weakSelf).offset(-25.0)
                    view.height.equalTo(14.0)
                }
            }
        }
        else {
            title.font = .OpenSansSemiBold(ofSize: 12.0)
            title.numberOfLines = 2
            message.font = .OpenSansSemiBold(ofSize: 10.0)
            
            title.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(weakSelf)
                    view.left.right.equalTo(weakSelf)
                    view.height.equalTo(34.0)
                }
            }
            
            message.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(title.snp.bottom).offset(4.0)
                    view.left.right.equalTo(weakSelf)
                    view.height.equalTo(14.0)
                }
            }
        }
    }
    
    func setList(live: Bool) {
                
        if live {            
            title.numberOfLines = 2
            title.font = UIFont.OpenSansSemiBold(ofSize: 15.0)
            title.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(weakSelf)
                    view.left.right.equalTo(weakSelf)
                    view.height.equalTo(41.0)
                }
            }

            message.font = UIFont.OpenSansSemiBold(ofSize: 10.0)
            message.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(title.snp.bottom).offset(5.0)
                    view.left.right.equalTo(weakSelf)
                    view.height.equalTo(16.0)
                }
            }

        } else {
            message.font = UIFont.systemFont(ofSize: 12.0, weight: .medium)
            message.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(weakSelf).offset(2.0)
                    view.left.right.equalTo(weakSelf)
                    view.height.equalTo(15.0)
                }
            }
            
            title.numberOfLines = 2
            title.font = UIFont.systemFont(ofSize: 22.0, weight: .medium)
            title.snp.makeConstraints { [weak self] view -> Void in
                if let weakSelf = self {
                    view.top.equalTo(message.snp.bottom).offset(5.0)
                    view.left.right.equalTo(weakSelf)
                    view.height.equalTo(50.0)
                }
            }
        }        
    }
            
}
