//
//  PositionPlayerView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/18.
//

import RxCocoa
import RxSwift
import SnapKit

class LivePlayerView: ControlPlayerView {
    
    private var livePauseTime: Int64 = 0
    var birdType: ContentBirdType = .normal
    lazy var pipView: BirdView = {
        var view: BirdView
        switch birdType {
        case .normal:
            view = BirdView()
          break
        case .soccer:
            view = SoccerBirdView(type: .pip)
            break
        case .basket:
            view = BasketBirdView(type: .pip)
            break
        case .ufc:
            view = UFCBirdView(type: .pip)
            break
        case .baseball:
            view = BaseballBirdView(type: .pip)
            break
        }
        view.birdType = .pip
        view.backgroundColor = .black
        view.isInteractive = _isInteractive
        self.addSubview(view)
        view.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.top.right.equalTo(weakSelf)
                let width = weakSelf.width * 0.4
                let height = weakSelf.height * 0.4
                view.width.equalTo(width)
                view.height.equalTo(height)
            }
        }
        return view
    }()
    
    lazy var btPip: UIButton = {
        var button = UIButton()
        let image = UIImage(named: "img_location_overview_landscape")
        button.setBackgroundImage(image, for: .normal)
        self.addSubview(button)
        button.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.right.equalTo(weakSelf).offset(-2)
                view.top.equalTo(weakSelf).offset(2)
            }
        }
        return button
    }()
    
    var hasToggle = false
        
    /* Postion swipe */
    var wResolution: Int32 = 1920
    var hResolution: Int32 = 1080
    var targetChannel: Int32 = 0
    var positionChannels: Set<Int> = []
    lazy var screenScale: CGFloat = {
        let scale = UIScreen.main.scale
        return scale
    }()
    
    lazy var markerView: MarkerView = {
        var view = MarkerView.init(frame: self.bounds)
        view.backgroundColor = .clear
        self.addSubview(view)
        view.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.top.left.right.bottom.equalTo(weakSelf)
            }
        }
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.type = .live
        progressView.btSwitch?.delegate = self
        setRXFucntion()
        fdPlayerRXState()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.type = .live
        progressView.btSwitch?.delegate = self
        setRXFucntion()
        fdPlayerRXState()
    }
    
    private func setRXFucntion() {
        progressView.btNow.rx.tap.bind { [weak self] in
            guard let self = self else { return }
            self.fdPlayer.playToNow()
            self.beginTimer()
            self.progressView.btNow.isHidden = true
            self.isTimeShift = false
        }.disposed(by: self.disposeBag)
    }
    
    override func landscapeMode(_ isLandscape: Bool) {
        _isLandscape = isLandscape
        if _isInteractive == true {
            swipePad.isHidden = !isLandscape
            timePad.isHidden = !isLandscape
        }
        landscapleTitleView.isHidden = !isLandscape
        btPip.isHidden = !isLandscape
        pipView.isHidden = !isLandscape
        if hasToggle != true {
            setToglePipView()
        }
    }
    
    func setToglePipView() {
        hasToggle = true
        btPip.rx.tap.bind { [weak self] in
            if let weakSelf = self {
                weakSelf.pipView.playToNow()
                weakSelf.pipView.fadeIn()
            }
        }.disposed(by: self.disposeBag)
        
        pipView.btClose.rx.tap.bind { [weak self] in
            if let weakSelf = self {
                weakSelf.pipView.pause()
                weakSelf.pipView.fadeOut()
            }
        }.disposed(by: self.disposeBag)
    }
    
    override func onControlPuase() {
        if fdPlayer.state != FD_STATE_PAUSE {
            let button = progressView.btPause
            progressView.playButtonTouchUpInside(button)
            playButtonTouchUpInside(button)
            
            self.pauseComplete = { [weak self] in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    print("\(#function) pause completet")
                    self.progressView.setLiveDuration(self.livePauseTime)
                    self.progressView.setTimeShiftMode(true)
                    self.progressView.btNow.isHidden = false
                }
            }
        }
    }
    
    private func fdPlayerRXState() {
        self.rxState.asObservable().subscribe { value in
            Log.message(to: "\(#function) state: \(value)")
        } onError: { error in
            Log.message(to: "\(#function) index: \(error)")
        }.disposed(by: self.disposeBag)
    }
    
    // MARK: - Button Acttion
    override func onPlay() {
        Log.message(to: "\(#function)")
        super.onPlay()
        isTimeShift = true
    }
    
    override func onPause() {
        Log.message(to: "\(#function)")
        super.pause()
        isTimeShift = true
        progressView.setTimeShiftMode(true)
    }
    
    // MARK: - FDLivePlayer Delegate
    override func currentPlayInfo(_ channel: Int32, frame: Int32, frameCycle: Int32, time: Int32) {
        if self.isTimeShift == true {
            //print("\(#function)>>>>>> timeshift: \(time)")
            DispatchQueue.main.async { [weak self] in
                self?.progressView.updatePlayTime(Int64(time))
            }
        } else {
            self.livePauseTime = Int64(time)
        }        
    }
    
    override func videoStreamInfo(_ width: Int32, height: Int32, duration: Int32) {
        wResolution = width
        hResolution = height
    }
    
    // MARK: - Auto tracking
    func getNalSEI(_ player: FDLivePlayer, uuid: String, metadata: NSObjectProtocol) {
        if let bt = progressView.btSwitch,
           bt.isOn == true {
            onAutoTraking(metadata: metadata)
        }
    }
    
    private func onAutoTraking( metadata: NSObjectProtocol) {
        if let info = metadata as? [String : AnyObject],
           let nPosX = info["positionX"] as? Int,
           let nPosY = info["positionY"] as? Int,
           let ratio = info["ratio"] as? Int,
           let resolutionWidth = info["width"] as? Int,
           let resolutionheight = info["height"] as? Int {
            if ratio > 100  {
                DispatchQueue.main.async {
                    let width = self.fdPlayer.playerView.width
                    let height = self.fdPlayer.playerView.height
                    let viewScale = self.screenScale
                    
                    let scale: CGFloat = CGFloat(ratio) / 100.0
                    let ratioX = CGFloat(nPosX) / CGFloat(resolutionWidth)
                    let ratioY = CGFloat(nPosY) / CGFloat(resolutionheight)
                    let screenWidth = width * scale * viewScale
                    let screenHeight = height * scale * viewScale
                    let centerX = width / 2
                    let centerY = height / 2
                    let ptX = ratioX * screenWidth
                    let ptY = ratioY * screenHeight
                    
                    var retX = ptX - centerX * viewScale
                    var retY = ptY - centerY * viewScale
                    
                    if retX < 0 { retX = 0 }
                    if retY < 0 { retY = 0 }
                    if retX > screenWidth - width * viewScale {
                        retX = screenWidth - width * viewScale
                    }
                    if retY > screenHeight - height * viewScale {
                        retY = screenHeight - height * viewScale
                    }

                    let ret = CGPoint(x: retX, y: retY)
                    self.fdPlayer.setDisplay(ret, scale: scale)
                }
            }
            if let channel =  info["channel"] as? Int {
                if self.currentChannel != (channel - 1) && self.targetChannel != channel {
                    self.fdPlayer.setTargetChannel(channel - 1)
                    self.targetChannel = Int32(channel)
                }
            }
            
            Log.message(to: "\(#function)>>>>>> channel: \(String(describing: info["channel"]))"
                + "x: \(String(describing: info["positionX"]))" +
                    "y: \(String(describing: info["positionY"]))")
        }
    }
    
    // MARK: Position Swipe
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.zoomScale <= 1.0 {
            markerView.isHidden = true
            fdPlayer.isPosition = false
            return
        }
        self.fdPlayer.isPosition = true
        if self.fdPlayer.isPosition == true {
            markerView.isHidden = false
            let offset = scrollView.contentOffset
            var retX = (offset.x + scrollView.frame.width / 2)
            var retY = (offset.y + scrollView.frame.height / 2)
            let ratioX = CGFloat(self.wResolution) / scrollView.contentSize.width
            let ratioY = CGFloat(self.hResolution) / scrollView.contentSize.height

            let lenght: CGFloat = CGFloat(fdPlayer.getPositionLenght())
            let makerLength = lenght / ratioY
            //print("maker lenght: \(lenght) makerLenght: \(makerLength)");
            retX = retX * ratioX
            retY = retY * ratioX// - lenght
            let ret = fdPlayer.setVecPositionAxis(self.currentChannel,
                                                   scale: Float(scrollView.zoomScale),
                                                   posX: Int32(retX),
                                                   posY: Int32(retY))
            print("ret: \(ret) = setVecPositionAxis(channel: \(self.currentChannel) "
                  + "scale: \(scrollView.zoomScale) posX: \(retX) posY: \(retY)")
            markerView.setPostionLengthWithScale(scrollView.zoomScale, length: makerLength)
        }
    }
    
    func getRenderWillUpdate(_ player: FDLivePlayer, channel: Int32) {
        if player.isPosition {
            // TODO: 락처리
            if self.positionChannels.contains(Int(channel)) {
                print("\(#function) channel: \(channel)")
                self.arrangeOffset(channel)
                self.positionChannels.remove((Int(channel)))
            }
        }
    }
    
    func arrangeOffset(_ channel: Int32) {
        let nPosX = UnsafeMutablePointer<Int32>.allocate(capacity: 1)
        let nPosY = UnsafeMutablePointer<Int32>.allocate(capacity: 1)
        let retX = self.fdPlayer.getMovedPosX(channel, nPoX: nPosX);
        let retY = self.fdPlayer.getMovedPosY(channel, nPoY: nPosY);
        Log.message(to: "getMovedPosX(channel: \(channel) ) ret: \(retX) posX: \(nPosX.pointee)")
        Log.message(to: "getMovedPosY(channel: \(channel) ) ret: \(retY) posY: \(nPosY.pointee)")
        if retX != 1 || retY != 1 {
            //LOGD(to: "Fail to get postion value. (X: \(retX) Y: \(retY))")
            return
        }
        DispatchQueue.main.async {
            let width = self.contain.contentSize.width
            let height = self.contain.contentSize.height
            let ratioX = CGFloat(nPosX.pointee) / CGFloat(self.wResolution)
            let ratioY = CGFloat(nPosY.pointee) / CGFloat(self.hResolution)
            let centerX = (self.contain.frame.width / 2)
            let centerY = (self.contain.frame.height / 2)
            
            let retX = width * ratioX
            let retY = height * ratioY
                        
            let valueX = retX - centerX
            let valueY = retY - centerY
            let ret = CGPoint(x: valueX, y: valueY)
            self.contain.contentOffset = ret
            
            nPosX.deallocate()
            nPosY.deallocate()
        }
    }
}

extension LivePlayerView: SwitchButtonDelegate {
    
    func onAutoTrakingMode(_ isMode: Bool) {
        swipePad.base.isHidden = isMode
        swipePad.stick.isHidden = isMode
        timePad.base.isHidden = isMode
        timePad.stick.isHidden = isMode
        //vBird.onAutoTrakingMode(isMode)
        progressView.slider.isHidden = isMode
        if isMode == false && fdPlayer.isDisplaySize == true {
            fdPlayer.isDisplaySize = false
        }
    }
    
    @objc func isOnValueChange(isOn: Bool) {
        var message = ""
        Log.message(to: "\(#function) Auto tracking is \(isOn)")
        beginTimer()
        onAutoTrakingMode(isOn)
        if isOn != true {
            fdPlayer.setDisplay(CGPoint.zero, scale: 1.0)
            message = "Auto Interactive Streaming OFF"
        } else {
            message = "Auto Interactive Streaming ON"
        }
        showToastMessage(message)
    }
}

extension LivePlayerView {
    
    override func sliderMoveBegin(_ slider: UISlider) {
        invalidateTimer()
        if fdPlayer.state != FD_STATE_PLAY && fdPlayer.state != FD_STATE_PAUSE {
            Log.message(to: "It is not playeing or pause.")
            return
        }
        lastPauseStatus = progressView.btPause.isSelected
        onControlPuase()
    }
    
    override func sliderMoveEnd(_ slider: UISlider, time: Int, base: Float) {
        Log.message(to: "\(#function) time: \(time)")
        print("\(#function) seek time: \(time)")
        fdPlayer.seek(time)
        beginTimer()
        if !lastPauseStatus {
            progressView.btPause.isSelected = false
            fdPlayer.controlHandler = { [weak self] isSuccess, message, erro in
                self?.fdPlayer.controlHandler = nil
                sleep(UInt32(1))
                self?.onPlay()
            }
        }
    }
    
    override func progressFinish(_ slider: UISlider) {
        // TODO: player 상태 체크 및 로직 조건 판단실행 필요
//        fdPlayer.playToNow()
//        progressView.setTimeShiftMode(false)
//        isTimeShift = false
    }
}

