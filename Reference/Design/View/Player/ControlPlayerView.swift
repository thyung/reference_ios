//
//  ControlPlayerView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/10.
//

import RxCocoa
import RxSwift
import SnapKit

enum PadTouch: Int {
    case end
    case begin
}

protocol PlayerDelegate {
    func updateChannel(_ channle: Int)
    func currentPlayInfo(_ channel: Int32, frame: Int32, frameCycle: Int32, time: Int32)
    func videoStreamInfo(_ width: Int32, height: Int32, duration: Int32)
}

// will use by vod player
class ControlPlayerView: PlayerView, PlayerDelegate {
    
    private var padState: Int = 0
    private var timer: Timer?
    private var isExist = false
    internal var lastPauseStatus: Bool = false
    internal var contain = UIScrollView()
    
    var btRepeat = UIButton()
    var swipePad: ControlPad!
    var timePad: ControlPad!
    let disposeBag: DisposeBag = DisposeBag()
    var landscapleTitleView = LandscapeTitleView()
    var progressView = ProgressView()
    var type: StreamType {
        get { return _type }
        set (newval) {
            _type = newval
            progressView.isLive = (type == .live)
            btRepeat.isHidden = (type == .live)
        }
    }
    var isLandScape: Bool {
        get { return  _isLandscape }
        set (newval) {
            DispatchQueue.main.async { [weak self] in
                self?.landscapeMode(newval)
            }
        }
    }
    var isInteractive: Bool {
        get { return  _isInteractive }
        set (newval) {
            DispatchQueue.main.async { [weak self] in
                self?.interactiveMode(newval)
            }
        }
    }
    lazy var lbNotPlayed: UILabel = {
        var label = UILabel()
        label.text = "The video cannot be played."
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.OpenSans(ofSize: 13.0)
        self.addSubview(label)
        label.snp.makeConstraints { [weak self] lb in
            guard let self = self else { return }
            lb.centerX.equalTo(self)
            lb.bottom.equalTo(self).offset(-39.0)
        }
        
        return label
    }()
    
    lazy var lbStreamInfo = UILabel()
    
    private var item: DispatchWorkItem?
    lazy var lbToast = UILabel()
    lazy var toastView: BaseView = {
        var view = BaseView.init(frame: self.bounds)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(view)
        view.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.top.equalTo(weakSelf).offset(0)
                ctrl.left.equalTo(weakSelf).offset(0)
                ctrl.right.equalTo(weakSelf).offset(0)
                ctrl.height.equalTo(48)
            }
        }
        
        lbToast.font = UIFont.systemFont(ofSize: 14.0)
        lbToast.textColor = UIColor.white
        lbToast.numberOfLines = 2
        view.addSubview(lbToast)
        lbToast.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.left.equalTo(weakSelf).offset(16)
                ctrl.right.equalTo(weakSelf).offset(-16)
                ctrl.top.bottom.equalTo(view).offset(0)
            }
        }
        
        return view
    }()
    
    /*
        //#define TICK   _startTime = [NSDate date]
        //#define TOCK   NSLog(@"%s Time interval: %f", __FUNCTION__, -[_startTime timeIntervalSinceNow] * 1000)
        var _startTime = Date()
        func TICK() {
            _startTime = Date()
            print("\(#function)>>>>>> \(_startTime)")
        }
        
        func TOCK(_ funcName: String = #function) {
            print("\(funcName) time intterval: \(_startTime.timeIntervalSinceNow * 1000)")
        }
      */
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)        
        self.type = .vod
        setupLandscapeBackground()
        setupContainView()
        setupjoystick()
        setupProgress()
        setupRepeatButton()
        beginTimer()
        setStreamInfoLabel()
        
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapHandler(_:)))
        tapRecognizer.delegate = self
        self.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func tapHandler(_ recognizer: UITapGestureRecognizer) {
        hideControl(!progressView.isHidden)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()        
        self.type = .vod
        setupLandscapeBackground()
        setupContainView()
        setupjoystick()
        setupProgress()
        setupRepeatButton()
        beginTimer()
    }

    private func setupRepeatButton() {
        if let normal = UIImage(named: "icon_loop_on"),
           let selected = UIImage(named: "icon_loop_off") {
            btRepeat.setBackgroundImage(normal, for: .normal)
            btRepeat.setBackgroundImage(selected, for: .selected)
        }
        btRepeat.backgroundColor = UIColor.clear
        self.addSubview(btRepeat)
        
        btRepeat.rx.tap
            .scan(false) {[weak self] (lastState, newValue) in
                self?.fdPlayer.isLoop = lastState
                return !lastState
            }
            .bind(to: btRepeat.rx.isSelected)
            .disposed(by: disposeBag)
        
        btRepeat.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.top.equalTo(weakSelf).offset(2)
                btn.right.equalTo(weakSelf).offset(-5)
            }
        }
    }
    
    private func setupLandscapeBackground() {
        self.addSubview(landscapleTitleView)
        landscapleTitleView.isHidden = true
        landscapleTitleView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.left.top.right.bottom.equalTo(weakSelf)
            }
        }
    }
    
    private func setupContainView() {
        contain.isScrollEnabled = false
        contain.showsHorizontalScrollIndicator = false
        contain.showsVerticalScrollIndicator = false
        contain.bounces = false
        contain.bouncesZoom = false
        contain.alwaysBounceHorizontal = false
        contain.alwaysBounceVertical = false
        contain.minimumZoomScale = 1.0
        contain.maximumZoomScale = 5.0
        contain.translatesAutoresizingMaskIntoConstraints = false
        contain.delegate = self
        self.insertSubview(contain, at: 0)
        contain.addSubview(fdPlayer.playerView)
        
        let width = UIScreen.width
        let height = width * (9.0/16.0)
        contain.snp.makeConstraints { view in
            view.edges.equalToSuperview()
        }
        fdPlayer.playerView.snp.remakeConstraints { view in
            view.edges.equalTo(contain)
            view.height.equalTo(height)
            view.width.equalTo(width)
        }
        
        
        let topShadow: UIImageView = UIImageView(image: #imageLiteral(resourceName: "imgPortraitPlayerShadow.png"))
        topShadow.isUserInteractionEnabled = false
        contain.addSubview(topShadow)
        
        topShadow.snp.makeConstraints { view in
            view.left.right.top.equalTo(contain)
            view.height.equalTo(79.0)            
        }

        
    }
    
    private func setupjoystick() {
        swipePad = ControlPad.init(frame: CGRect.init(x: 0, y: 0, width: 120, height: 50))
        swipePad.delegate = self
        if let normal = UIImage(named: "icon_channel_swipe_normal"),
           let highlight = UIImage(named: "icon_channel_swipe_dim") {
            swipePad.stick.image = normal
            swipePad.stick.highlightedImage = highlight
        }
        swipePad.isHidden = true
        self.addSubview(swipePad)
        swipePad.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.bottom.equalTo(weakSelf.safeAreaLayoutGuide.snp.bottom).offset(-80)
                ctrl.left.equalTo(weakSelf).offset(40)
                ctrl.height.equalTo(50)
                ctrl.width.equalTo(120)
            }
        }

        timePad  = ControlPad.init(frame: CGRect.init(x: 0, y: 0, width: 120, height: 50))
        timePad.delegate = self
        timePad.isHidden = true
        if let normal = UIImage(named: "icon_time_machine_normal"),
           let highlight = UIImage(named: "icon_time_machine_dim") {
            timePad.stick.image = normal
            timePad.stick.highlightedImage = highlight
        }
        self.addSubview(timePad)
        timePad.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.bottom.equalTo(weakSelf.safeAreaLayoutGuide.snp.bottom).offset(-80)
                view.right.equalTo(weakSelf).offset(-40)
                view.height.equalTo(50)
                view.width.equalTo(120)
            }
        }
    }
    
    private func setupProgress() {
        progressView.btPause.rx.tap.subscribe { [weak self] event in
            if let btn = self?.progressView.btPause {
                //btn.isSelected ? self?.onPause() : self?.onPlay()
                self?.playButtonTouchUpInside(btn)
            }
        }.disposed(by: self.disposeBag)

        self.addSubview(progressView)
        progressView.backgroundColor = .clear
        progressView.delegate = self        
        progressView.snp.makeConstraints { [weak self] view in
            let height = UIScreen.width * (72.0/390.0)
            if let weakSelf = self {
                view.left.right.equalTo(weakSelf)
                view.height.equalTo(height)
                view.bottom.equalTo(weakSelf.safeAreaLayoutGuide.snp.bottom)
            }
        }
    }
    
    // MARK: - Button Action
    @objc func playButtonTouchUpInside(_ sender: Any?) {
        if let btn = sender as? UIButton {
            btn.isSelected ? onPause() : onPlay()
        }
        beginTimer()
    }
    
    internal func onPlay() {
        Log.message(to: "\(#function)")
        fdPlayer.play()
        isTimeShift = false
    }
    
    internal func onPause() {
        Log.message(to: "\(#function)")
        fdPlayer.pause()
        isTimeShift = true
    }
    
    internal func beginTimer() {
        invalidateTimer()
        timer = Timer.scheduledTimer(timeInterval: 5, target: self,
                                     selector: #selector(onTick(timer:)), userInfo: nil, repeats: false)
        Log.message(to: "")
    }
    
    internal func invalidateTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc private func onTick(timer: Timer) {
        //Log.message(to: "\(#function)")
        hideControl(true)
    }
    
    private func hideControl(_ isHidde: Bool) {
        if isHidde == true {
            swipePad.isEnable = false
            timePad.isEnable = false
            if _isLandscape == true {
                landscapleTitleView.fadeOut()
            }
            if type == .vod {
                btRepeat.fadeOut()
            }
            progressView.fadeOut()
            invalidateTimer()
        } else {
            if _isLandscape == true {
                if let btn = progressView.btSwitch, btn.isOn != true {
                    swipePad.isEnable = true
                    timePad.isEnable = true
                }
                landscapleTitleView.fadeIn()
            }
            if type == .vod {
                btRepeat.fadeIn()
            }
            progressView.fadeIn()
            beginTimer()
        }
    }
    
    internal func onControlPuase() {
        if fdPlayer.state != FD_STATE_PAUSE {
            let button = progressView.btPause
            progressView.playButtonTouchUpInside(button)
            playButtonTouchUpInside(button)            
        }
    }
    
    internal func landscapeMode(_ isLandscape: Bool) {
        _isLandscape = isLandscape
        if _isInteractive == true {
            swipePad.isHidden = !isLandscape
            timePad.isHidden = !isLandscape
        }
        landscapleTitleView.isHidden = !isLandscape
    }
    
    override func showReopenButton(_ isShow: Bool) {
        DispatchQueue.main.async {
            self.btReopen.isHidden = !isShow
            self.lbNotPlayed.isHidden = !isShow
            if isShow == true {
                self.bringSubviewToFront(self.btReopen)
                self.bringSubviewToFront(self.lbNotPlayed)
            }
        }
    }
    
    func updateChannel(_ channle: Int) { }
    func currentPlayInfo(_ channel: Int32, frame: Int32, frameCycle: Int32, time: Int32) { }
    func videoStreamInfo(_ width: Int32, height: Int32, duration: Int32) { }
}

extension ControlPlayerView {
    func setStreamTitle(_ title: String) {
        landscapleTitleView.lbTitle.text = title
        
    }
    
    func setStreamDescription(_ description: String) {
        landscapleTitleView.lbDscript.text = description
    }
    
    func interactiveMode(_ isMode: Bool) {
        _isInteractive = isMode
        swipePad.isHidden = !isMode
        timePad.isHidden = !isMode
    }
}

extension ControlPlayerView {
    func showToastMessage(_ message: String) {
        toastCancle()
        showToastView()
        item = DispatchWorkItem { [weak self] in
            self?.closeToastView()
            self?.item = nil
        }
        lbToast.text = message
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: item!)
    }
    
    private func showToastView() {
        self.addSubview(toastView)
        toastView.fadeIn()
    }
    
    private func toastCancle() {
        if item != nil {
            item?.cancel()
        }
    }
    
    func closeToastView()  {
        toastView.fadeOut()
    }
    
    private func setStreamInfoLabel() {
        #if DEBUG
        lbStreamInfo.font = UIFont.systemFont(ofSize: 12.0)
        lbStreamInfo.textColor = UIColor.white
        lbStreamInfo.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        lbStreamInfo.numberOfLines = 8
        self.addSubview(lbStreamInfo)
        lbStreamInfo.snp.makeConstraints { [weak self] view in
            if let self = self {
                view.width.equalTo(180.0)
                view.height.equalTo(100)
                view.top.equalTo(self)
                view.right.equalTo(self)
            }
        }
        #endif
    }
}

extension ControlPlayerView: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return fdPlayer.playerView
    }
}

// MARK: - ControlPad Delegate
extension ControlPlayerView: ControlPadDelegate {
    
    @objc func moveBegin(_ pad: ControlPad) {
        if fdPlayer.state != FD_STATE_PLAY && fdPlayer.state != FD_STATE_PAUSE {
            Log.message(to: "It is not playeing or pause.")
            return
        }

        if pad == self.timePad {
            lastPauseStatus = progressView.btPause.isSelected
            onControlPuase()
        }
        
        self.padState |= PadTouch.begin.rawValue << pad.tag
        if padState != 0 {
            let aQueue = DispatchQueue.global(qos: .userInteractive)
            isExist = false
            aQueue.async {
                self.onEventThread()
            }
            Log.message(to: "\(#function) \(pad) thread start.")
        }

        hideControl(false)
        invalidateTimer()
    }        
    
    @objc func moveEnd(_ pad: ControlPad) {
        if let player = self.fdPlayer {
            if player.state != FD_STATE_PLAY && player.state != FD_STATE_PAUSE {
                Log.message(to: "It is not playeing or puase.")
                return
            }
        }

        if pad == self.timePad {
            if !lastPauseStatus {
                progressView.btPause.isSelected = false
                onPlay()
            }
        }

        self.padState &= ~(PadTouch.begin.rawValue << pad.tag)
        if padState == 0 {
            isExist = true
            pad.clearEvent()
            Log.message(to: "\(#function) \(pad) thread stop.")
        }
        beginTimer()
    }
    
    @objc func onEventThread() {
        autoreleasepool {
            repeat {
                if isExist == true { break }
                let interval = 0.04
                let swipe = self.swipePad.getEvent()
                let time = self.timePad.getEvent()
                
                
                /* swipe shift */
                if let s: Int8 = swipe, time == nil {
                    switch s {
                    case -3 ... -1:
                        changeChannel(self.isTimeShift, direct: "left", frame: abs(s))
                        break
                    case 1...3:
                        changeChannel(self.isTimeShift, direct: "right", frame: s)
                        break
                    default:
                        break
                    }
                }

                /* time shift */
                if let t: Int8 = time, swipe == nil {
                    switch Int(t) {
                    case -3 ... -1:
                        changeTimeShift(true, frame: abs(t))
                        break
                    case 1...3:
                        changeTimeShift(false, frame: t)
                        break
                    default:
                        break
                    }
                }

                /* swipe & time shift */
                if let s: Int8 = swipe, let t: Int8 = time {
                    switch Int(t) {
                    case -3 ... -1:
                        switch Int(s) {
                        case -3 ... -1: /* left & left */
                            changeTimeShift(true, direct: "left", frame: abs(t + s)/2)
                            break
                        case 1...3: /* right & left*/
                            changeTimeShift(true, direct: "right", frame: (abs(t) + s)/2)
                            break
                        default:
                            break
                        }
                    case 1...3:
                        switch Int(s) {
                        case -3 ... -1: /* left & right*/
                            changeTimeShift(false, direct: "left", frame: (t + abs(s))/2)
                            break
                        case 1...3:  /* right & right */
                            changeTimeShift(false, direct: "right", frame: (t+s)/2)
                            break
                        default:
                            break
                        }
                    default:
                        break
                    }
                }
                                                                
                Thread.sleep(forTimeInterval: interval)
            } while true
        }
    }
}

// MARK: - Progress Delegate
extension ControlPlayerView: ProgressViewDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, let btn = progressView.btSwitch {
            return !view.isDescendant(of: btn)
        }
        if let view = touch.view, let ctrl = swipePad {
            return !view.isDescendant(of: ctrl)
        }
        if let view = touch.view, let ctrl = timePad {
            return !view.isDescendant(of: ctrl)
        }

        return true
    }
    
    @objc func sliderMoveBegin(_ slider: UISlider) {
        invalidateTimer()
        if fdPlayer.state != FD_STATE_PLAY && fdPlayer.state != FD_STATE_PAUSE {
            Log.message(to: "It is not playeing or pause.")
            return
        }
        lastPauseStatus = progressView.btPause.isSelected
        onControlPuase()
    }
    
    @objc func sliderMoveEnd(_ slider: UISlider, time: Int, base: Float) {
        Log.message(to: "\(slider) time: \(time)")
        //print("\(slider) time: \(time)")
        fdPlayer.seek(time)
        beginTimer()
        if !lastPauseStatus {
            progressView.btPause.isSelected = false
            onPlay()
        }
    }
    
    @objc func progressFinish(_ slider: UISlider) {
    }
}

// MARK: - FDLivePlayer Delegate
extension ControlPlayerView {

    override func getCurrentPlayInfo(_ player:FDLivePlayer,
                            channel: Int32,
                            frame: Int32,
                            frameCycle: Int32,
                            time: Int32,
                            utc: String,
                            type: Int32,
                            timeshiftSec: Int32) {
//        Log.message(to: "\(String(describing: self)) channel: \(channel), frame: \(frame) " +
//            "cycle: \(frameCycle) time: \(time) tuc: \(utc)")
        #if DEBUG
        
        var state = "\(player.state.rawValue)"
        if player.state == FD_STATE_PLAY {
            state = "PLYA: " + state
        }
        if player.state == FD_STATE_PAUSE {
            state = "PAUSE: " + state
        }
        
        let aString = "  channel: \(channel) \n  frame: \(frame) \n" +
            "  cycle: \(frameCycle) \n  time: \(time) \n  tuc: \(utc)  \n state: \(state)"
        DispatchQueue.main.async {
            self.lbStreamInfo.text = aString
        }
        #endif
        switch self.type {
        case .vod:
            DispatchQueue.main.async {
                self.progressView.updatePlayTime(Int64(time))
            }
            break
        default:
            break
        }
        
        if self.currentChannel != channel {
            Log.message(to: "\(#function) current channle:\(channel)")
            self.currentChannel = channel
            self.rxChannel.onNext(Int(channel))
        }
        currentPlayInfo(channel, frame: frame, frameCycle: frameCycle, time: time)
    }
        
    override func getVideoStreamInfo(_ player:FDLivePlayer,
                                     width: Int32,
                                     height: Int32,
                                     duration: Int32,
                                     videoCodec: String,
                                     audioCodec: String) {
        Log.message(to: "\(#function) width: \(width) height: \(height) duration: \(duration).")
        if self.type == .vod {
            progressView.duration = Int64(duration)
        }
        videoStreamInfo(width, height: height, duration: duration)
    }
    
    override func getStart(_ player:FDLivePlayer, code: Int32) {
        Log.message(to: "\(#function) code:\(code)")
        DispatchQueue.main.async {
            self.beginTimer()
        }
    }
    
    override func getError(_ player:FDLivePlayer, code: Int32, message: String) {
        Log.message(to: " error: \(code) - \(message)")
        closeIndicator()
        switch Int(code) {
        case 2200...2205:
            showMessageAlert(message: message, title: "")
            showReopenButton(true)
            break
        case 2300...2301:
            // end label 문구 출력.main live stream 에서만 사용한다.
            showReopenButton(true)
            break
        case Int(FD_ERR_NET_5000.rawValue):
            break
        default:
            showMessageAlert(message: message, title: "")
            break
        }
    }
    
    override func getPlayDone(_ player:FDLivePlayer) {
        Log.message(to: "\(#function)")
        if player.isLoop != true {
            DispatchQueue.main.async { [weak self] in
                self?.lbEnd.isHidden = false
                self?.fdPlayer.streamClose()
            }
        }
    }
}
