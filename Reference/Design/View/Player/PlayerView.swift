/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit
import RxCocoa
import RxSwift

enum StreamType: Int {
    case unknown = 0
    case live
    case vod
    case landscape
}

class PlayerView: BaseView, UIGestureRecognizerDelegate {
        
    private var distanceX: CGFloat = 0.0
    private var distanceY: CGFloat = 0.0
    private var streamUrl: String?
    
    internal var btReopen = UIButton()        
    internal var _type: StreamType = .unknown
    internal var _isLandscape = false
    internal var _isInteractive = true
    internal var toastTimer: Timer?
    internal var pauseComplete: (() -> Void)?
    internal var closeComplete: (() -> Void)?
    
    var isTimeShift = false
    var isPanGesture = false
    var fdPlayer: FDLivePlayer!
    var currentChannel: Int32 = 0 // O base
    var rxChannel = PublishSubject<Int>()
    var rxState = PublishSubject<FD_PLAYER_STATE>()
            
    lazy var indicator: UIActivityIndicatorView = {
        var view = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        view.color = .white
        self.addSubview(view)
        view.snp.makeConstraints {[weak self] ctrl in
            if let aView = self {
                ctrl.center.equalTo(aView)
            }
        }
        return view
    }()
        
    lazy var vBlock: UIView = {
        var view = UIView.init(frame: self.bounds)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        self.addSubview(view)
        view.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.top.bottom.left.right.equalTo(weakSelf).offset(0)
            }
        }
        return view
    }()
    
    lazy var lbEnd: UILabel = {
        /* stream end lable */
        let label = UILabel.init()
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.text = "The broadcast has ended."
        label.textColor = UIColor.white
        self.addSubview(label)
        label.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.center.equalTo(weakSelf)
            }
        }
        return label
    }()
    
    // MARK: - Life Cycle
    
    deinit {
        fdPlayer.streamClose()
        fdPlayer = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
    }
    
    override func initVariable() {
        self.backgroundColor = .black
        
        /* FDLPlayer */
        fdPlayer = FDLivePlayer.init()
        fdPlayer.delegate = self
        fdPlayer.isLoop = true
        let width = UIScreen.width
        let height = width * (9.0/16.0)
        let rect = CGRect(x: 0.0, y: 0.0, width: width, height: height)
        fdPlayer.playerView.frame = rect // for simulator
        fdPlayer.playerView.backgroundColor = UIColor.colorRGB(30.0, 30.0, 30.0)
        fdPlayer.addObserver(self, forKeyPath: "state", options: .new, context: nil)
        self.insertSubview(fdPlayer.playerView, at: 0)
        
        let normal = UIImage(named: "icon_refresh_player_normal")
        btReopen.backgroundColor = UIColor.clear
        btReopen.setBackgroundImage(normal, for: UIControl.State.normal)
        btReopen.addTarget(self,
                           action: #selector(reopenButttonTouchUpInside(_:)),
                           for: UIControl.Event.touchUpInside)
        btReopen.isHidden = true
        self.addSubview(btReopen)
    }
    
    override func setupLayout() {
        /*
        let panRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(panGestureHandler(_: )))
        self.addGestureRecognizer(panRecognizer)
        panRecognizer.delegate = self
        */
        fdPlayer.playerView.snp.makeConstraints { view in
            view.top.bottom.left.right.equalTo(self)
        }
        
        btReopen.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.center.equalTo(weakSelf)
            }
        }
    }
    
    // MARK: - Pan Gesture
    @objc func panGestureHandler(_ recognizer: UIPanGestureRecognizer) {
        if isPanGesture != true { return }
        let distance = recognizer.translation(in: self)
        switch recognizer.state {
        case .changed:
            recognizer.cancelsTouchesInView = false
            let valueX = abs(distance.x)
            let valueY = abs(distance.y)
            (valueY > valueX) ? panGestureUpDown(distance: distance)
                : panGestureSwipe(distance: distance)
            break
        case .ended:
            distanceX = 0.0;
            distanceY = 0.0;
            break
        default:
            break
        }
    }
    
    func panGestureSwipe(distance: CGPoint) {
        let value = distance.x - distanceX
        let absolute = abs(value)
        if absolute > 5 {
            value > 0 ? changeChannel(isTimeShift, direct: "right", frame: 1)
                : changeChannel(isTimeShift, direct: "left", frame: 1)
        }
        distanceX = distance.x
    }
    
    func panGestureUpDown(distance: CGPoint) {
        if isTimeShift == true {
            let value = distance.y - distanceY
            let absolute = abs(value)
            if absolute > 5 { /* down : up */
                changeTimeShift((distance.y > 0), frame: 1)
            }
        }
    }
    
    // MARK: - Stream open    
    func setupStream(_ baseUrl: String?, group: String?, target: String?) -> String? {
        var retUrl: String? = nil
        if var aUrl = baseUrl {
            if  let gp = group, let tg = target {
                aUrl = aUrl + "&group=\(gp)"
                aUrl = aUrl + "&target=\(tg)"
            }
            retUrl = aUrl
            if aUrl.contains("type=live") {
                _type = .live
            } else if aUrl.contains("type=vod") {
                _type = .vod
            }
        }
        
        return retUrl
    }
            
    @discardableResult
    func openStream(_ baseUrl: String? = nil, group: String? = nil, target: String? = nil, interactive: Bool = true) -> String? {
        let retUrl = setupStream(baseUrl, group: group, target: target)
        onOpen(retUrl, interactive: interactive)
        return retUrl
    }

    @discardableResult
    private func onOpen(_ url: String?, interactive: Bool = true) -> String? {
        var aUrl: String? = nil
        if url == nil {
            aUrl = streamUrl
        } else {
            aUrl = url
            self.streamUrl = url
        }
        if let strUrl = aUrl {
            let bUrl = NSURL(string: strUrl)
            let strIP = "http:\(bUrl?.host ?? "")" /* live server address */
            let port = 7070 /* Server port number */
            
            var ret = -1
            self.showIndicator()
            #if DEBUG
            ret = Int(fdPlayer.streamOpen(strUrl, isTCP: true, isHWAccel: false));
            #else
            ret = Int(fdPlayer.streamOpen(strUrl, isTCP: true, isHWAccel: interactive));
            #endif
            if ret != 0 {
                self.closeIndicator()
            }
            print("rtsp address: \(String(describing: strUrl)) ret: \(ret)")
            ret = Int(fdPlayer.restFulOpen(strIP, port: port))
        } else {
            print("\(#function) is not exist stream url.")
        }
        
        return aUrl
    }
    
    func streamClose() {
        fdPlayer.streamClose()
    }
    
    func streamReopen() {
        self.closeComplete = { [weak self] in
            _ = self?.onOpen(nil)
        }
        fdPlayer.streamClose()
    }
    
    func play() {
        fdPlayer.play()
    }
    
    func playToNow() {
        fdPlayer.playToNow()
    }
    
    func pause() {        
        fdPlayer.pause()
    }
    
    // MARK: - Change chnnel & TimeShift
    func changeChannel(_ isShift: Bool, direct: String, frame: Int8) {
        _ = isShift ? fdPlayer.setChangeFrameCh("pause", direction:direct, moveFrame:Int(frame))
            : fdPlayer.setChangeChannel("normal", direction: direct, moveFrame:Int(frame))
    }
    
    func changeTimeShift(_ isRewind: Bool, direct: String = "stop", frame: Int8) {
        _ = isRewind ? fdPlayer.setChangeFrameCh("rewind", direction:direct, moveFrame:Int(frame))
            : fdPlayer.setChangeFrameCh("forward", direction:direct, moveFrame:Int(frame))
    }
        
    // MARK: - Button Action
    @objc func reopenButttonTouchUpInside(_ sender: Any?) {
        self.closeComplete = { [weak self] in
            DispatchQueue.main.async {
                _ = self?.onOpen(nil)
                self?.showIndicator()
                if let button = sender as? UIButton {
                    button.isHidden = true
                }
            }
        }
        fdPlayer.streamClose()
    }
    
    func showReopenButton(_ isShow: Bool) {
        DispatchQueue.main.async { [weak self] in
            if let button  = self?.btReopen {
                button.isHidden = !isShow
                if isShow == true {
                    self?.bringSubviewToFront(button)
                }
            }
        }
    }
}

extension PlayerView {
    
    func showIndicator() {
        DispatchQueue.main.async {
            self.vBlock.isHidden = false
            self.indicator.isHidden = false
            self.bringSubviewToFront(self.vBlock)
            self.bringSubviewToFront(self.indicator)
            self.indicator.startAnimating()
        }
    }

    func closeIndicator() {
        DispatchQueue.main.async {
            self.vBlock.isHidden = true
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
        }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func showMessageAlert(message: String, title: String) {
        DispatchQueue.main.async {
            let controller = UIAlertController.init(title: title ,
                                                    message: message ,
                                                    preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) {(action) in
            }
            controller.addAction(okAction)
            if let parent = self.parentViewController {
                parent.present(controller, animated: true, completion: nil)
            }
        }
    }
}

extension PlayerView: FDPlayerDelegate {
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        if keyPath == "state" {
            let value = change?[.newKey]
            Log.message(to: "\(#function) state: \(fdPlayer.state) value: \(String(describing: value))")
            switch fdPlayer.state {
            case FD_STATE_NONE:
                Log.message(to: "\(#function) FD_STATE_NONE")
                print("\(#function) FD_STATE_NONE")
                break
            case FD_STATE_OPENING:
                print("\(#function) FD_STATE_OPENING")
                break
            case FD_STATE_OPEN:
                print("\(#function) FD_STATE_OPEN")
                break
            case FD_STATE_PLAY:
                print("\(#function) FD_STATE_PLAY")
                break
            case FD_STATE_PAUSE:
                print("\(#function) FD_STATE_PAUSE")
                break
            case FD_STATE_EOF:
                print("\(#function) FD_STATE_EOF")
                break
            case FD_STATE_CLOSING:
                print("\(#function) FD_STATE_CLOSING")
                break
            case FD_STATE_CLOSE:
                print("\(#function) FD_STATE_CLOSE")
                break
            case FD_STATE_ERROR:
                print("\(#function) FD_STATE_ERROR")
                break
            default:
                break
            }
            rxState.onNext(fdPlayer.state)
        }
    }
    
    func getCurrentPlayInfo(_ player:FDLivePlayer,
                            channel: Int32,
                            frame: Int32,
                            frameCycle: Int32,
                            time: Int32,
                            utc: String,
                            type: Int32,
                            timeshiftSec: Int32) {
        if self.currentChannel != channel {
            self.currentChannel = channel
            Log.message(to: "\(#function) current channel: \(self.currentChannel)")
        }
    }
    
    func getVideoStreamInfo(_ player:FDLivePlayer,
                            width: Int32,
                            height: Int32,
                            duration: Int32,
                            videoCodec: String,
                            audioCodec: String) {
    }
    
    func getStart(_ player:FDLivePlayer, code: Int32) {
        print("\(#function) code:\(code)")
    }
    
    func getStop(_ player:FDLivePlayer, code: Int32) {
        if code == 0 {
            if let handler = self.closeComplete {
                handler()
            }
        }
    }
    
    func getPlay(_ player:FDLivePlayer) {
        self.closeIndicator()
        self.showReopenButton(false)
        print("\(#function)")
    }
    
    func getPause(_ player:FDLivePlayer) {
        if let handler = self.pauseComplete {
            handler()
        }
    }
    
    func getPlayDone(_ player:FDLivePlayer) {
        Log.message(to: "\(#function)")
        if player.isLoop != true {
            self.showReopenButton(true)            
        }
    }
    
    func getError(_ player:FDLivePlayer, code: Int32, message: String) {
        print("\(#function) error: \(code) - \(message)")
        self.closeIndicator()
        switch Int(code) {
        case 2200...2205:
            self.showReopenButton(true)
            break
//        case 2300...2301:
//            // end label 문구 출력.main live stream 에서만 사용한다.
//            self.showReopenButton(true)
//            break
        case Int(FD_ERR_NET_5000.rawValue):
            break
        default:
            showMessageAlert(message: message, title: "")
            break
        }
    }
        
}
