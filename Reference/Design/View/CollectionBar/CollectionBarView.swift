//
//  CollectionBarView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit


enum CategoryType: String {
    case all = "icoAll"
    case baseball = "icoBaseball"
    case golf = "icoGolf"
    case baseketball = "icoBasketball"
    case icehockey = "icoIcehockey"
    case soccer = "icoSoccer"
    case handball = "icoHandball"
    case fieldhockey = "icoFieldhockey"
    case volleyball = "icoVolleyball"
    case tennis = "icoTennis"
    case badminton = "icoBadminton"
    case ufc = "icoUfc"
    case taekwondo = "icoTaekwondo"
    case judo = "icoJudo"
    case wrestling = "icoWrestling"
    case athletics = "icoAthletics"
    case climbing = "icoSportclimbing"
    case bmx = "icoBmx"
    case gymnastics = "icoGymnastics"
    
    var name: String {
        switch self {
        case .all:
            return "ALL"
        case .baseball:
            return "Baseball"
        case .golf:
            return "Golf"
        case .baseketball:
            return "Baseketball"
        case .icehockey:
            return "Icehockey"
        case .soccer:
            return "Soccer"
        case .handball:
            return "Handball"
        case .fieldhockey:
            return "Fieldhockey"
        case .volleyball:
            return "Volleyball"
        case .tennis:
            return "Tennis"
        case .badminton:
            return "Badminton"
        case .ufc:
            return "UFC"
        case .taekwondo:
            return "Taekwondo"
        case .judo:
            return "Judo"
        case .wrestling:
            return "Wrestling"
        case .athletics:
            return "Athletics"
        case .climbing:
            return "Climbing"
        case .bmx:
            return "BMX"
        case .gymnastics:
            return "Gymnastics"
        }
    }
}


class CollectionBarView: UIView {

    let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    var categoryList: [CategoryModel]? {
        didSet {
            self.collectionView.reloadData()
            
            DispatchQueue.main.async {
                let first = IndexPath(row: 0, section: 0)
                self.collectionView.selectItem(at: first, animated: false, scrollPosition: .top)
            }
            
        }
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    public init() {
        super.init(frame: .zero)
        setComponent()
        setAutolayOut()
    }
    
}

extension CollectionBarView {
    private func setComponent() {
        backgroundColor = .clear
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 18.0
            flowLayout.minimumInteritemSpacing = 0.0

        }
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.bounces = false
        collectionView.register(cellType: BarCollectionViewCell.self)
        collectionView.backgroundColor = .clear

        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.isScrollEnabled = true
        collectionView.bounces = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = true

        self.addSubview(collectionView)
        collectionView.setLayerBorder()
                
    }
    
    private func setAutolayOut() {
        collectionView.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.right.top.bottom.equalTo(self)
        }
    }

    
    
}


extension CollectionBarView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let list = categoryList else {
            return 0
        }
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let list = categoryList else {
            return UICollectionViewCell()
        }

        let info: CategoryModel = list[indexPath.row]
        
//        let type: CategoryType = list[indexPath.row]
        
        let cell: BarCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath, cellType: BarCollectionViewCell.self)
        cell.indexRow = indexPath.row
        cell.info = info
        cell.setLayerBorder()
        return cell

        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension CollectionBarView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        guard let list = categoryList else {
//            return CGSize(width: 0.0, height: 0.0)
//        }
//
//        let dic: Any = list[indexPath.row]
        
        return CGSize(width: 64.0, height: 102.0)

        
    }
    
}
