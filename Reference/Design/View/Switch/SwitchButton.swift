//
//  SwieeftSwitchButton.swift
//  SwieeftSwitch
//
//  Created by 1 on 14/02/2020.
//  Copyright © 2020 swieeft. All rights reserved.
//

import UIKit

@objc protocol SwitchButtonDelegate: AnyObject {
    @objc func isOnValueChange(isOn: Bool)
}

class SwitchButton: UIButton {
    typealias SwitchColor = (bar: UIColor, circle: UIColor, text: UIColor)

    private var barView: UIView!
    private var circleView: UIView!
    private var lbText: UILabel!

    var isOn: Bool = false {
        didSet {
            self.changeState()
        }
    }

    /* on */
    var onColor: SwitchColor = (#colorLiteral(red: 0.9960784314, green: 0.9058823529, blue: 0.9058823529, alpha: 1), #colorLiteral(red: 0.7506764531, green: 0.7506943345, blue: 0.7506847978, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)) {
        didSet {
            if isOn {
                self.barView.backgroundColor = self.onColor.bar
                self.circleView.backgroundColor = self.onColor.circle
                self.lbText.textColor = self.onColor.text
            }
        }        
    }

    /* off */
    var offColor: SwitchColor = (#colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1), #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)) {
        didSet {
            if isOn == false {
                self.barView.backgroundColor = self.offColor.bar
                self.circleView.backgroundColor = self.offColor.circle
                self.lbText.textColor = self.offColor.text
            }
        }
    }

    var animationDuration: TimeInterval = 0.25
    private var isAnimated: Bool = false
    var barViewTopBottomMargin: CGFloat = 5
    
    @IBOutlet weak var delegate: SwitchButtonDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.buttonInit(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.buttonInit(frame: frame)
    }

    private func buttonInit(frame: CGRect) {

        let barViewHeight = frame.height - (barViewTopBottomMargin * 2)

        barView = UIView(frame: CGRect(x: 0, y: barViewTopBottomMargin, width: frame.width, height: barViewHeight))
        barView.backgroundColor = self.offColor.bar
        barView.layer.masksToBounds = true
        barView.layer.cornerRadius = barViewHeight / 2
        self.addSubview(barView)

        circleView = UIView(frame: CGRect(x: 0, y: 0, width: frame.height, height: frame.height))
        circleView.backgroundColor = self.offColor.circle
        circleView.layer.masksToBounds = true
        circleView.layer.cornerRadius = frame.height / 2
                
        lbText = UILabel(frame: circleView.bounds)
        lbText.textAlignment = .center
        lbText.text = "A"
        circleView.addSubview(lbText)
        self.addSubview(circleView)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.setOn(on: !self.isOn, animated: true)
    }

    func setOn(on: Bool, animated: Bool) {
        self.isAnimated = animated
        self.isOn = on
    }

    private func changeState() {

        var circleCenter: CGFloat = 0
        var barViewColor: UIColor = .clear
        var circleViewColor: UIColor = .clear
        var textColor: UIColor = .clear

        if self.isOn {
            circleCenter = self.frame.width - (self.circleView.frame.width / 2)
            barViewColor = self.onColor.bar
            circleViewColor = self.onColor.circle
            textColor = self.onColor.text
        } else {
            circleCenter = self.circleView.frame.width / 2
            barViewColor = self.offColor.bar
            circleViewColor = self.offColor.circle
            textColor = self.offColor.text
        }

        let duration = self.isAnimated ? self.animationDuration : 0

        UIView.animate(withDuration: duration, animations: { [weak self] in
            guard let self = self else { return }

            self.circleView.center.x = circleCenter
            self.barView.backgroundColor = barViewColor
            self.circleView.backgroundColor = circleViewColor
            self.lbText.textColor = textColor

        }) { [weak self] _ in
            guard let self = self else { return }

            self.delegate?.isOnValueChange(isOn: self.isOn)
            self.isAnimated = false
        }
    }
}
