//
//  ToastView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/23.
//

import UIKit
import RxSwift
import RxCocoa

class ToastView: UIView {

    private(set) var disposeBag = DisposeBag()
    let button: UIButton = UIButton()
    let label: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    public init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        setComponent()
    }

}

extension ToastView {
    
    private func setComponent() {
        
        let imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named: "iconNoticeNormal")
        self.addSubview(imageView)
        imageView.setLayerBorder()
                
        imageView.snp.makeConstraints { view -> Void in
            view.top.equalTo(self).offset(6.0)
            view.left.equalTo(self).offset(8.0)
            view.width.height.equalTo(32.0)
        }
        
        
        button.setBackgroundImage(UIImage(named: "iconNoticeCloseNormal"), for: .normal)
        self.addSubview(button)
        button.setLayerBorder()
        

        
        button.snp.makeConstraints { view -> Void in
            view.top.equalTo(self).offset(6.0)
            view.right.equalTo(self).offset(-8.0)
            view.width.height.equalTo(32.0)
        }
        
        
        label.font = .systemFont(ofSize: 14.0)
        label.text = "Sorry, we are currently adjusting the screen and calibra.."
        label.textColor = .black
        label.textAlignment = .left
        self.addSubview(label)
        label.setLayerBorder()
        
        label.snp.makeConstraints { view -> Void in
            view.left.equalTo(imageView.snp.right)
            view.right.equalTo(button.snp.left)
            view.top.equalTo(self).offset(14.0)
            view.bottom.equalTo(self).offset(-14.0)
        }
        
    }
        
    public func remove() {
        self.removeFromSuperview()
    }
    
}
