//
//  TableTitleView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/09.
//

import Foundation

class TableTitlteView: BaseView {
    
    var lbTitle: UILabel!
    var ivStatus: UIImageView!
    var lbContent: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLayout()
    }
    
    override func setupLayout() {
        /* live image */
        ivStatus = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 92, height: 22)) //tempoaray
        self.addSubview(ivStatus)
        ivStatus.setLayerBorder()
        ivStatus.snp.makeConstraints { view in
            view.left.equalTo(self).offset(16)
            view.centerY.equalTo(self).offset(11)
        }
        /* title */
        lbTitle = UILabel.init()
        lbTitle.font = UIFont.systemFont(ofSize: 15.0)
        lbTitle.textColor = .lightGray
        lbTitle.setLayerBorder()
        self.addSubview(lbTitle)
        lbTitle.snp.makeConstraints { label in
            label.left.equalTo(ivStatus)
            label.bottom.equalTo(ivStatus).offset(-30)
        }
        /* content */
        lbContent = UILabel.init()
        lbContent.font = UIFont.systemFont(ofSize: 20.0)
        lbContent.textColor = UIColor.black
        self.addSubview(lbContent)
        lbContent.setLayerBorder()
        lbContent.snp.makeConstraints { label in
            label.left.equalTo(ivStatus.snp.right).offset(8)
            label.centerYWithinMargins.equalTo(ivStatus)
        }
    }
}
