//
//  ContentTitleView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/08.
//

import UIKit

enum NavigationType: Int {
    case title = 0
    case subTitle = 1
}

class CotentTitleView: BaseView {
 
    var btPrvious: UIButton = UIButton()
    var lbTitle: UILabel = UILabel()
    lazy var lbSubTitle: UILabel = {
        let label = UILabel()
        label.textColor = .colorRGBA(0, 0, 0, 0.5)
        label.font = .PoppinsMedium(ofSize: 10.0)
        label.textAlignment = .center
        return label
    }()
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    public init(type: NavigationType) {
        super.init(frame: .zero)
        setComponent(type: type)
        setAutoLayout(type: type)
    }
    

    private func setComponent(type: NavigationType) {
        btPrvious.backgroundColor = .clear
//        btPrvious.setBackgroundImage(UIImage(named: "icoBack"), for: .normal)
//        btPrvious.setBackgroundImage(UIImage(named: "icoBack_press"), for: .highlighted)
        
        btPrvious.setImage(UIImage(named: "icoBack"), for: .normal)
        btPrvious.setImage(UIImage(named: "icoBack_press"), for: .highlighted)        

        btPrvious.addTarget(self, action: #selector(buttonTouchUpInside(_:)), for: .touchUpInside)
        self.addSubview(btPrvious)
        btPrvious.setLayerBorder()
        
        if type == .subTitle {
            self.addSubview(lbSubTitle)
            lbSubTitle.setLayerBorder()
        }

        /* subtitle label */
        lbTitle.textColor = .black
        lbTitle.textAlignment = .center
        lbTitle.font = .PoppinsMedium(ofSize: 16.0)
        self.addSubview(lbTitle)
        lbTitle.setLayerBorder()

    }
    
    private func setAutoLayout(type: NavigationType) {
        btPrvious.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.top.equalTo(weakSelf).offset(4.0)
                btn.left.equalTo(weakSelf).offset(2.0)
                btn.width.height.equalTo(40.0)
            }
        }

        lbTitle.snp.makeConstraints { [weak self] label in
            if let weakSelf = self {
                label.left.equalTo(weakSelf).offset(52.0)
                label.right.equalTo(weakSelf).offset(-52.0)
                label.height.equalTo(23.0)
                label.centerY.equalTo(weakSelf)
            }
        }

        if type == .subTitle {
            lbSubTitle.snp.makeConstraints { [weak self] label in
                if let weakSelf = self {
                    label.centerX.equalTo(weakSelf)
                    label.height.equalTo(15.0)
                    label.bottom.equalTo(lbTitle.snp.top)
                }
            }
        }
    }
    
    
    
    @objc func buttonTouchUpInside(_ sender: Any?) {
    }
}
