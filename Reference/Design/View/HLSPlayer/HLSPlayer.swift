//
//  HLSPlayer.swift
//  Reference
//
//  Created by 4Dreplay on 2021/07/14.
//

import UIKit
import AVKit

class HLSPlayerView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
 
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        
        set {
            playerLayer.videoGravity = .resizeAspect
            playerLayer.player = newValue
        }
    }
    
    var url: URL? {
        didSet {
            guard let u = url else {
                return
            }
            let avPlayer = AVPlayer(url: u)
            self.player = avPlayer
            self.player?.play()            
        }
    }
    
}
