//
//  BaseballBirdView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/28.
//

import RxCocoa
import RxSwift

enum BaseballPostion: Int {
    case batter = 1
    case peacher
    case firstBase
    case secondBase
    case thirdBase
    case over
}

class BaseballBirdView: BirdView {
    
    let disposeBag: DisposeBag = DisposeBag()
    
    var ivBackGround = UIImageView()
    var btPoint01 = UIButton()
    var btPoint02 = UIButton()
    var btPoint03 = UIButton()
    var btPoint04 = UIButton()
    var btPoint05 = UIButton()
    var btOver = UIButton()
    
    var position: BaseballPostion = .batter
    var rxPosition = PublishSubject<BaseballPostion>()
        
    public init(type: BirdType = .normal) {
        super.init(frame: .zero)
        birdType = type
        initVariable()
        setupLayout()
        setCameraButton()
        setRXFunction()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initVariable()
        setupLayout()
        setCameraButton()
        setRXFunction()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initVariable()
        setupLayout()
        setCameraButton()
        setRXFunction()
    }
    
    override func initVariable() {
        //if let image = UIImage(named: "img_ground_null") {
        let image = UIImage(named: "img_birdview_origin") // for test
        ivBackGround.image = image
        self.addSubview(ivBackGround)
                
        switch birdType {
        case .normal:
            let on = UIImage(named: "icon_birdview_on")
            let off = UIImage(named: "icon_birdview_off")
            btToggle.setBackgroundImage(on, for: .normal)
            btToggle.setBackgroundImage(off, for: .selected)
            setRXToggle()
            break
        case .pip:
            let image = UIImage(named: "icon_close")
            btClose.setBackgroundImage(image, for: .normal)
            break
        }
    }
    
    override func setupLayout() {
        ivBackGround.snp.makeConstraints { view in
            view.left.right.top.bottom.equalTo(self)
        }
    }
    
    private func setCameraButton() {
        var btWidth = getRelativeContraintValue(38.0)
        var btHeight = getRelativeContraintValue(28.0)
        if btWidth < 35 { btWidth = 35 }
        if btHeight < 25 { btHeight = 25 }
        
        var normal = UIImage(named: "img_location")
        var selected = UIImage(named: "img_location_slected")
        
        btPoint01.tag = (BaseballPostion.batter).rawValue
        btPoint01.setBackgroundImage(normal, for: .normal)
        btPoint01.setBackgroundImage(selected, for: .selected)
        btPoint01.isSelected = true
        self.addSubview(btPoint01)
        btPoint01.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.width.equalTo(btWidth)
                btn.height.equalTo(btHeight)
                btn.centerX.equalTo(weakSelf)
                let offset = getRelativeContraintValue(-14.0)
                btn.bottom.equalTo(weakSelf).offset(offset)
            }
        }

        btPoint02.tag = (BaseballPostion.peacher).rawValue
        btPoint02.setBackgroundImage(normal, for: .normal)
        btPoint02.setBackgroundImage(selected, for: .selected)
        self.addSubview(btPoint02)
        btPoint02.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.width.equalTo(btWidth)
                btn.height.equalTo(btHeight)
                btn.centerX.equalTo(weakSelf)
                let offset = getRelativeContraintValue(-6.0)
                btn.bottom.equalTo(btPoint01.snp.top).offset(offset)
            }
        }
        
        btPoint03.tag = (BaseballPostion.firstBase).rawValue
        btPoint03.setBackgroundImage(normal, for: .normal)
        btPoint03.setBackgroundImage(selected, for: .selected)
        self.addSubview(btPoint03)
        btPoint03.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.width.equalTo(btWidth)
                btn.height.equalTo(btHeight)
                let right =  getRelativeContraintValue(-46.0)
                let centerY = getRelativeContraintValue(40.0)
                btn.right.equalTo(weakSelf).offset(right)
                btn.centerY.equalTo(weakSelf).offset(centerY)
            }
        }
        
        btPoint04.tag = (BaseballPostion.secondBase).rawValue
        btPoint04.setBackgroundImage(normal, for: .normal)
        btPoint04.setBackgroundImage(selected, for: .selected)
        self.addSubview(btPoint04)
        btPoint04.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.width.equalTo(btWidth)
                btn.height.equalTo(btHeight)
                btn.centerX.equalTo(weakSelf)
                let offset = getRelativeContraintValue(-6.0)
                btn.bottom.equalTo(btPoint02.snp.top).offset(offset)
            }
        }
        
        btPoint05.tag = (BaseballPostion.thirdBase).rawValue
        btPoint05.setBackgroundImage(normal, for: .normal)
        btPoint05.setBackgroundImage(selected, for: .selected)
        self.addSubview(btPoint05)
        btPoint05.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                btn.width.equalTo(btWidth)
                btn.height.equalTo(btHeight)
                let right =  getRelativeContraintValue(46.0)
                let centerY = getRelativeContraintValue(40.0)
                btn.left.equalTo(weakSelf).offset(right)
                btn.centerY.equalTo(weakSelf).offset(centerY)
            }
        }
        
        normal = UIImage(named: "img_location_overview_normal")
        selected = UIImage(named: "img_location_overview_selected")
        btOver.setBackgroundImage(normal, for: .normal)
        btOver.setBackgroundImage(selected, for: .selected)
        btOver.tag = (BaseballPostion.over).rawValue
        self.addSubview(btOver)
        btOver.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var size = getRelativeContraintValue(46.0)
                if size < 30 { size = 30 }
                let offset = getRelativeContraintValue(12.0)
                btn.width.height.equalTo(size)
                btn.left.equalTo(weakSelf).offset(offset)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
    }
    
    private func setRXToggle() {
        btToggle.rx.tap
            .scan(false) { [weak self] (lastState, newValue) in
                if let weakSelf = self {
                    weakSelf.hiddenButton(!lastState)
                }
                return !lastState
            }
            .bind(to: btToggle.rx.isSelected)
            .disposed(by: disposeBag)
    }
    
    private func setRXFunction() {
        let buttons = [btPoint01, btPoint02, btPoint03, btPoint04, btPoint05, btOver].map{ $0! }
        let selectedButton = Observable.from(
            buttons.map { button in button.rx.tap.map { button } }
        ).merge()
        buttons.reduce(Disposables.create()) {disposable, button in
            let subscription = selectedButton.map { $0 == button }
            .bind(to: button.rx.isSelected)
            return Disposables.create(disposable, subscription)
        }
        .disposed(by: self.disposeBag)
        
        Observable.merge(
            btPoint01.rx.tap.map { _ in BaseballPostion.batter },
            btPoint02.rx.tap.map { _ in BaseballPostion.peacher },
            btPoint03.rx.tap.map { _ in BaseballPostion.firstBase },
            btPoint04.rx.tap.map { _ in BaseballPostion.secondBase },
            btPoint05.rx.tap.map { _ in BaseballPostion.thirdBase },
            btOver.rx.tap.map { _ in BaseballPostion.over}
        ).subscribe(onNext: {
            self.rxPosition.onNext($0)
            self.position = $0
            Log.message(to: "\(#function) postion: \($0)")
          }
        ).disposed(by: disposeBag)
    }
    
    func hiddenButton(_ isHidden: Bool) {
        if isHidden == true {
            btPoint01.fadeOut()
            btPoint02.fadeOut()
            btPoint03.fadeOut()
            btPoint04.fadeOut()
            btPoint05.fadeOut()
            btOver.fadeOut()
        } else {
            btPoint01.fadeIn()
            btPoint02.fadeIn()
            btPoint03.fadeIn()
            btPoint04.fadeIn()
            btPoint05.fadeIn()
            btOver.fadeIn()
        }
    }
    
    override func interactiveMode(_ isMode: Bool) {
        _isInteractive = isMode
        hiddenButton(!isMode)
        if birdType == .normal {
            btToggle.isHidden = !isMode
        }
    }
}
