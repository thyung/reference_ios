//
//  UFCBirdView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/23.
//

import RxCocoa
import RxSwift

class UFCBirdView: BirdView {
    
    let disposeBag: DisposeBag = DisposeBag()    
    var wheel = RotaryWheel(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
    var ivBackground = UIImageView()
    var ivCameraRill = UIImageView()
    
    private var distanceX: CGFloat = 0.0
    private var distanceY: CGFloat = 0.0
    
    var total = 40 // delegate channel 0 base , 채널 개수는 1base
    var current: Int = 0
        
    public init(type: BirdType = .normal) {
        super.init(frame: .zero)
        birdType = type
        initVariable()
        setupLayout()
        //setRXFunction()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initVariable()
        setupLayout()
        //setRXFunction()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initVariable()
        setupLayout()
        //setRXFunction()
    }
    
    override func initVariable() {
        self.playerView.backgroundColor = .darkGray
        self.addSubview(wheel)
        wheel.setLayerBorder()
        wheel.backgroundColor = .clear
                    
        switch birdType {
        case .normal:
            let bg = UIImage(named: "bird_view_portrait_null_normal")
            ivBackground.image = bg
            let ril = UIImage(named: "nav_camera_rill_normal")
            ivCameraRill.image = ril
            
            let on = UIImage(named: "icon_birdview_on")
            let off = UIImage(named: "icon_birdview_off")
            btToggle.setBackgroundImage(on, for: .normal)
            btToggle.setBackgroundImage(off, for: .selected)
            setRXFunction()
            break
        case .pip:
            let bg = UIImage(named: "bird_view_landscape_null")
            ivBackground.image = bg
            let ril = UIImage(named: "nav_camera_rill_landscape")
            ivCameraRill.image = ril
            let close = UIImage(named: "icon_close")
            btClose.setBackgroundImage(close, for: .normal)
            break
        }
        self.insertSubview(ivBackground, at: 0)
        wheel.insertSubview(ivCameraRill, at: 0)
    }
    
    override func setupLayout() {
        wheel.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.centerX.equalTo(weakSelf)
                view.top.equalTo(weakSelf).offset(10)
                view.bottom.equalTo(weakSelf).offset(-10)
                view.width.equalTo(wheel.snp.height)
            }
        }
        ivBackground.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.left.right.top.bottom.equalTo(weakSelf)
            }
        }
        ivCameraRill.snp.makeConstraints { view in
            view.left.right.top.bottom.equalTo(wheel)
        }
    }
    
    private func setRXFunction() {
        btToggle.rx.tap
            .scan(false) { [weak self] (lastState, newValue) in
                if let weakSelf = self {
                    weakSelf.hiddenControl(!lastState)
                }
                return !lastState
            }
            .bind(to: btToggle.rx.isSelected)
            .disposed(by: disposeBag)
    }
    
    func hiddenControl(_ isHidden: Bool) {
        isHidden ? wheel.fadeOut() : wheel.fadeIn()
    }
    
    override func interactiveMode(_ isMode: Bool) {
        _isInteractive = isMode        
        wheel.isHidden = !isMode
        if birdType == .normal {
            btToggle.isHidden = !isMode
        }    
    }
    
    override func updateCameraPosition(_ channel: Int) {
        let degree: CGFloat =  CGFloat.pi * 2 * (7.0/8.0) / CGFloat(total)
        let ret: CGFloat = degree * CGFloat(channel)
        //print("\(#function) degree: \(degree), ret: \(ret)")
        wheel.udateDegree(ret)
    }
}
