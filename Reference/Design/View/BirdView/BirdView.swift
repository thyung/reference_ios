//
//  BirdView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/15.
//

import RxCocoa
import RxSwift

enum ContentBirdType: Int {
    case normal = 0
    case baseball
    case ufc
    case basket
    case soccer
}

enum BirdType: Int {
    case normal = 0
    case pip
}

class BirdView: BaseView {
    var playerView = PlayerView()
    var birdType: BirdType = .normal
    lazy var btToggle: UIButton = {
        var button = UIButton()
        self.addSubview(button)
        button.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.right.equalTo(weakSelf).offset(-15)
                view.top.equalTo(weakSelf).offset(16)
            }
        }
        return button
    }()
    
    lazy var btClose: UIButton = {
        var button = UIButton()        
        self.addSubview(button)
        button.snp.makeConstraints {[weak self] view in
            if let weakSelf = self {
                view.right.equalTo(weakSelf).offset(-2)
                view.top.equalTo(weakSelf).offset(2)
            }
        }
        return button
    }()
    
    internal var _isInteractive = true
    var isInteractive: Bool {
        get { return _isInteractive }
        set (newval) {
            DispatchQueue.main.async { [weak self] in
                self?.interactiveMode(newval)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(playerView)
        playerView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.left.right.top.bottom.equalTo(weakSelf)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSubview(playerView)
        playerView.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.left.right.top.bottom.equalTo(weakSelf)
            }
        }
    }
    
    func openStream(_ baseUrl: String? = nil, group: String? = nil, target: String? = nil) {
        _ = playerView.openStream(baseUrl, group: group, target: target)
    }
    
    func streamClose() {
        playerView.fdPlayer.streamClose()
    }
    
    public func pause() {
        playerView.fdPlayer.pause()
    }
    
    public func playToNow() {
        switch playerView.fdPlayer.state {
        case FD_STATE_OPEN, FD_STATE_PLAY, FD_STATE_PAUSE:
            playerView.fdPlayer.playToNow()
            break;
        default:
           _ = playerView.openStream()
            break;
        }
        
    }
    
    func interactiveMode(_ isMode: Bool) {
        _isInteractive = isMode
    }
    
    func updateCameraPosition(_ channel: Int) {
        
    }
    
    internal func getRelativeContraintValue(_ constant: CGFloat, guide: CGFloat = 414.0) -> CGFloat {
        let width = (birdType == .pip) ? (UIScreen.height * (16 / 9) * 0.4) : UIScreen.width
        /* if pip use landscape. */
        let value =  (constant / guide) * width
        return value
    }    
}





