//
//  SoccorBirdView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/23.
//

import RxCocoa
import RxSwift

class SoccerBirdView: BirdView {
    
    let __MAX = 42
    let disposeBag: DisposeBag = DisposeBag()
    
    var ivBackGround = UIImageView()
    var ivRail = UIImageView()
    var btPoint01 = UIButton()
    var btPoint02 = UIButton()
    var btPoint03 = UIButton()
    var btPoint04 = UIButton()
    var btPoint05 = UIButton()
    var btPoint06 = UIButton()
    var btPoint07 = UIButton()
    var btPoint08 = UIButton()
    var btPoint09 = UIButton()
    var btPoint10 = UIButton()
    
    private var railImages: [UIImage] = []
    // TODO: 외부에서 입력 설정
    var leftIndex = 41
    var totalCount = 80
    var current = 0
    var channel = 0 // 1 base
    var rxChannel = PublishSubject<Int>()
    //var railIndex = 0    
        
    public init(type: BirdType = .normal) {
        super.init(frame: .zero)
        birdType = type
        initVariable()
        setupLayout()
        setRXFunction()
        setCameraButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initVariable()
        setupLayout()
        setCameraButton()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initVariable()
        setupLayout()
        setCameraButton()
    }
    
    override func initVariable() {
        setRailImages()
    
        if let image = UIImage(named: "soccer_controller") {
            ivBackGround.image = image
        }
        self.addSubview(ivBackGround)

        switch birdType {
        case .normal:
            let on = UIImage(named: "icon_birdview_on")
            let off = UIImage(named: "icon_birdview_off")
            btToggle.setBackgroundImage(on, for: .normal)
            btToggle.setBackgroundImage(off, for: .selected)
            setRXFunction()
            break
        case .pip:
            let image = UIImage(named: "icon_close")
            btClose.setBackgroundImage(image, for: .normal)
            break
        }

        ivRail.image = railImages.first
        ivRail.setLayerBorder()
        self.addSubview(ivRail)
    }
    
    private func setRailImages() {
        for i in 0..<__MAX  {
            if let image = UIImage(named: "soccer_rail_\(i+1)") {
                railImages.append(image)
            }
        }
    }
    
    override func setupLayout() {
        ivBackGround.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.left.right.top.bottom.equalTo(weakSelf)
            }
        }
        ivRail.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(19.0, guide: 390)
                view.left.equalTo(weakSelf).offset(offset)
                view.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(66.0, guide: 390)
                view.top.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(20.0, guide: 390)
                view.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
    }
    
    private func setRXFunction() {
        btToggle.rx.tap
            .scan(false) { [weak self] (lastState, newValue) in
                if let weakSelf = self {
                    weakSelf.hiddenControl(!lastState)
                }
                return !lastState
            }
            .bind(to: btToggle.rx.isSelected)
            .disposed(by: disposeBag)
    }
    
    func hiddenControl(_ isHidden: Bool) {
        isHidden ? ivRail.fadeOut() : ivRail.fadeIn()
        btPoint01.isHidden = isHidden
        btPoint02.isHidden = isHidden
        btPoint03.isHidden = isHidden
        btPoint04.isHidden = isHidden
        btPoint05.isHidden = isHidden
        btPoint06.isHidden = isHidden
        btPoint07.isHidden = isHidden
        btPoint08.isHidden = isHidden
        btPoint09.isHidden = isHidden
        btPoint10.isHidden = isHidden
    }
    
    override func interactiveMode(_ isMode: Bool) {
        _isInteractive = isMode
        ivRail.isHidden = !isMode
        btPoint01.isHidden = !isMode
        btPoint02.isHidden = !isMode
        btPoint03.isHidden = !isMode
        btPoint04.isHidden = !isMode
        btPoint05.isHidden = !isMode
        btPoint06.isHidden = !isMode
        btPoint07.isHidden = !isMode
        btPoint08.isHidden = !isMode
        btPoint09.isHidden = !isMode
        btPoint10.isHidden = !isMode
        if birdType == .normal {
            btToggle.isHidden = !isMode
        }
    }

    override func updateCameraPosition(_ channel: Int) {
        if channel < 1 { return }
        
        var index = current
        switch channel {
        case 1:
            index = 0
            break
        case 2..<(leftIndex - 2):
            let interval:Float = Float(totalCount) / 42.0
            index = Int(Float(channel) / interval) + 1
            if index < 1 { index = 1 }
            let lastIndex = leftIndex - 2
            if channel < lastIndex && index >= 21 {
                index = 20
            }
            break
        case leftIndex - 1:
            index = 21
            break
        case leftIndex..<totalCount:
            let interval:Float = Float(totalCount) / 42.0
            index = Int(Float(channel) / interval) + 1
            if index > railImages.count - 2 {
                index = railImages.count - 2
            }
            break
        case totalCount:
            index = railImages.count - 1
            break
        default:
            if totalCount < channel {
                index = railImages.count - 1
            }
            break
        }
        
        DispatchQueue.main.async {
            self.ivRail.image = self.railImages[index]
            self.current = index
            //print("\(#function) channel: \(channel) index: \(index)")
        }
    }
}

extension SoccerBirdView {
    private func setCameraButton() {
        btPoint01.tag = 1
        btPoint01.setLayerBorder()
        self.addSubview(btPoint01)
        btPoint01.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(138, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(66, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }

        btPoint02.tag = 6
        self.addSubview(btPoint02)
        btPoint02.setLayerBorder()
        btPoint02.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(70, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(66, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
        
        btPoint03.tag = 11
        btPoint03.setLayerBorder()
        self.addSubview(btPoint03)
        btPoint03.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset =  getRelativeContraintValue(22, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset =  getRelativeContraintValue(105, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }

        btPoint04.tag = 16
        btPoint04.setLayerBorder()
        self.addSubview(btPoint04)
        btPoint04.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(50, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(20, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }

        btPoint05.tag = 21
        btPoint05.setLayerBorder()
        self.addSubview(btPoint05)
        btPoint05.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(135, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(20, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }

        btPoint06.tag = 22
        btPoint06.setLayerBorder()
        self.addSubview(btPoint06)
        btPoint06.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(135, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(20, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }

        btPoint07.tag = 27
        btPoint07.setLayerBorder()
        self.addSubview(btPoint07)
        btPoint07.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(50, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(20, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }

        btPoint08.tag = 32
        btPoint08.setLayerBorder()
        self.addSubview(btPoint08)
        btPoint08.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(22, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(105, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }

        btPoint09.tag = 37
        self.addSubview(btPoint09)
        btPoint09.setLayerBorder()
        btPoint09.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(70, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(66, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }

        btPoint10.tag = 42
        self.addSubview(btPoint10)
        btPoint10.setLayerBorder()
        btPoint10.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset =  getRelativeContraintValue(138, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset =  getRelativeContraintValue(66, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
                
        Observable.merge(
            btPoint01.rx.tap.map { _ in 1 },
            btPoint02.rx.tap.map { _ in 6 },
            btPoint03.rx.tap.map { _ in 11 },
            btPoint04.rx.tap.map { _ in 16 },
            btPoint05.rx.tap.map { _ in 21 },
            btPoint06.rx.tap.map { _ in 22 },
            btPoint07.rx.tap.map { _ in 27 },
            btPoint08.rx.tap.map { _ in 32 },
            btPoint09.rx.tap.map { _ in 37 },
            btPoint10.rx.tap.map { _ in 42 }
        ).subscribe(onNext: {
            Log.message(to: "\(#function)index: \($0)")
            self.cameraButtonTouch($0)
        }).disposed(by: disposeBag)
    }
    
    func cameraButtonTouch(_ tag: Int) {
        //railIndex = tag
        //1  6, 11, 16, 21
        //22, 27, 32, 37, 42
        ivRail.image = railImages[tag - 1] // tag 1 base
        let interval: Float = Float(totalCount) / 8.0
        current = tag - 1
        self.ivRail.image = railImages[tag - 1]
        switch tag {
        case 1:
            channel = 1
            break
        case 6..<21:
            let idx = tag / 5
            channel = Int(Float(idx) * interval)
            break
        case 21:
            channel = leftIndex - 1
            break
        case 22:
            channel = leftIndex
            break
        case 23..<42:
            let idx = tag / 5 - 4
            channel = leftIndex + Int(Float(idx) * interval)
            break
        case 42:
            channel = totalCount
            break
        default:
            break
        }
        Log.message(to: "\(#function) channel: \(channel)")
        rxChannel.onNext(channel)
    }
}
