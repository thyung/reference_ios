//
//  BasketBirdView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/23.
//

import RxCocoa
import RxSwift

class BasketBirdView: BirdView {
    
    let __MAX = 42
    let disposeBag: DisposeBag = DisposeBag()
        
    var ivBackGround = UIImageView()
    var ivRail = UIImageView()
    var btPoint01 = UIButton()
    var btPoint02 = UIButton()
    var btPoint03 = UIButton()
    var btPoint04 = UIButton()
    var btPoint05 = UIButton()
    var btPoint06 = UIButton()
    
    var btPoint07 = UIButton()
    var btPoint08 = UIButton()
    var btPoint09 = UIButton()
    var btPoint10 = UIButton()
    var btPoint11 = UIButton()
    var btPoint12 = UIButton()
                
    // TODO: 외부에서 입력 설정
    var leftIndex = 41
    var totalCount = 80
    var channel = 0
    var rxChannel = PublishSubject<Int>()
    private var railImages: [UIImage] = []
    
    public init(type: BirdType = .normal) {
        super.init(frame: .zero)
        birdType = type
        initVariable()
        setupLayout()
        setCameraButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initVariable()
        setupLayout()
        setCameraButton()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initVariable()
        setupLayout()
        setCameraButton()
    }
    
    override func initVariable() {
        setRailImages()
        self.addSubview(ivBackGround)
        
        switch birdType {
        case .normal:
            let bg = UIImage(named: "basket_controller")
            ivBackGround.image = bg
            
            let on = UIImage(named: "icon_birdview_on")
            let off = UIImage(named: "icon_birdview_off")
            btToggle.setBackgroundImage(on, for: .normal)
            btToggle.setBackgroundImage(off, for: .selected)
            setRXFunction()
            break
        case .pip:
            let bg = UIImage(named: "basket_controller_landscape")
            ivBackGround.image = bg
            
            let image = UIImage(named: "icon_close")
            btClose.setBackgroundImage(image, for: .normal)
            break
        }

        ivRail.image = railImages.first
        ivRail.setLayerBorder()
        self.addSubview(ivRail)
    }
    
    private func setRailImages() {
        for i in 0..<__MAX  {
            if let image = UIImage(named: "basket_rail_\(i+1)") {
                railImages.append(image)
            }
        }
    }
    
    override func setupLayout() {
        ivBackGround.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                view.left.right.top.bottom.equalTo(weakSelf)
            }
        }
        ivRail.snp.makeConstraints { [weak self] view in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(29.0, guide: 390)
                view.left.equalTo(weakSelf).offset(offset)
                view.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(46.0, guide: 390)
                view.top.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(13.0, guide: 390)
                view.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
    }
    
    private func setRXFunction() {
        btToggle.rx.tap
            .scan(false) { [weak self] (lastState, newValue) in
                if let weakSelf = self {
                    weakSelf.hiddenControl(!lastState)
                }
                return !lastState
            }
            .bind(to: btToggle.rx.isSelected)
            .disposed(by: disposeBag)
    }
    
    func hiddenControl(_ isHidden: Bool) {
        isHidden ? ivRail.fadeOut() : ivRail.fadeIn()
        btPoint01.isHidden = isHidden
        btPoint02.isHidden = isHidden
        btPoint03.isHidden = isHidden
        btPoint04.isHidden = isHidden
        btPoint05.isHidden = isHidden
        btPoint06.isHidden = isHidden
        btPoint07.isHidden = isHidden
        btPoint08.isHidden = isHidden
        btPoint09.isHidden = isHidden
        btPoint10.isHidden = isHidden
        btPoint11.isHidden = isHidden
        btPoint12.isHidden = isHidden
    }
    
    override func interactiveMode(_ isMode: Bool) {
        _isInteractive = isMode
        ivRail.isHidden = !isMode
        btPoint01.isHidden = !isMode
        btPoint02.isHidden = !isMode
        btPoint03.isHidden = !isMode
        btPoint04.isHidden = !isMode
        btPoint05.isHidden = !isMode
        btPoint06.isHidden = !isMode
        btPoint07.isHidden = !isMode
        btPoint08.isHidden = !isMode
        btPoint09.isHidden = !isMode
        btPoint10.isHidden = !isMode
        btPoint11.isHidden = !isMode
        btPoint12.isHidden = !isMode
        if birdType == .normal {
            btToggle.isHidden = !isMode
        }
    }
    
    // 1 base
    override func updateCameraPosition(_ channel: Int) {
        if channel < 1 { return }
        
        var index = 0
        switch channel {
        case 1:
            index = 0
            break
        case 2..<(leftIndex - 2):
            let interval:Float = Float(totalCount) / 42.0
            index = Int(Float(channel) / interval) + 1
            if index < 1 { index = 1 }
            let lastIndex = leftIndex - 2
            if channel < lastIndex && index >= 21 {
                index = 20
            }
            break
        case leftIndex - 1:
            index = 21
            break
        case leftIndex..<totalCount:
            let interval:Float = Float(totalCount) / 42.0
            index = Int(Float(channel) / interval) + 1
            if index > railImages.count - 2 {
                index = railImages.count - 2
            }
            break
        case totalCount:
            index = railImages.count - 1
            break
        default:
            if totalCount < channel {
                index = railImages.count - 1
            }
            break
        }
        
        DispatchQueue.main.async {
            self.ivRail.image = self.railImages[index]
            Log.message(to: "\(#function) channel: \(channel) index: \(index)")
        }
    }
}


extension BasketBirdView {
    
    private func setCameraButton() {
        //1  5, 9, 13, 17  21
        btPoint01.tag = 1
        btPoint01.setLayerBorder()
        self.addSubview(btPoint01)
        btPoint01.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(149, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(84, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }

        btPoint02.tag = 5
        self.addSubview(btPoint02)
        btPoint02.setLayerBorder()
        btPoint02.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(92, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(46, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
        
        btPoint03.tag = 9
        btPoint03.setLayerBorder()
        self.addSubview(btPoint03)
        btPoint03.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(30, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(78, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
        
        btPoint04.tag = 13
        btPoint04.setLayerBorder()
        self.addSubview(btPoint04)
        btPoint04.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(30, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(46, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
        
        btPoint05.tag = 17
        btPoint05.setLayerBorder()
        self.addSubview(btPoint05)
        btPoint05.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(92, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(16, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
        
        btPoint06.tag = 21
        btPoint06.setLayerBorder()
        self.addSubview(btPoint06)
        btPoint06.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(149, guide: 390)
                btn.right.equalTo(weakSelf).offset(-offset)
                offset = getRelativeContraintValue(50, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
        
        //22, 26, 30, 34, 38, 42
        btPoint07.tag = 22
        btPoint07.setLayerBorder()
        self.addSubview(btPoint07)
        btPoint07.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(150, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(50, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
        
        btPoint08.tag = 26
        btPoint08.setLayerBorder()
        self.addSubview(btPoint08)
        btPoint08.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(90, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(16, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
        
        btPoint09.tag = 30
        btPoint09.setLayerBorder()
        self.addSubview(btPoint09)
        btPoint09.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(29, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(46, guide: 390)
                btn.bottom.equalTo(weakSelf).offset(-offset)
            }
        }
        
        btPoint10.tag = 34
        btPoint10.setLayerBorder()
        self.addSubview(btPoint10)
        btPoint10.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(29, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(78, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
        
        btPoint11.tag = 38
        self.addSubview(btPoint11)
        btPoint11.setLayerBorder()
        btPoint11.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(94, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(46, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
        
        btPoint12.tag = 42
        btPoint12.setLayerBorder()
        self.addSubview(btPoint12)
        btPoint12.snp.makeConstraints { [weak self] btn in
            if let weakSelf = self {
                var offset = getRelativeContraintValue(40, guide: 390)
                btn.width.height.equalTo(offset)
                offset = getRelativeContraintValue(148, guide: 390)
                btn.left.equalTo(weakSelf).offset(offset)
                offset = getRelativeContraintValue(84, guide: 390)
                btn.top.equalTo(weakSelf).offset(offset)
            }
        }
        
        Observable.merge(
            btPoint01.rx.tap.map { _ in 1 },
            btPoint02.rx.tap.map { _ in 5 },
            btPoint03.rx.tap.map { _ in 9 },
            btPoint04.rx.tap.map { _ in 13 },
            btPoint05.rx.tap.map { _ in 17 },
            btPoint06.rx.tap.map { _ in 21 },
            btPoint07.rx.tap.map { _ in 22 },
            btPoint08.rx.tap.map { _ in 26 },
            btPoint09.rx.tap.map { _ in 30 },
            btPoint10.rx.tap.map { _ in 34 },
            btPoint11.rx.tap.map { _ in 38 },
            btPoint12.rx.tap.map { _ in 42 }
        ).subscribe(onNext: {
            Log.message(to: "\(#function)index: \($0)")
            self.cameraButtonTouch($0)
        }).disposed(by: disposeBag)
    }
    
    func cameraButtonTouch(_ tag: Int) {
        channel = tag
        self.ivRail.image = railImages[tag - 1]
        let interval = totalCount / 10
        
        switch tag {
        case 1:
            channel = 1
            break
        case 5..<21:
            let idx = tag / 4
            channel = idx * interval
            break
        case 21:
            channel = leftIndex - 1
            break
        case 22:
            channel = leftIndex
            break
        case 26..<42:
            let idx = (tag / 4) - 5
            channel = leftIndex + (idx * interval)
            break
        case 42:
            channel = totalCount
            break
        default:
            break
        }
        rxChannel.onNext(channel)
        Log.message(to: "\(#function) channel: \(channel) interval: \(interval)")
    }
}

