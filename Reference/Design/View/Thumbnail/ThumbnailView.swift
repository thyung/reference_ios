//
//  ThumbnailView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//


enum ThumnailType: String {
    case time = "time"
    case button = "button"
    case title = "title"
    case message = "message"
}

import UIKit
import Nuke

class ThumbnailView: UIView {
    let thumbnail: UIImageView = UIImageView()
    lazy var time: UILabel = {
        let label = UILabel()
        label.font = UIFont.OpenSans(ofSize: 11.0)
        label.textAlignment = .center
        label.textColor = .white
        label.roundCorners(.allCorners, radius: 2.0)        
        label.backgroundColor = .colorRGBA(0, 0, 0, 0.65)
        return label
    }()
        
    lazy var title: UILabel = {
        let label = UILabel()
        label.font = .OpenSansSemiBold(ofSize: 13.0)
        label.numberOfLines = 2
        label.textAlignment = .left
        label.textColor = .white
        return label
    }()
    
    lazy var message: UILabel = {
        let label = UILabel()
        label.font = .OpenSans(ofSize: 13.0)
        label.textAlignment = .left
        label.numberOfLines = 2
//        label.text = "Judo gold\nMedal match"
        label.textColor = .white
        return label
    }()
    
    lazy var rotate: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icoInteractive")
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    lazy var pause: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "btnPlay")
        imageView.isUserInteractionEnabled = false
        return imageView
    }()

    lazy var overlay: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "bgOverlayVerticalPress")
        imageView.isUserInteractionEnabled = false
        return imageView
    }()

    
    
    lazy var overlayVertical: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "bgOverlayVertical")
        imageView.isUserInteractionEnabled = false
        imageView.roundCorners([.bottomLeft,.bottomRight], radius: 6)
        return imageView
    }()

    lazy var overlayHorizontal: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "bgOverlayHorizontal")
        imageView.isUserInteractionEnabled = false
//        imageView.roundCorners([.bottomLeft,.bottomRight], radius: 6)
        return imageView
    }()

    
    var url: String? {
        didSet {
            guard let thumnailUrl = self.url else {
                self.thumbnail.image = #imageLiteral(resourceName: "imgThumbnailDefault.png")
                return
            }
            self.thumbnail.contentMode = .scaleAspectFill
            let options = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "imgThumbnailDefault.png"), transition: .fadeIn(duration: 0.5), failureImage: #imageLiteral(resourceName: "imgThumbnailDefault.png"), failureImageTransition: .none)
            
            
            let u = UrlList.imageUrl(path: thumnailUrl)
            Nuke.loadImage(with: u, options: options, into: thumbnail)
            
        }
    }
    
    var info: GroupInfoModel? {
        didSet {
            guard let info = self.info else {
                self.rotate.isHidden = true
                return
            }
            let groups = info.groups
            if groups.count > 0 {
                let group: GroupModel = groups[0]
                self.rotate.isHidden = group.channel_count > 1 ? false : true
            }
        }
    }
    
    var duration: Double? {
        didSet {
            guard let d = self.duration else {
                return
            }
            self.time.text = Int(d).getDuration()
        }
    }

    
    public init(type: ThumnailType) {
        super.init(frame: .zero)
        setComponent()
        draw(type: type)
    }
            
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ThumbnailView {
    
    func setComponent() {
        self.addSubview(thumbnail)
        thumbnail.setLayerBorder()
        thumbnail.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.left.right.bottom.equalTo(self)
        }
        
        self.addSubview(rotate)
        rotate.setLayerBorder()
        rotate.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.top.equalTo(self).offset(4.0)
            view.width.equalTo(39.0)
            view.height.equalTo(24.0)
        }
    }
    
    
    func draw(type: ThumnailType) {
        
        switch type {
        case .time:
            setTime()
            break
        case .title:
            setTitle()
            break
        case .message:
            setMessage()
            break
        case .button:
            setButton()
            break
        }
    }
    
    private func setTime() {
        self.addSubview(time)
        time.setLayerBorder()
        time.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.right.equalTo(self).offset(-5.0)
            view.bottom.equalTo(self).offset(-5.0)
            view.width.equalTo(32.0)
            view.height.equalTo(17.0)
        }
    }
    
    private func setTitle() {
//        self.thumbnail.roundCorners(.allCorners, radius: 4)
        self.addSubview(overlayHorizontal)
        overlayHorizontal.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.right.bottom.equalTo(self)
            view.height.equalTo(90.0)
        }
        
        self.addSubview(overlay)
        overlay.isHidden = true

        overlay.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.left.right.bottom.equalTo(self)
        }
        
        self.bringSubviewToFront(rotate)

        self.addSubview(title)
        title.setLayerBorder()
        title.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.equalTo(self).offset(15.0)
            view.right.equalTo(self).offset(-15.0)
            view.bottom.equalTo(self).offset(-14.0)
            view.height.equalTo(36.0)
        }
        
        self.addSubview(message)
        message.setLayerBorder()
        message.font = .OpenSans(ofSize: 10.0)
        message.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.equalTo(self).offset(15.0)
            view.right.equalTo(self).offset(-15.0)
            view.bottom.equalTo(title.snp.top).offset(-6.0)
            view.height.equalTo(14.0)
        }
        
    }
    
    private func setMessage() {
                
        self.addSubview(overlayVertical)
        overlayVertical.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.right.bottom.equalTo(self)
            view.height.equalTo(58.0)
        }
        
        self.addSubview(overlay)
        overlay.isHidden = true

        overlay.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.left.right.bottom.equalTo(self)
        }
        
        self.bringSubviewToFront(rotate)

        self.addSubview(message)    
        message.setLayerBorder()
        message.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.left.equalTo(self).offset(15.0)
            view.right.equalTo(self).offset(-15.0)
            view.bottom.equalTo(self).offset(-10.0)
            view.height.equalTo(40.0)
        }
    }
    
    private func setButton() {
        
        self.addSubview(pause)
        pause.setLayerBorder()
        pause.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.width.height.equalTo(50.0)
            view.centerX.centerY.equalTo(self)
        }
    }
}


