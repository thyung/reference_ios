//
//  PickCollectionViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit
import Nuke

class PickCollectionViewCell: BaseCollectionViewCell {
    
    public static var identifier: String = "PickCollectionViewCell"
        
    let thumbnail: ThumbnailView = ThumbnailView(type: .message)
    var indexRow: Int = 0
    
    var dic: ContentModel? {
        didSet {
            guard let dictionary = self.dic else {
                return
            }
            self.setInfo(info: dictionary)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                thumbnail.overlay.isHidden = false
            } else {
                thumbnail.overlay.isHidden = true
            }
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        setComponent()
        setAutolayOut()
    }
            
    private func setComponent() {
        self.addSubview(thumbnail)        
        thumbnail.roundCorners(.allCorners, radius: 4.0)
        thumbnail.setLayerBorder()
    }
    
    private func setAutolayOut() {
        thumbnail.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.left.right.bottom.equalTo(self)
        }
        
    }
    
    public func setInfo(info: ContentModel) {
        thumbnail.url = info.thumbnail_url
        thumbnail.message.text = info.title
        thumbnail.info = info.group_info
    }
}
