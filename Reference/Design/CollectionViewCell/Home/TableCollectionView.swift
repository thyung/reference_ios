//
//  TableCollectionView.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit
import RxSwift
import RxCocoa

enum ContentType: String {
    case live = "live"
    case clip1 = "clip1"
    case clip2 = "clip2"
    case moments = "moments"
    case clip3 = "clip3"
    case clip4 = "clip4"
    case longclip = "longclip"
}


enum pickType {
    case background
    case none
}

class TableCollectionView: BaseCollectionViewCell, UIScrollViewDelegate {
    
    public static var identifier: String = "TableCollectionView"
    private(set) var disposeBag = DisposeBag()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = true
        tableView.bounces = false
        tableView.register(cellType: LiveTableViewCell.self)        //라이브
        tableView.register(cellType: PickTableViewCell.self)        //10 픽 or miss out
        tableView.register(cellType: ClipTableViewCell.self)        //Spotlight or hot clip
        tableView.register(cellType: MomentTableViewCell.self)      //Moment
        tableView.contentInsetAdjustmentBehavior = .never
        return tableView
    }()
    
    let listItem: BehaviorRelay<[SectionModel]> = BehaviorRelay(value: [])
    var contentList: [SectionModel]? {
        didSet {
            guard let contents = self.contentList else {
                self.tableView.isHidden = true
                return
            }
            self.tableView.isHidden = false
            self.listItem.accept(contents)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        setComponent()
        setAutolayOut()
    }
     
    private func setComponent() {
        self.addSubview(tableView)        
        tableView.rx.setDelegate(self).disposed(by: self.disposeBag)
        setDataSource()
        tableView.reloadData()
    }
    
    private func setAutolayOut() {
        tableView.snp.makeConstraints { view -> Void in
            view.top.left.bottom.right.equalTo(self)
        }
    }
    
    override func prepareForReuse() {
       super.prepareForReuse()
    }

}

extension TableCollectionView {
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: tableView.rx.items) { [weak self] (tableView, row, element) in
                guard let `self` = self else { return UITableViewCell() }
                let indexPath = IndexPath(item: row, section: 0)
                let dic: SectionModel = self.listItem.value[indexPath.row]
                let type = ContentType(rawValue: dic.layout ?? "")
                switch (type) {
                case .live:
                    let cell = self.tableView.dequeueReusableCell(for: indexPath,
                                                                  cellType: LiveTableViewCell.self)
                    cell.selectionStyle = .none
                    cell.title.draw(type: .line)
                    cell.title.title.text = dic.name
                    cell.contentList = dic.contents
                    return cell
                case .longclip:
                    let cell = self.tableView.dequeueReusableCell(for: indexPath,
                                                                  cellType: PickTableViewCell.self)
                    cell.selectionStyle = .none
                    cell.title.draw(type: .normal)
                    cell.title.title.text = dic.name
                    cell.contentList = dic.contents
                    return cell
                case .clip1, .clip2, .clip3, .clip4:
                    let cell = self.tableView.dequeueReusableCell(for: indexPath,
                                                                  cellType: ClipTableViewCell.self)
                    cell.selectionStyle = .none
                    cell.title.draw(type: .normal)
                    cell.title.title.text = dic.name
                    cell.contentList = dic.contents
                    return cell
                case .moments:
                    let cell = self.tableView.dequeueReusableCell(for: indexPath,
                                                                  cellType: MomentTableViewCell.self)
                    cell.selectionStyle = .none
                    cell.title.draw(type: .middle)
                    cell.title.title.text = dic.name
                    cell.contentList = dic.contents
                    return cell
                    
                case .none:
                    return UITableViewCell()
                }
            }
            .disposed(by: self.disposeBag)
    }
}

