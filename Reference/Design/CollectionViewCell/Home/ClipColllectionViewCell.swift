//
//  ClipColllectionViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/09.
//

import UIKit
import Nuke

class ClipColllectionViewCell: BaseCollectionViewCell {
    
    public static var identifier: String = "ClipColllectionViewCell"
        
    let thumbnail: ThumbnailView = ThumbnailView(type: .time)
    let info: InfoView = InfoView(type: .clip(.vod))
    var indexRow: Int = 0
    
    var dic: ContentModel? {
        didSet {
            guard let dictionary = self.dic else {
                return
            }
            self.setInfo(info: dictionary)
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        setComponent()
        setAutolayOut()
    }
            
    private func setComponent() {
        self.addSubview(thumbnail)
        thumbnail.roundCorners(.allCorners, radius: 4.0)
        thumbnail.setLayerBorder()
        
        self.addSubview(info)
//        info.title.text = "Brazil 5:4 Germany after penalty shoot-out"
//        info.message.text = "SEMI-FINAL • WOMEN"
        info.setLayerBorder()
    }
            
    private func setAutolayOut() {
        thumbnail.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.left.right.equalTo(self)            
            view.height.equalTo(92.0)
        }
        
        info.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(thumbnail.snp.bottom).offset(6.0)
            view.left.right.equalTo(self)
            view.height.equalTo(58.0)
        }
    }
    
    public func setInfo(info: ContentModel) {
        
        let ratio: CGFloat = 163/375
        let labelWitdh = UIScreen.width * ratio
        let strHeight: CGFloat = info.title?.height(width: labelWitdh, font: self.info.title.font) ?? 0.0
        
        self.info.title.snp.updateConstraints { view -> Void in
            if strHeight < 17.0 {
                view.height.equalTo(17.0)
            } else {
                view.height.equalTo(34.0)
            }
        }
        
        self.info.title.text = info.title
        self.info.message.text = info.description
        thumbnail.url = info.thumbnail_url
        thumbnail.info = info.group_info
        thumbnail.duration = info.duration
    }
    
    
}
