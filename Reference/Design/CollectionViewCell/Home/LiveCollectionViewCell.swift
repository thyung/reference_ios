//
//  LiveCollectionViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit
import Nuke

class LiveCollectionViewCell: BaseCollectionViewCell {
    
    public static var identifier: String = "LiveCollectionViewCell"
        
    let thumbnail: ThumbnailView = ThumbnailView(type: .button)
    let info: InfoView = InfoView(type: .clip(.live))
    var indexRow: Int = 0
    
    var dic: ContentModel? {
        didSet {
            guard let dictionary = self.dic else {
                return
            }
            self.setInfo(info: dictionary)
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        setComponent()
        setAutolayOut()
    }
            
    private func setComponent() {
        self.addSubview(thumbnail)
        thumbnail.roundCorners([.topLeft, .topRight], radius: 7.0)
        thumbnail.setLayerBorder()
        
        
        self.addSubview(info)
//        info.title.text = "MARGARITA MAMUN"
//        info.message.text = "BRONZE MEDAL GAME • FINAL"

        info.roundCorners([.bottomLeft, .bottomRight], radius: 7.0)
        info.setLayerBorder()
        
    }
            
    private func setAutolayOut() {
        thumbnail.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(self)
            view.left.equalTo(self)
            view.right.equalTo(self)
            view.height.equalTo(193.0)
        }
        
        info.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(thumbnail.snp.bottom)
            view.left.right.equalTo(self)
            view.height.equalTo(68.0)
        }
        
    }
    
    public func setInfo(info: ContentModel) {
        self.info.title.text = info.title
        self.info.message.text = info.description
        thumbnail.url = info.thumbnail_url
        thumbnail.info = info.group_info
    }
    
    
}
