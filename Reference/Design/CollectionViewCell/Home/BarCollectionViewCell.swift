//
//  BarCollectionViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit
import Nuke

class BarCollectionViewCell: BaseCollectionViewCell {
    public static var identifier: String = "BarCollectionViewCell"
    let imageView: UIImageView = UIImageView()
    let live: UIImageView = UIImageView()
    let name: UILabel = UILabel()
    var indexRow: Int = 0
    var info: CategoryModel? {
        didSet {
            guard let info = self.info else {
                return
            }

            self.name.text = info.name
            if let live = info.has_live {
                self.live.isHidden = !live
            } else {
                self.live.isHidden = true
            }
            
            if let unselected = info.unselected_icon_url {
                let unSelectedUrl = UrlList.imageUrl(path: unselected)
                Nuke.loadImage(with: unSelectedUrl, into: imageView)
            }
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var isSelected: Bool {
        didSet {            
            guard let info = self.info else {
                return
            }
            
            if isSelected {
                if let selectedUrl = info.selected_icon_url {
                    let url = UrlList.imageUrl(path: selectedUrl)
                    Nuke.loadImage(with: url, into: imageView)
                }
            } else {                
                if let unSelectedUrl = info.unselected_icon_url {
                    let url = UrlList.imageUrl(path: unSelectedUrl)
                    Nuke.loadImage(with: url, into: imageView)
                }
            }
            
            self.name.textColor = isSelected ? UIColor.black : UIColor.gray
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        self.addSubview(imageView)
        imageView.setLayerBorder()
        
        live.image = UIImage(named: "tagLive")
        self.addSubview(live)
        
        name.font = UIFont.PoppinsMedium(ofSize: 12.0)
        name.textAlignment = .center
        name.textColor = .gray
        self.addSubview(name)
        name.setLayerBorder()
                
        setAutolayOut()
    }
                
    private func setAutolayOut() {
        imageView.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(self)
            view.width.height.equalTo(64.0)
            view.centerX.equalTo(self)
        }

        live.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.centerX.equalTo(self)
            view.top.equalTo(self).offset(56.0)
            view.width.equalTo(33.0)
            view.height.equalTo(16.0)
        }
        
        imageView.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(self)
            view.width.height.equalTo(64.0)
            view.centerX.equalTo(self)
        }

        name.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(imageView.snp.bottom).offset(9.0)
            view.left.right.equalTo(self)
            view.height.equalTo(17.0)
        }
        
    }
        
    
}
