//
//  BaseCollectionViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell, Reusable {
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
                    
    
}
