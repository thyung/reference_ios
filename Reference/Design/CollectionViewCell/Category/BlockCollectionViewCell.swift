//
//  BlockCollectionViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit


class BlockCollectionViewCell: BaseCollectionViewCell {
    
    public static var identifier: String = "BlockCollectionViewCell"
        
    let selectedView: UIView = UIView()
    let name: UILabel = UILabel()
    
    var indexRow: Int = 0
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.selectedView.isHidden = isHighlighted ? false : true
            self.name.textColor = isHighlighted ? .colorRGBA(0, 0, 0, 0.5) : .black
        }
    }


    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        setComponent()
        setAutolayOut()
    }
            
    private func setComponent() {
        self.backgroundColor = .white
        
        selectedView.backgroundColor = UIColor.colorRGB(246, 246, 246)
        self.addSubview(selectedView)
        selectedView.isHidden = true
        
        name.font = UIFont.OpenSansSemiBold(ofSize: 13.0)
        name.textColor = .black
        self.addSubview(name)
        name.setLayerBorder()
                
    }
            
    private func setAutolayOut() {
        
        selectedView.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.bottom.left.right.equalTo(self)            
        }
        
        name.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.center.equalTo(self)
            view.height.equalTo(15.0)
        }
    }

}
