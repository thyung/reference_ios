//
//  SearchWordTableViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/11.
//

import UIKit
import RxSwift
import RxCocoa

class SearchWordTableViewCell: BaseTableViewCell {
    public static var identifier: String = "SearchWordTableViewCell"
    let glass: UIImageView = UIImageView()
    let keyword: UILabel = UILabel()
    let close: UIButton = UIButton()
    var disposeBag: DisposeBag = DisposeBag()

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setComponent()
        setAutoLayOut()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
        
    private func setComponent() {
        self.backgroundColor = .clear
        
        glass.image = UIImage(named: "icoSearch")
        self.contentView.addSubview(glass)
        glass.setLayerBorder()
        
        keyword.font = UIFont.OpenSansSemiBold(ofSize: 16.0)
        keyword.textAlignment = .left
        keyword.textColor = .black
        
        self.contentView.addSubview(keyword)
        keyword.setLayerBorder()

        close.setImage(UIImage(named: "icoHistoryDelete"), for: .normal)
        self.contentView.addSubview(close)
        close.setLayerBorder()
    }
    
    private func setAutoLayOut() {
        glass.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.contentView).offset(16.0)
            view.left.equalTo(self.contentView).offset(16.0)
            view.width.height.equalTo(20.0)
            view.bottom.equalTo(self.contentView).offset(-16.0)
        }
        
        keyword.snp.makeConstraints { view -> Void in
            view.centerY.equalTo(self.contentView)
            view.left.equalTo(self.glass.snp.right).offset(12.0)
            view.right.equalTo(close.snp.left).offset(-18.0)
            view.height.equalTo(22.0)
        }

        close.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.contentView).offset(6.0)
            view.right.equalTo(self.contentView).offset(-16.0)
            view.width.height.equalTo(40.0)
            
        }

    }

}
