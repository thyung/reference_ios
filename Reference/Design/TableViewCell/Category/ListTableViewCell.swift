//
//  ListTableViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/09.
//

import UIKit
import Nuke

class ListTableViewCell: BaseTableViewCell {    
    public static var identifier: String = "ListTableViewCell"
    let thumbnail: ThumbnailView = ThumbnailView(type: .time)
    let info: InfoView = InfoView(type: .list(.live))
    let play: UIImageView = UIImageView(image: UIImage(named: "iconSelectPlayContent"))
    var dic: ContentModel? {
        didSet {
            guard let dictionary = self.dic else {
                return
            }
            self.setInfo(info: dictionary)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.play.isHidden = false
                self.backgroundColor = .colorRGB(239, 239, 239)
            } else {
                self.play.isHidden = true
                self.backgroundColor = .clear
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                self.backgroundColor = .colorRGB(239, 239, 239)
            } else {
                self.backgroundColor = .clear
            }
        }
    }
    

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setComponent()
        setAutoLayOut()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state        
    }
        
    private func setComponent() {
        self.backgroundColor = .clear
        self.addSubview(thumbnail)
        thumbnail.setLayerBorder()

        thumbnail.addSubview(play)
        play.setLayerBorder()
        play.isHidden = true
        
        self.addSubview(info)
        info.setLayerBorder()
    }
    
    private func setAutoLayOut() {
        thumbnail.snp.makeConstraints { [weak self] view -> Void in
            if let weakSelf = self {
                view.top.equalTo(weakSelf).offset(5.0)
                view.left.equalTo(weakSelf).offset(16.0)
                view.width.equalTo(160.0)
                view.height.equalTo(90.0)
                view.bottom.equalTo(weakSelf).offset(-5.0)
            }
        }
        
        play.snp.makeConstraints { view -> Void in
                view.center.equalTo(thumbnail)
                view.width.height.equalTo(32.0)
        }
        
        info.snp.makeConstraints { [weak self] view -> Void in
            if let weakSelf = self {
                view.top.equalTo(weakSelf.thumbnail.snp.top)
                view.left.equalTo(weakSelf.thumbnail.snp.right).offset(10.0)
                view.right.equalTo(weakSelf).offset(-16.0)
                view.height.equalTo(thumbnail.snp.height)
            }
        }
    }
    
    public func setInfo(info: ContentModel) {
        let ratio: CGFloat = 173/375
        let labelWitdh = UIScreen.width * ratio
        let strHeight: CGFloat = info.title?.height(width: labelWitdh, font: self.info.title.font) ?? 0.0

        self.info.title.snp.updateConstraints { view -> Void in
            if strHeight < 21.0 {
                view.height.equalTo(21.0)
            } else {
                view.height.equalTo(41.0)
            }
        }

        self.info.title.text = info.title
        self.info.message.text = info.description
        
        if let thumbUrl = info.thumbnail_url {
            let url = UrlList.imageUrl(path: thumbUrl)
            Nuke.loadImage(with: url, into: thumbnail.thumbnail)
        }
        
        if let group = info.group_info {
            let groups = group.groups
            if groups.count > 0 {
                let group: GroupModel = groups[0]
                self.thumbnail.rotate.isHidden = group.channel_count > 1 ? false : true
            }
        } else {
            self.thumbnail.rotate.isHidden = true
        }
        if let duration = info.duration {            
            self.thumbnail.time.text = Int(duration).getDuration()
        }

    }
}
