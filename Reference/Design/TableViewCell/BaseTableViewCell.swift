//
//  BaseTableViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit
import SnapKit

class BaseTableViewCell: UITableViewCell, Reusable {
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
        
}
