/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class NextPageCell: BaseTableViewCell {
    public static var identifier: String = "NextPageCell"
    var indicator = UIActivityIndicatorView()
    var lbLoding = UILabel()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setComponent()
        setAutoLayOut()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setComponent()
        setAutoLayOut()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setComponent() {
        self.backgroundColor = .clear        
        indicator.style = .large
        indicator.color = .darkGray
        self.addSubview(indicator)
        lbLoding.text = "Loading..."
        lbLoding.textAlignment = .center
        self.addSubview(lbLoding)
    }
    
    private func setAutoLayOut() {
        indicator.snp.makeConstraints { [weak self] view in
            guard let `self` = self else { return }
            view.centerX.equalTo(self)
            view.centerY.equalTo(self).offset(-10)
        }
        
        lbLoding.snp.makeConstraints { label in
            label.centerX.equalTo(indicator)
            label.top.equalTo(indicator.snp.bottom).offset(4)
        }
    }
        
    public func startIndicator() {
        indicator.isHidden = false
        indicator.startAnimating()
    }
    
    public func stopIndicator() {
        indicator.isHidden = true
        indicator.stopAnimating()
    }
}
