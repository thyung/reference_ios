//
//  LiveTableViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit
import RxCocoa
import RxSwift

class LiveTableViewCell: BaseTableViewCell, UIScrollViewDelegate {
    public static var identifier: String = "LiveTableViewCell"
    let title: TitleView = TitleView()
    let collection: UICollectionView = UICollectionView(frame: .zero,
                                                        collectionViewLayout: UICollectionViewFlowLayout.init())
    private(set) var disposeBag = DisposeBag()
    let listItem: BehaviorRelay<[ContentModel]> = BehaviorRelay(value: [])
    var contentList: [ContentModel]? {
        didSet {
            guard let contents = self.contentList else {
                return
            }
            
            self.listItem.accept(contents)
        }
    }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setComponent()
        setAutoLayOut()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
    private func setComponent() {
        self.backgroundColor = .clear    
        self.contentView.addSubview(title)
        title.setLayerBorder()

        if let layout = collection.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 32.0
            layout.minimumInteritemSpacing = 0.0
            layout.sectionInset = UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
            layout.itemSize = CGSize(width: UIScreen.width - 32.0, height: 266.0)
        }
        
        collection.backgroundColor = .clear
        collection.bounces = false
        collection.register(cellType: LiveCollectionViewCell.self)
        collection.alwaysBounceVertical = false
        collection.alwaysBounceHorizontal = false
        collection.isScrollEnabled = true
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.isPagingEnabled = true
        collection.rx.setDelegate(self).disposed(by: self.disposeBag)
        self.contentView.addSubview(collection)
        collection.setLayerBorder()
     
        collection.rx
            .itemSelected
            .subscribe(onNext:{ [weak self] indexPath in
                guard let `self` = self else { return }
                let dic: ContentModel = self.listItem.value[indexPath.row]
                NavigationManager.navigationToVodViewController(type: .live, birdType: .baseball, content: dic)
            }).disposed(by: self.disposeBag)

        setDataSource()
    }
    
    private func setAutoLayOut() {
        title.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(self.contentView).offset(30.0)
            view.left.right.equalTo(self.contentView)
            view.height.equalTo(23.0)
        }
        
        collection.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(title.snp.bottom).offset(12.0)
            view.left.right.equalTo(self.contentView)
            view.height.equalTo(261.0)
            view.bottom.equalTo(self.contentView)
        }

    }
}


extension LiveTableViewCell {
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: collection.rx.items) { (collectionView, row, element) in
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.collection.dequeueReusableCell(for: indexPath,
                                                               cellType: LiveCollectionViewCell.self)
                let dic: ContentModel = self.listItem.value[indexPath.row]
                cell.dic = dic
                return cell
            }.disposed(by: self.disposeBag)
    }
}
