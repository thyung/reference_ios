//
//  ThumbnailTableViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit

class ThumbnailTableViewCell: BaseTableViewCell {
    
    public static var identifier: String = "ThumbnailTableViewCell"
    let thumbnail: ThumbnailView = ThumbnailView()
    let title: UILabel = UILabel()
    let message: UILabel = UILabel()

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setComponent()
        setAutoLayOut()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
    private func setComponent() {
        self.backgroundColor = .clear
        self.addSubview(thumbnail)
        thumbnail.setLayerBorder()
        
        title.font = .systemFont(ofSize: 12.0)
        self.addSubview(title)
        title.setLayerBorder()
        
        message.font = .boldSystemFont(ofSize: 15.0)
        message.textAlignment = .left
        message.numberOfLines = 2
        self.addSubview(message)
        message.setLayerBorder()
    }
    
    private func setAutoLayOut() {
        thumbnail.snp.makeConstraints { view -> Void in
            view.top.left.equalTo(self).offset(20.0)
            view.width.equalTo(97.0)
            view.height.equalTo(35.0)
            view.bottom.equalTo(self).offset(-20.0)
        }
        
        title.snp.makeConstraints { view -> Void in
            view.top.equalTo(self).offset(21.0)
            view.left.equalTo(self.thumbnail.snp.right).offset(10.0)
            view.right.equalTo(self).offset(-20.0)
            view.height.equalTo(16.0)
        }
        
        message.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.title.snp.bottom).offset(12.0)
            view.left.equalTo(self.thumbnail.snp.right).offset(10.0)
            view.right.equalTo(self).offset(-20.0)
            view.height.equalTo(45.0)
        }

    }


}
