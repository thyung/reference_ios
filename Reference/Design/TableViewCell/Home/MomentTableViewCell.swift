//
//  MomentTableViewCell.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/09.
//

import UIKit
import RxCocoa
import RxSwift

class MomentTableViewCell: BaseTableViewCell, UIScrollViewDelegate {
    
    public static var identifier: String = "MomentTableViewCell"
    let backgroundImageView: UIImageView = UIImageView()
    let title: TitleView = TitleView(type: .middle)
    let collection: UICollectionView = UICollectionView(frame: .zero,
                                                        collectionViewLayout: ZoomFlowLayout.init())
    private(set) var disposeBag = DisposeBag()
    let listItem: BehaviorRelay<[ContentModel]> = BehaviorRelay(value: [])
    var contentList: [ContentModel]? {
        didSet {
            guard let contents = self.contentList else {
                return
            }
            self.listItem.accept(contents)
        }
    }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setComponent()
        setAutoLayOut()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
    private func setComponent() {
        self.backgroundColor = .clear
        
        backgroundImageView.image = UIImage(named: "bgSection")
        self.addSubview(backgroundImageView)
        
        self.contentView.addSubview(title)
        title.setLayerBorder()
                
        collection.backgroundColor = .clear
        collection.bounces = false
        collection.register(cellType: MomentCollectionViewCell.self)
        collection.alwaysBounceVertical = false
        collection.alwaysBounceHorizontal = false
        collection.isScrollEnabled = true
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.isPagingEnabled = false
        collection.contentInsetAdjustmentBehavior = .always
        collection.rx.setDelegate(self).disposed(by: self.disposeBag)
        self.contentView.addSubview(collection)
        collection.setLayerBorder()
        
        collection.rx
            .itemSelected
            .subscribe(onNext:{ [weak self] indexPath in
                guard let `self` = self else { return }
                let list = self.listItem.value
                let model: ContentModel = list[indexPath.row]
                NavigationManager.navigationToVodViewController(type: .vod, content: model, list: list)                
            }).disposed(by: self.disposeBag)
        
        setDataSource()
    }
    
    private func setAutoLayOut() {
        backgroundImageView.snp.makeConstraints { [weak self] view -> Void in
            guard let `self` = self else { return }
            view.top.equalTo(self).offset(31.0)
            view.left.right.equalTo(self)
            view.height.equalTo(297.0)
        }
        
        title.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.contentView).offset(31.0)
            view.left.right.equalTo(self.contentView)
            view.height.equalTo(65.0)
        }
        
        collection.snp.makeConstraints { view -> Void in
            view.top.equalTo(title.snp.bottom)
            view.left.right.equalTo(self.contentView)
            view.height.equalTo(200.0)
            view.bottom.equalTo(self.contentView).offset(-32.0)
        }

    }
    
}

extension MomentTableViewCell {
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: collection.rx.items) { [weak self] (collectionView, row, element) in
                guard let `self` = self else { return UICollectionViewCell() }
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.collection.dequeueReusableCell(for: indexPath,
                                                               cellType: MomentCollectionViewCell.self)
                let dic: ContentModel = self.listItem.value[indexPath.row]
                cell.dic = dic
                return cell
                                    
            }.disposed(by: self.disposeBag)
    }
}
