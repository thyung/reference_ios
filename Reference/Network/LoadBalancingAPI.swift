//
//  LoadBalancingAPI.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/21.
//

import Foundation

extension FDNetworkManager {
    //4DLive Player에서 DSS URL을 요청한다.
    class func getRtspUrl(dic: FourDss.Url.Request,
                          completion: @escaping (Bool, FourDss.Url.Response?) -> Void) {
        let url: String = "\(UrlList.lsUrl(url: ServiceUrl.getRtspUrl))"
        self.request(method: .get,
                     url: url,
                     param: dic.dictionary) { response, success, error in
            if success, let r = response {
                ModelParser.parsing(json: r,
                                    type: FourDss.Url.Response.self) { data, error in
                    if let _ = error { return }
                    completion(success, data)
                }
            }
        }
    }
}
