//
//  ContentAPI.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

extension FDNetworkManager {
    class func getContentList(dic: Content.List.Request? = nil,
                              completion: @escaping (Bool, Content.List.Response?, Error?) -> Void) {
        let url: String = "\(UrlList.url(url: ServiceUrl.getContentList))"        
        self.request(method: .get,
                     url: url,
                     param: dic != nil ? dic?.dictionary : nil) { response, success, error in
            if success, let r = response {
                ModelParser.parsing(json: r,
                                    type: Content.List.Response.self) { data, error in
                    if let _ = error { return }
                    completion(success, data, error)
                }
            } else {
                completion(success, nil, error)
            }            
        }
    }
}
