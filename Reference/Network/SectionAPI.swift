//
//  SectionAPI.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/23.
//

import Foundation

extension FDNetworkManager {
    class func getSectionList(id: String? = nil,
                              completion: @escaping (Bool, SectionListModel?, Error?) -> Void) {
        let url: String = "\(UrlList.url(url: ServiceUrl.getSectionInfo))"
        
        var dic:[String: Any] = [:]
        dic["category_id"] = id
        
        self.request(method: .get,
                     url: url,
                     param: id != nil ? dic : nil) { response, success, error in
            if success, let r = response {
                ModelParser.parsing(json: r,
                                    type: SectionListModel.self) { data, error in
                    if let _ = error { return }
                    completion(success, data, error)
                }
            }
        }
    }
}
