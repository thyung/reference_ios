//
//  CategoryAPI.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation


extension FDNetworkManager {
    class func getCategoryList(completion: @escaping (Bool, CategoryListModel?, Error?) -> Void) {
        let url: String = "\(UrlList.url(url: ServiceUrl.getCategryList))"        
        self.request(method: .get,
                     url: url,
                     param: nil) { response, success, error in
            if success, let r = response {
                ModelParser.parsing(json: r,
                                    type: CategoryListModel.self) { data, error in
                    if let _ = error { return }
                    completion(success, data, error)
                }
            } else {
                completion(success, nil, error)
            }
        }
    }
}
