//
//  Sample.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import Foundation

//통신 모델 정의
//class Sample {
//    struct Request: Codable {
//        let lang: String
//    }
//
//    struct Response: Codable {
//        let resultCode: Int
//        let message: String
//        let data: Any
//    }
//
//    struct ErrorModel: Codable {
//        let error: ErrorMessageModel
//    }
//
//    struct ErrorMessageModel: Codable {
//        let message: String?
//    }
//


//통신 방법
//    class func request(dic: Request, completion: @escaping (Bool, Response?, ErrorModel?) -> Void) {
//        let url: String = ""
//        self.request(method: .get,
//                     url: url,
//                     param: dic.dictionary) {response, success in
//            if success {
//                ModelParser.parsing(
//                    json: response,
//                    type: Response.self,
//                    completion: { data, error in
//                        if let _ = error { return }
//                        completion(success, data, nil)
//                    })
//            }else{
//
//                ModelParser.parsing(
//                    json: response,
//                    type: ErrorModel.self,
//                    completion: { data, error in
//                        if let _ = error { return }
//                        completion(success, nil, data)
//                    }
//                )
//            }
//        }
//    }
//
//}


