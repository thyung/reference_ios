//
//  PopManager.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class PopUpManager {
        
    class func cellPopUp(vc: UIViewController, model: PopUpViewModel) {
//        let pvc = PopUpViewController(model: model)
//        pvc.delegate = vc as? CustomPopUpViewDelegate
//        pvc.providesPresentationContextTransitionStyle = true
//        pvc.definesPresentationContext = true
//        pvc.modalPresentationStyle = .overCurrentContext
//        pvc.modalTransitionStyle = .crossDissolve
//        vc.present(pvc, animated: true, completion: nil)        
    }
        
    class func customToast(vc: UIViewController? = nil,
                           v: UIView? = nil,
                           top: CGFloat = 16.0,
                           message: String? = nil,
                           left: CGFloat = 20.0) {
        
        var toast: ToastView?
        toast = ToastView()
        toast?.roundCorners(.allCorners, radius: 22.0)
        toast?.setLayerBorder()
        
        if let m = message { toast?.label.text = m }
        
        toast?.button.rx.tap.subscribe { [weak toast] event in
            toast?.removeFromSuperview()
            toast = nil
        }.disposed(by: toast!.disposeBag)
        
        
        
        if let viewController = vc {
            viewController.view.addSubview(toast!)
            toast?.snp.makeConstraints { view -> Void in
                view.left.equalTo(viewController.view).offset(20.0)
                view.right.equalTo(viewController.view).offset(-20.0)
                view.top.equalTo(viewController.view).offset(16.0)
                view.bottom.equalTo(toast!).offset(-14.0)
            }
            
            Observable<Int>.interval(.seconds(3), scheduler: MainScheduler.instance).bind { timePassed in
                toast?.removeFromSuperview()
                toast = nil
            }.disposed(by: toast!.disposeBag)
            
            return
        }
        
        
        if let view = v {
            view.addSubview(toast!)
            toast?.snp.makeConstraints { v -> Void in
                v.left.equalTo(view).offset(20.0)
                v.right.equalTo(view).offset(-20.0)
                v.top.equalTo(view).offset(16.0)
                v.bottom.equalTo(view).offset(-14.0)
            }
            
            Observable<Int>.interval(.seconds(3), scheduler: MainScheduler.instance).bind { timePassed in
                toast?.removeFromSuperview()
                toast = nil
            }.disposed(by: toast!.disposeBag)
            
            return
        }
        
        if let viewController = UIApplication.topViewController() {
            DispatchQueue.main.async {
                
                viewController.view.addSubview(toast!)
                toast?.snp.makeConstraints { view -> Void in
                    view.left.equalTo(viewController.view).offset(20.0)
                    view.right.equalTo(viewController.view).offset(-20.0)
                    view.top.equalTo(viewController.view).offset(16.0)
                    view.bottom.equalTo(toast!).offset(-14.0)
                }
                
                Observable<Int>.interval(.seconds(3), scheduler: MainScheduler.instance).bind { timePassed in
                    toast?.removeFromSuperview()
                    toast = nil
                }.disposed(by: toast!.disposeBag)
                
                return
                
            }
        }
    }
}
