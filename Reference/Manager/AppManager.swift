//
//  AppManager.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit
import UserNotifications

enum ServerType: Int {
    case develop = 0
    case release = 1
}

class AppManager {
    let plist_Configuration: String = "Configuration"
    let plist_version: String = "CFBundleShortVersionString"                // 앱 버젼
    let plist_buildVersion: String = "CFBundleVersion"                      // 빌드 번호
    let userDefault_AccessToken: String = "accessToken"                     // 특정 회사에서 사용하는 토큰
    let userDefault_CurrentDomain: String = "currentDomain"                 // 사용 도메인
    let userDefault_UserId: String = "userId"                               // 유저 아이디
    let userDefault_RecentSearchWords: String = "recentSearchWords"         // 입력했던 최신 검색어

    // 앱 매니저 부분
    var serverType: ServerType                                              // 서버 타입
    let version: String                                                     // 앱 버젼
    let buildVersion: String                                                // 앱 빌드 버젼
    var isRelease: Bool                                                     // 상용 배포 체크
    var pushToken: String?                                                  // 앱 푸시 토큰
    var isAdd: Bool = false                                                 // 정의되어 있지 않은 키값이 default에 들어가는 유무

    static let shared: AppManager = {
        let instance = AppManager()
        return instance
    }()

    private init() {
        // App Status: Debug/Release
        if let version = Bundle.main.object(forInfoDictionaryKey: plist_version) as? String {
            self.version = version
        } else {
            self.version = ""
        }

        // App Build Version on Debug Mode
        if let buildVersion = Bundle.main.object(forInfoDictionaryKey: plist_buildVersion) as? String {
            self.buildVersion = buildVersion
        } else {
            self.buildVersion = ""
        }

        guard let configuration = Bundle.main.object(forInfoDictionaryKey: plist_Configuration) as? String else {
            self.serverType = .develop
            self.isRelease = false
            return
        }
        
        self.serverType = configuration == "Release" ? .release : .develop
        self.isRelease = configuration == "Release" ? true : false
        
    }
}

// MARK: - Application Status

extension AppManager {
    
    func getPlist(_ fileName: String) -> [[String: AnyObject]]? {
        var plist: [[String: AnyObject]]? = nil
        guard let path: String = Bundle.main.path(forResource: fileName, ofType: "plist") else {
            Log.message(to: "Fail to load file: \(fileName)")
            return nil
        }
        var format = PropertyListSerialization.PropertyListFormat.xml
        guard let aData: Data = FileManager.default.contents(atPath: path) else {
            Log.message(to: "Fail to load contents: \(path)")
            return nil
        }
        do {
            try plist = PropertyListSerialization.propertyList(from: aData,
                                                                  options: .mutableContainersAndLeaves,
                                                                  format: &format) as? [[String : AnyObject]]
        } catch let error as NSError {
            Log.message(to: "Error property list serialization: \(error)")
        }
        return plist
    }

    // 현재 위치
    func getLocaleCode() -> String {
        return NSLocale.current.languageCode ?? "kr"
    }

    // 현재 위치 타임존
    func getTimeZone() -> String {
        return TimeZone.current.identifier
    }
    
    // 특정 값을 삭제
    func removeUserDefaultKey(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    // 모든 값을 삭제
    func removeAllUserDefault() {
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
    }

    // 특정 값을 추가 - (위에 정의안된 값을 추가 함, 정의 안된 값을 추가시 isAdd값이 Yes로 변경)
    func updateUserDefault(key: String, value: Any) -> Bool {
        self.isAdd = true
        let defaults = UserDefaults.standard
        defaults.setValue(value, forKey: key)
        return defaults.synchronize()
    }
    
    func loadUserDefault(key: String) -> Any? {
        return UserDefaults.standard.value(forKey: key)
    }

    // poc 도메인
    func isPOCDomain() -> Bool {
        return UserDefaults.standard.string(forKey: userDefault_CurrentDomain) != nil ? true : false
    }

    func setPOCDomain(domain: String) {
        UserDefaults.standard.set(domain, forKey: userDefault_CurrentDomain)
    }

    func getPOCDomain() -> String {
        if let domain = UserDefaults.standard.string(forKey: userDefault_CurrentDomain) {
            return domain
        } else {
            return "test"
        }
    }


    // poc 특정 도메인에서 token 처리
    func isAccessToken() -> Bool {
        return UserDefaults.standard.string(forKey: userDefault_AccessToken) != nil ? true : false
    }

    func setAccessToken(token: String) {
        UserDefaults.standard.set(token, forKey: userDefault_AccessToken)
    }

    func getAccessToken() -> String {
        if let token = UserDefaults.standard.string(forKey: userDefault_AccessToken) {
            return token
        } else {
            return ""
        }
    }
    
    func addRecentSearchWord(text: String) {
        let defaults = UserDefaults.standard
        var recentSearchWords = defaults.stringArray(forKey: userDefault_RecentSearchWords) ?? [String]()
        recentSearchWords.insert(text, at: 0)
        var reorder = recentSearchWords.orderedSet
        if reorder.count > 5 { reorder.removeLast() }
        defaults.set(reorder, forKey: userDefault_RecentSearchWords)
    }
    
    func getRecentSearchWordList() -> Array<String>{
        let defaults = UserDefaults.standard
        let recentSearchWords = defaults.stringArray(forKey: userDefault_RecentSearchWords) ?? [String]()
        return recentSearchWords
    }
    
    func removeRecentSearchWordAt(_ index:Int) {
        let defaults = UserDefaults.standard
        var recentSearchWords = defaults.stringArray(forKey: userDefault_RecentSearchWords) ?? [String]()
        if recentSearchWords.count > 0 { recentSearchWords.remove(at: index) }
        defaults.set(recentSearchWords, forKey: userDefault_RecentSearchWords)
    }
    
    func removeRecentSearchWordAll() {
        let defaults = UserDefaults.standard
        var recentSearchWords = defaults.stringArray(forKey: userDefault_RecentSearchWords) ?? [String]()
        recentSearchWords.removeAll()
        defaults.set(recentSearchWords, forKey: userDefault_RecentSearchWords)
    }

}

extension RangeReplaceableCollection where Element: Hashable {
    var orderedSet: Self {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
    mutating func removeDuplicates() {
        var set = Set<Element>()
        removeAll { !set.insert($0).inserted }
    }
}
