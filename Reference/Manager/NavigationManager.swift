//
//  NavigationManager.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/16.
//

import Foundation


class NavigationManager {
    
    class func navigationToVodViewController(type: StreamType = .vod,
                                             birdType: ContentBirdType = .normal,
                                             content: ContentModel? = nil,
                                             list: [ContentModel]? = nil) {
        switch type {
        case .vod:
            let vod = VodViewListController()
            vod.item = content
            vod.contentList = list
            if let topVC = UIApplication.topViewController() {
                topVC.navigationController?.pushViewController(vod, animated: true)
            }
            break
        case .live:
            let live = LiveViewController()
            live.birdType = birdType
            live.item = content
            live.contentList = list
            if let topVC = UIApplication.topViewController() {
                topVC.navigationController?.pushViewController(live, animated: true)
            }
            break
        case .landscape:
            let landscape = LandscapeViewController()
            //landscape.item = content
            //landscape.contentList = list
            if let topVC = UIApplication.topViewController() {
                topVC.navigationController?.pushViewController(landscape, animated: true)
            }
        break
        default:
            break
        }
    }
}
