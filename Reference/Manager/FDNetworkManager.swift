//
//  FDNetworkManager.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit
import Alamofire

struct AlamofireManager {
    static var shared: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 1000
        let session = Alamofire.Session(configuration: configuration)
        return session
    }()
}

struct AlamofireHeaders {
    static func createHeader() -> HTTPHeaders {
        let token = AppManager.shared.getAccessToken()
        let version = AppManager.shared.version
        let deviceModel = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        
        let headers: HTTPHeaders = [
            "AUTHTOKEN": ""
        ]
        
        return headers
    }
}


class Connectivity {
    class var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}


class FDNetworkManager {
    static var downloadFiles: [Any] = [Any]()
    static var isDownloading: Bool = false
    
    class func responseLog(response: AFDataResponse<Any>) {
        Log.message(to: "Response Log Start ==============================")
        if let request = response.request {
            Log.message(to: "Request: \(request)")
        }
        if let respond = response.response {
            Log.message(to: "Respond: \(respond)")
        }
        if let result = response.value as? [String: Any] {
            Log.message(to: "Result: \(result)")
        }
        Log.message(to: "Response Log:  End ================================= " )
    }
    
    
    class func request(
        method: HTTPMethod,
        url: String,
        param: [String: Any]?,
        completion: @escaping (Data?, Bool, Error?) -> Void
    ) {
        if !Connectivity.isConnectedToInternet {
            Log.message(to: "Yes! internet is not available.")
            let error = NSError(domain: url,
                                code: -1, /* 네트워크 접속 실패 에러코드 -1 정의함. */
                                userInfo: [NSLocalizedDescriptionKey : "Network is not available."])
            completion(nil, false, error as Error)
            return
        }

        activityIndicator(show: true)

        if AlamofireManager.shared.session.configuration.timeoutIntervalForRequest != 1000 {
            let alamofire = Alamofire.Session.default
            alamofire.session.configuration.timeoutIntervalForRequest = 1000
            AlamofireManager.shared = alamofire
        }

        var encodingType: ParameterEncoding

        if method == .put || method == .post {
            encodingType = JSONEncoding.default
        } else {
            encodingType = URLEncoding.queryString
        }
        
        if let p = param { Log.message(to: "param: \(p)") }

        
        AlamofireManager.shared.request(
            url,
            method: method,
            parameters: param,
            encoding: encodingType,
            headers: AlamofireHeaders.createHeader()
        ).validate().responseJSON {
            response in
            self.activityIndicator(show: false)
            
            self.responseLog(response: response)

            var success: Bool
            switch response.result {
            case .success:
                success = true
            case let .failure(error):
                success = false
                switch error as? AFError {
                case let .some(.invalidURL(url)):
                    Log.message(to: "\(url)")
                case let .some(.parameterEncodingFailed(reason)):
                    Log.message(to: "\(reason)")
                case let .some(.multipartEncodingFailed(reason)):
                    Log.message(to: "\(reason)")
                case let .some(.responseValidationFailed(reason)):
                    Log.message(to: "\(reason)")
                case let .some(.responseSerializationFailed(reason)):
                    Log.message(to: "\(reason)")
                case .none:
                    break
                default:
                    break
                }
            }

            if let data = response.data {
                completion(data, success, nil)
            }
        }
    }

    class func refreshRequest(
        method: HTTPMethod,
        url: String,
        param: [String: Any]?,
        completion: @escaping (Data, Bool) -> Void
    ) {
        activityIndicator(show: true)

        if AlamofireManager.shared.session.configuration.timeoutIntervalForRequest != 1000 {
            let alamofire = Alamofire.Session.default
            alamofire.session.configuration.timeoutIntervalForRequest = 1000
            AlamofireManager.shared = alamofire
        }

        var encodingType: ParameterEncoding

        if method == .put || method == .post {
            encodingType = JSONEncoding.default
        } else {
            encodingType = URLEncoding.queryString
        }
        
        if let p = param { Log.message(to: "param: \(p)") }

        AlamofireManager.shared.request(
            url,
            method: method,
            parameters: param,
            encoding: encodingType,
            headers: AlamofireHeaders.createHeader()
        ).validate().responseJSON {
            response in

            self.activityIndicator(show: false)
            self.responseLog(response: response)

            var success: Bool

            switch response.result {
            case .success:
                success = true
            case let .failure(error):
                success = false
                Log.message(to: "error: \(error)")
            }

            if let data = response.data {
                completion(data, success)
            }
        }
    }

        
    class func activityIndicator(show: Bool) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = show
        }
    }
}

// 네트 관련 함수
extension FDNetworkManager {
    // 서버와 통신을 전부다 끊는다.
    class func cancellAll() {
        AlamofireManager.shared.session.getTasksWithCompletionHandler { sessionDataTask, uploadData, downloadData in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }

    // 섹션을 전부다 멈춘다.
    class func stopAllSessions() {
        AlamofireManager.shared.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
        AlamofireManager.shared.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
    }

    // 특정 통신을 캔슬한다. (현재 string으로 하는데, 태그로 해야하나?)
    class func cancel(url: String) {
        AlamofireManager.shared.session.getTasksWithCompletionHandler { sessionDataTask, _, _ in
            sessionDataTask.forEach {
                if $0.originalRequest?.url?.absoluteString == url {
                    $0.cancel()
                }
            }
        }
    }

    class func cancelTask() {
        AlamofireManager.shared.session.getTasksWithCompletionHandler { sessionDataTask, _, _ in
            sessionDataTask.forEach { $0.cancel() }
        }
    }

    class func cancelDownload() {
        AlamofireManager.shared.session.getTasksWithCompletionHandler { _, _, downloadData in
            downloadData.forEach { $0.cancel() }
        }
    }

    class func cancelUpload() {
        AlamofireManager.shared.session.getTasksWithCompletionHandler { _, uploadData, _ in
            uploadData.forEach { $0.cancel() }
        }
    }
}

// swiftlint:enable all
