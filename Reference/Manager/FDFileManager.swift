//
//  FDFileManager.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit


enum FileType: String {
    case fourds = "4ds"
    case none = ""
}


class FDFileManager {

    class func writeImagefile(name: String, image: UIImage?, overWrite: Bool) -> String {
        var ret: String = "ERRORO"
        let count = 0
        guard count != name.count else { return "Invalid params." }
        if image == nil { return "Invalid params." }
                
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if paths.count > 0 {
            let dir = (paths.first! as NSString).appendingPathComponent("fd_image")
            if FileManager.default.fileExists(atPath: dir) == false {
                do {
                    try FileManager.default.createDirectory(
                        atPath: dir, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    Log.message(to: "Error creating directory: \(error.localizedDescription)")
                    return error.localizedDescription
                }
            }
            let filePath = (dir as NSString).appendingPathComponent("\(name).4ds")
            let url = URL.init(fileURLWithPath: filePath)
            do {
                if FileManager.default.fileExists(atPath: filePath) == false ||  overWrite == true {
                    try image?.jpegData(compressionQuality: 1.0)?.write(to: url, options: .atomic)
                } else {
                    Log.message(to: "File exist and over write: \(overWrite ? "true" : "false") \n \(filePath)")
                }
            } catch let error as NSError {
                Log.message(to: "Error creating directory: \(error.localizedDescription)")
                return error.localizedDescription
            }
            ret = name
        }
        return ret
    }
    
    class func loadImageFile(name: String) -> UIImage? {
        var ret: UIImage? = nil
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if paths.count > 0 {
            let base = paths.first
            let pathUrl = URL(fileURLWithPath: base!).appendingPathComponent("fd_image/\(name).4ds")
            ret = UIImage(contentsOfFile: pathUrl.path)
        }
        return ret
    }
    
    @discardableResult
    class func removeImageFile(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if paths.count > 0 {
            let dir = (paths.first! as NSString).appendingPathComponent("fd_image")
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: dir)
                for file in files {
                    if file.hasSuffix(".4ds") {
                        let filePath = (dir as NSString).appendingPathComponent("\(name).4ds")
                        if FileManager.default.fileExists(atPath: filePath) == true {
                            try FileManager.default.removeItem(atPath: filePath)
                        }
                    }
                }
            } catch let error as NSError {
                Log.message(to: "Error creating directory: \(error.localizedDescription)")
                return error.localizedDescription
            }
        }
        return name
    }

    //기본 폴더 document 및 특정 폴더 custom에서 원하는 타입의 파일(들)의 주소를 가지고 온다.
    class func search(folder: String = "", type: FileType) -> [URL]? {
        // Get the document directory url
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if folder != "" {
            documentsUrl.appendPathComponent("\(folder)")
        }
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
            print(directoryContents)
            
            let files = directoryContents.filter{ $0.pathExtension == type.rawValue }
            print("\(type.rawValue) urls:", files)

            // if you want to filter the directory contents you can do like this:
            let fileNames = files.map{ $0.deletingPathExtension().lastPathComponent }
            print("\(type.rawValue) list:", fileNames)
            
            return files

        } catch {
            print(error)
            return nil
        }
    }

    //document에 있는 파일들의 주소를 전부다 가지고 온다.
    class func listFilesFromDocumentsFolder() -> [String]? {
        let fileMngr = FileManager.default;

        // Full path to documents directory
        let docs = fileMngr.urls(for: .documentDirectory, in: .userDomainMask)[0].path

        // List all contents of directory and return as [String] OR nil if failed
        return try? fileMngr.contentsOfDirectory(atPath:docs)
    }
    
    //커스텀 폴더안에 있는 이미지 파일을 가지고 온다
    class func getImage(folderName: String, fileName: String, type: FileType) -> UIImage? {
        
        let fileManager = FileManager.default
        var imagePath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        imagePath.appendPathComponent("\(folderName)")
        imagePath.appendPathComponent("\(fileName)")
        let imgPath = imagePath.path
        if fileManager.fileExists(atPath: imgPath){
            return UIImage(contentsOfFile: imgPath)
        }else{
            print("Image is not Here")
            return nil
        }
                
    }
    
    //커스텀 폴더안에 있는 모든 파일들을 가지고 온다.
    class func getImageList(folderName: String, type: FileType) -> [UIImage] {
        
        var images: [UIImage] = []
        let fileManager = FileManager.default
        var imagePath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        imagePath.appendPathComponent("\(folderName)")

        let directoryContents = try! fileManager.contentsOfDirectory(at: imagePath, includingPropertiesForKeys: nil)
        for imageURL in directoryContents where imageURL.pathExtension == type.rawValue {
            if let image = UIImage(contentsOfFile: imageURL.path) {
                // now do something with your image
                images.append(image)
            } else {
               fatalError("Can't create image from file \(imageURL)")
            }
        }
        
        return images
    }
    
    //커스텀 폴더안에 폰트관련 파일이 있는지 확인한다.
    class func getFile(path: String) -> URL? {
        
        return nil
    }
    
    //특정 Plist 데이터를 가지고 온다.
    class func getPlistData(word: String) -> String? {
        
        guard let path = Bundle.main.path(forResource: "Search", ofType: "plist")  else {
            return nil
        }
        guard let dic: Dictionary<String, AnyObject> = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> else {
            return nil
        }
        
        let query: String = word.lowercased()
        
        if query.containsWhitespace() {
            let words = query.components(separatedBy: " ")

            for i in 0..<words.count {
                let name = words[i]
                if let key = dic.keys.first(where: { $0.lowercased().contains(name) }), let result = dic[key] {
                    print("key :\(key)")
                    Log.message(to: "word exit")
                    return result as? String
                }
            }
            Log.message(to: "word exit")
            return nil
            
        } else {
            if let key = dic.keys.first(where: { $0.lowercased().contains(query) }), let result = dic[key] {
                Log.message(to: "word exit")
                return result as? String
            } else {
                Log.message(to: "word not exsit")
                return nil
            }
        }
    }    
}



