//
//  SceneDelegate.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/04.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }        
        window = UIWindow(frame: UIScreen.main.bounds)
        setRootViewController()
        window?.makeKeyAndVisible()
        window?.windowScene = windowScene

    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}



enum TabBarType: Int {
    case home = 0
    case sport = 1
    case search = 2
}

extension SceneDelegate {
    func setRootViewController() {
        let tabBarController = UITabBarController()
        tabBarController.tabBar.backgroundColor = .white
        tabBarController.tabBar.isTranslucent = false
        tabBarController.tabBar.tintColor = .colorRGB(212, 0, 16)
        tabBarController.tabBar.unselectedItemTintColor = .black
        let home = UINavigationController(rootViewController: HomeViewController())
        let category = UINavigationController(rootViewController: CategoryViewController())
        let search = UINavigationController(rootViewController: SearchViewController())
        tabBarController.viewControllers = [home, category, search]
        tabBarController.tabBar.setLayerBorder()
                
        let homeItem = UITabBarItem(title: "Home", image: UIImage(named: "icoHomeNor"), tag: 0)
        let categoryItem = UITabBarItem(title: "Category", image: UIImage(named: "icoCategoryNor"), tag: 1)
        let searchItem = UITabBarItem(title: "Search", image: UIImage(named: "icoSearchNor"), tag: 2)

        let selectedColor: UIColor = .colorRGB(211, 25, 32)
        let unselectedColor: UIColor = .colorRGB(51, 51, 51)
        
        homeItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
        homeItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)
        
        homeItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.PoppinsMedium(ofSize: 10.0)], for: .normal)
        homeItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.PoppinsMedium(ofSize: 10.0)], for: .selected)
        homeItem.selectedImage = UIImage(named: "icoHomeSel")
        
        categoryItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.PoppinsMedium(ofSize: 10.0)], for: .normal)
        categoryItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.PoppinsMedium(ofSize: 10.0)], for: .selected)
        categoryItem.selectedImage = UIImage(named: "icoCategorySel")

        searchItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.PoppinsMedium(ofSize: 10.0)], for: .normal)
        searchItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.PoppinsMedium(ofSize: 10.0)], for: .selected)
        searchItem.selectedImage = UIImage(named: "icoSearchSel")

        home.tabBarItem = homeItem
        category.tabBarItem = categoryItem
        search.tabBarItem = searchItem
        
        
        self.window?.rootViewController = tabBarController
    }
    
    func setTabBar(index: Int) {
        if let tabBarController = self.window?.rootViewController as? UITabBarController {
            tabBarController.selectedIndex = index
        }
    }
}
