//
//  Reusable.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import Foundation

public protocol Reusable {
    static var identifier: String { get }
}

public extension Reusable {
    /// By default, use the name of the class as String for its identifier
    static var identifier: String {
        return String(describing: self)
    }
}
