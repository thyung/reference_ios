//
//  FDQueue.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/04.
//

import Foundation

class FDQueue<T>: NSObject {
    private var array: [T] = []
    private let accessQueue = DispatchQueue(label: "FD_EVENT_QUEUE", attributes: .concurrent)
    private var lock = NSLock()
    
    
    var count: Int {
        var count = 0
        accessQueue.sync {
            count = self.array.count
        }
        return count
    }

    func enqueue(_ element: T) {
        accessQueue.async { [weak self] in
            self?.lock.lock()
            self?.array.append(element)
            self?.lock.unlock()
        }
    }
    
    func dequeue() -> T? {
        var element: T? = nil
        accessQueue.sync { [weak self] in
            self?.lock.lock()
            element = self?.array.first
            if let cnt = self?.array.count, cnt > 0 {
                self?.array.remove(at: 0)
            }
            self?.lock.unlock()
        }
        return element
    }
    
    func clear() {
        accessQueue.async { [weak self] in
            self?.lock.lock()
            self?.array.removeAll()
            self?.lock.unlock()
        }
    }
    
    func remove(at: Int){
        accessQueue.async { [weak self] in
            self?.lock.lock()
            self?.array.remove(at: at)
            self?.lock.unlock()
        }
    }
    
    func first() -> T? {
        var element: T?
        accessQueue.sync { [weak self] in
            self?.lock.lock()
            element = self?.array.first
            self?.lock.unlock()
        }
        return element
    }
    
    public subscript(index: Int) -> T {
        set(newValue) {
            self.accessQueue.async() {
                self.array[index] = newValue
            }
        }
        get {
            var element: T!
            self.accessQueue.sync() {
                element = self.array[index]
            }
            return element
        }
    }
}
