//
//  BaseViewController.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/26.
//

import UIKit

class BaseViewController: UIViewController {
    
    var keyboardHeight: CGFloat = 0.0    
    var tabBar: UITabBar? {
        get {
            if let tabBar = self.navigationController?.tabBarController?.tabBar {
                return tabBar
            }
            return nil
        }
        set {}
    }
    
    lazy var blockView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.view.addSubview(view)
        view.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.top.bottom.left.right.equalTo(weakSelf.view)
            }
        }
        return view
    }()
    lazy var indicator: UIActivityIndicatorView = {
        var view = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        view.color = .darkGray
        self.view.addSubview(view)
        view.snp.makeConstraints { [weak self] ctrl in
            if let weakSelf = self {
                ctrl.center.equalTo(weakSelf.view)
            }
        }
        return view
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillClose),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if isMovingFromParent {
            FDNetworkManager.cancellAll()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationItem.largeTitleDisplayMode = .always

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterForeground(noti:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterForeground(noti:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterBackground(noti:)),
                                               name: UIApplication.willResignActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterBackground(noti:)),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(orientationChanged(noti:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        setOrientaion()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    internal func setOrientaion() {
        AppUtility.lockOrientation(.portrait)
        UIDevice.current.setValue(UIDeviceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    // MARK: - Templet Method
    internal func initVariable() {
    }
    
    internal func setupLayout() {
    }
    
    internal func tabBarHidden(_ isHidden: Bool) {
        if let tabBar = self.tabBar {
            tabBar.isHidden = isHidden
            extendedLayoutIncludesOpaqueBars = isHidden
        }
    }
}


extension BaseViewController {
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            var keyboardHeight: CGFloat = 0.0
            if #available(iOS 11.0, *) {
                keyboardHeight = keyboardRectangle.height - view.safeAreaInsets.bottom
            } else {
                // Fallback on earlier versions
                keyboardHeight = keyboardRectangle.height
            }
            self.keyboardHeight = -keyboardHeight
            updateViewConstraints()
        }
    }

    @objc func keyboardWillClose(_ notification: Notification) {
        keyboardHeight = 0.0
        updateViewConstraints()
        view.endEditing(true)
    }

    func dismissController() {
        if let nv = self.navigationController {
            nv.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: Notification
    @objc internal func enterForeground(noti: NSNotification) {
    }
    
    @objc internal func enterBackground(noti: NSNotification) {
    }
    
    @objc internal func orientationChanged(noti: NSNotification) {
    }
    
    public func showMessageAlert(message: String, title: String) {
        let controller = UIAlertController.init(title: title ,
                                                message: message ,
                                                preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) {(action) in
        }
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func showIndicator() {
        DispatchQueue.main.async {
            self.blockView.isHidden = false
            self.indicator.isHidden = false
            self.view.bringSubviewToFront(self.blockView)
            self.view.bringSubviewToFront(self.indicator)
            self.indicator.startAnimating()
        }
    }

    func closeIndicator() {
        DispatchQueue.main.async {
            self.blockView.isHidden = true
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
        }
    }
}

extension BaseViewController: UIGestureRecognizerDelegate {
    @objc func closeView() {
        keyboardHeight = 0.0
        updateViewConstraints()
        view.endEditing(true)
    }
}


