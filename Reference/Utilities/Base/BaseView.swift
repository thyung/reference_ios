//
//  BaseView.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/08.
//

class BaseView: UIView {
    internal func initVariable() {
    }
        
    internal func setupLayout() {
    }
}

// MARK: - Animation
extension UIView {
    func fadeIn(duration: TimeInterval = 0.4) {
        self.isHidden = false
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 0.4) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.isHidden = true
        }
    }
    
    func removeFadeOut(duration: TimeInterval = 0.4) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.removeFromSuperview()
        }
    }
    
    func startRotate() {
        let rotationAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
        rotationAnimation.duration = 1;
        rotationAnimation.isCumulative = true;
        rotationAnimation.repeatCount = .infinity;
        self.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func stopRotate() {
        self.layer.removeAnimation(forKey: "rotationAnimation")
    }
}
