//
//  AlertMessage.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/26.
//

import Foundation


class AlertMessage {
    static let MSG_GAME_DELAYED_TITLE = "Delay of Game"
    static let MSG_GAME_DELAYED_MESSAGE = "The game is being delayed.\nPlease try again later.\n"

    static let MSG_GAME_CANCELED_TITLE = "Cancellation of game"
    static let MSG_GAME_CANCELED_MESSAGE = "The game is canceled.\n\n"

    static let MSG_GAME_SCHEDULED_MESSAGE = "This program hasn\'t aired yet.\nYou can watch once it starts to air."
                                + "\n Please try again later."

    static let MSG_TITLE_NOTI = "Notification"
    static let MSG_TITLE_ERROR = "Error"

    static let MSG_NETWORK_PROBLEM = "There was a problem with the network."
    static let MSG_UNKNOWN_ERROR = "An unknow error has occureed. \n Please try again."
    static let MSG_LOAD_CONTENT_ERROR = "An unknown error has occurred while loading this content. \n Please try again."
    static let MSG_GAME_AIRED_YET = "This match hasn't been yet. \n The match will be aired alive. \n Please wait."
}
