//
//  UIWindow+.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit

extension UIWindow {
    func dismiss() {
        isHidden = true
        windowScene = nil
    }
}
