//
//  UIView+.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit

public typealias Spacer = UIView

public enum AnchorAxis: Int {
    case x
    case y
    case xy
}

extension UIView {
    // MARK: - Frame

    var left: CGFloat {
        get { return frame.minX }
        set { frame.origin.x = newValue }
    }

    var right: CGFloat {
        get { return frame.maxX }
        set { frame.origin.x = newValue - width }
    }

    var top: CGFloat {
        get { return frame.minY }
        set { frame.origin.y = newValue }
    }

    var bottom: CGFloat {
        get { return frame.maxY }
        set { frame.origin.y = newValue - height }
    }

    var width: CGFloat {
        get { return frame.width }
        set { frame.size.width = newValue }
    }

    var height: CGFloat {
        get { return frame.height }
        set { frame.size.height = newValue }
    }

    private var contentWidth: CGFloat {
        return (self as? UIScrollView)?.contentSize.width ?? bounds.width
    }

    private var contentHeight: CGFloat {
        return (self as? UIScrollView)?.contentSize.height ?? bounds.height
    }

}


// MARK: - Builder

extension UIView {
    convenience init(backgroundColor: UIColor = .clear) {
        self.init(frame: .zero)
        self.backgroundColor = backgroundColor
        setLayerBorder()
    }

}

extension UIView {
    @discardableResult
    func subviewsRecursive() -> [UIView] {
        return subviews + subviews.flatMap { $0.subviewsRecursive() }
    }
    
    @discardableResult
    func setShadow(radius: CGFloat, color: UIColor, offset: CGSize, opacity: Float) -> UIView {
        layer.shadowRadius = radius
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
        return self
    }
}


extension UIView {
    
    func x() -> CGFloat {
        return self.frame.origin.x
    }

    func y() -> CGFloat {
        return self.frame.origin.y
    }

    func endX() -> CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    
    func endY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
    //뷰의 위 아래 양옆의 코너를 라운드화 하기
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        
    }
    
    //Debug일시 뷰의 영역을 잡아준다.
    func setLayerBorder(color: UIColor = UIColor.randomColor, width: CGFloat = 0.5) {
        #if DEBUG
        layer.borderWidth = width
        layer.borderColor = color.cgColor
        #endif
    }
}

