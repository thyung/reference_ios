//
//  String+.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/26.
//

import UIKit

extension String {
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }

    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    //스페이스 유무
    func containsWhitespace() -> Bool {
        return rangeOfCharacter(from: .whitespacesAndNewlines) != nil
    }

    //Json to Dictionary
    func dictionaryFromJson() -> [String : String]? {
        var ret: [String : String]? = nil
        guard let data = self.data(using: .utf8) else {
            Log.message(to: "Fail to encoded(\(self))")
            return ret
        }
        
        do {
            ret = try JSONSerialization.jsonObject(with: data, options: .mutableContainers ) as? [String : String]
        } catch let error  {
            Log.message(to: "\(#function) error: \(error)")
        }
        
        return ret
    }
    
    //Json to Array
    func arrayFromJson() -> [ [String : String]?]? {
        var ret: [ [String : String]?]? = nil
        guard let data = self.data(using: .utf8) else {
            Log.message(to: "Fail to encoded(\(self))")
            return ret
        }
        
        do {
            ret = try JSONSerialization.jsonObject(with: data, options: .mutableContainers ) as? [[String : String]?]
        } catch let error  {
            Log.message(to: "\(#function) error: \(error)")
        }
        
        return ret
    }
    
    func stringLength(font: UIFont? = nil, fontSize: CGFloat, height: CGFloat) -> CGFloat {
        var tempFont: UIFont
        if let f = font {
            tempFont = f
        } else {
            tempFont = UIFont.systemFont(ofSize: fontSize)
        }
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 999.0, height: height))
        label.font = tempFont
        label.text = self

        label.sizeToFit()
        return label.frame.size.width
    }
    
    // convert String to Date
    func date(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: self)
        return date
    }

    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }

    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
    
    // convert String to Image
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return UIImage(data: data)
        }
        return nil
    }

    func getDate(format: String, timeZone: String? = nil) -> String {
        let formatter = DateFormatter()
        guard let date: Date = formatter.date(from: self) else {
            return "Invalid Date"
        }
        
        if let tz = timeZone {
            formatter.timeZone = TimeZone(identifier: tz)
        }
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        formatter.dateFormat = format
        
        return formatter.string(from: date)
    }
    
    // get height
    func height(width: CGFloat,
                font: UIFont = UIFont.systemFont(ofSize: 16.0)) -> CGFloat {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.text = self
            label.font = font
            label.sizeToFit()

            return label.frame.height
        }
    
}
