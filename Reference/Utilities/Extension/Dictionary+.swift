//
//  Dictionary+.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import Foundation

extension Dictionary {
    var queryString: String? {
        return self.reduce("") { "\($0!)\($1.0)=\($1.1)&" }
    }
    
    func contains(key: Key) -> Bool {
      self.index(forKey: key) != nil
    }
}
