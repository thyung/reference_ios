//
//  Int+.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/04.
//

import Foundation


extension Int {
    
    func getDuration() -> String{
        let sign = self.signum()
        //let seconds = abs(self)/1000
        let seconds = abs(self)
        var ms = NSMutableString.init(capacity: 8)
        let h = seconds / 3600
        let m = seconds / 60 % 60
        let s = seconds % 60
        
        if seconds < 0 {
            ms = "∞"
        } else {
            if sign < 0 { ms = "- " }
            if h > 0 { ms.appendFormat("%d:", h) }
            if m < 10 { ms.append("0") }
            ms.appendFormat("%d:", m)
            if s < 10 { ms.append("0") }
            ms.appendFormat("%d", s)
        }
        return ms as String
    }
    
}
