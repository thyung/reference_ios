//
//  UILabel+.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/09.
//

import Foundation


extension UILabel {
    // 앞뒤에 이미지를 추가한다.
    // - Parameters:
    // - text: 추가 텍스트
    // - image: 추가이미지
    // - imageBehindText: text 앞뒤 이미지 여부
    // - keepPreviousText: 기존 text를 유지 여부
    func addTextWithImage(
        text: String,
        image: UIImage,
        imageBehindText: Bool,
        keepPreviousText: Bool
    ) {
        let lAttachment = NSTextAttachment()
        lAttachment.image = image

        // 1pt = 1.32px
        let lFontSize = round(font.pointSize * 1)
        let lRatio = image.size.width / image.size.height

        lAttachment.bounds = CGRect(
            x: 10,
            y: ((font.capHeight - lFontSize) / 2).rounded(),
            width: lRatio * lFontSize,
            height: lFontSize
        )

        let lAttachmentString = NSAttributedString(attachment: lAttachment)
        if imageBehindText {
            let lStrLabelText: NSMutableAttributedString

            if keepPreviousText, let lCurrentAttributedString = attributedText {
                lStrLabelText = NSMutableAttributedString(attributedString: lCurrentAttributedString)
                lStrLabelText.append(NSMutableAttributedString(string: text))
            } else {
                lStrLabelText = NSMutableAttributedString(string: text)
            }
            lStrLabelText.append(lAttachmentString)
            attributedText = lStrLabelText
        } else {
            let lStrLabelText: NSMutableAttributedString

            if keepPreviousText, let lCurrentAttributedString = attributedText {
                lStrLabelText = NSMutableAttributedString(attributedString: lCurrentAttributedString)
                lStrLabelText.append(NSMutableAttributedString(attributedString: lAttachmentString))
                lStrLabelText.append(NSMutableAttributedString(string: "  " + text))
            } else {
                lStrLabelText = NSMutableAttributedString(attributedString: lAttachmentString)
                lStrLabelText.append(NSMutableAttributedString(string: "  " + text))
            }

            attributedText = lStrLabelText
        }
    }

    /// image 삭제
    func removeImage() {
        let text = self.text
        attributedText = nil
        self.text = text
    }
    
    // Pass value for any one of both parameters and see result
       func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

           guard let labelText = self.text else { return }

           let paragraphStyle = NSMutableParagraphStyle()
           paragraphStyle.lineSpacing = lineSpacing
           paragraphStyle.lineHeightMultiple = lineHeightMultiple

           let attributedString:NSMutableAttributedString
           if let labelattributedText = self.attributedText {
               attributedString = NSMutableAttributedString(attributedString: labelattributedText)
           } else {
               attributedString = NSMutableAttributedString(string: labelText)
           }

           // Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

           self.attributedText = attributedString
       }
}
