//
//  UIFont+.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/10.
//

import UIKit


//openSans
extension UIFont {
        
    class func OpenSans(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Regular", size: size)!
    }
    
    class func OpenSansBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Bold", size: size)!
    }
    
    class func OpenSansExtraBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-ExtraBold", size: size)!
    }

    class func OpenSansSemiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-SemiBold", size: size)!
    }

    class func OpenSansLight(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Light", size: size)!
    }

    class func OpenSansItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Italic", size: size)!
    }

    class func OpenSansLightItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-LightItalic", size: size)!
    }

    class func OpenSansSemiBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-SemiBoldItalic", size: size)!
    }

    class func OpenSansExtraBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-ExtraBoldItalic", size: size)!
    }

    class func OpenSansBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-BoldItalic", size: size)!
    }

}

//popins
extension UIFont {
        
    class func Poppins(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Regular", size: size)!
    }
    
    class func PoppinsMedium(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Medium", size: size)!
    }
    
    class func PoppinsMediumItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-MediumItalic", size: size)!
    }

    class func PoppinsSemiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: size)!
    }

    class func PoppinsSemiBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-SemiBoldItalic", size: size)!
    }

    class func PoppinsThin(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Thin", size: size)!
    }

    class func PoppinsThinItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-ThinItalic", size: size)!
    }

    class func PoppinsBlack(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Black", size: size)!
    }

    class func PoppinsBlackItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-BlackItalic", size: size)!
    }

    class func PoppinsBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Bold", size: size)!
    }
    
    class func PoppinsBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-BoldItalic", size: size)!
    }

    class func PoppinsExtraBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-ExtraBold", size: size)!
    }

    class func PoppinsExtraBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-ExtraBoldItalic", size: size)!
    }

    class func PoppinsExtraLight(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-ExtraLight", size: size)!
    }

    class func PoppinsExtraLightItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-ExtraLightItalic", size: size)!
    }

    class func PoppinsItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Italic", size: size)!
    }

    class func PoppinsLight(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Light", size: size)!
    }

    class func PoppinsLightItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-LightItalic", size: size)!
    }
    
}
