//
//  UrlList.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct DevelopURL {
    static let url = "https://olympics-tokyo.kiosk.4dreplay.io/v4/"
    static let imageUrl = "https://olympics-tokyo.kiosk.4dreplay.io"
    static var lsUrl = ""
}

struct RelaseURL {
    static let url = "https://olympics-tokyo.kiosk.4dreplay.io/v4/"
    static let imageUrl = "https://olympics-tokyo.kiosk.4dreplay.io"
    static var lsUrl = ""
}

//URL List
struct ServiceUrl {
    static let getCategryList: String = "category"                      //카테고리 리스트
    static let getContentList: String = "content"                       //컨텐츠 리스트
    static let getRtspUrl: String = "service/4dss/url"                  //4DSS URL 요청
    static let getSectionInfo: String = "section"                  //섹션 요청
}

class UrlList {
    class func url(url:String) -> String {
        let domain: String
        domain = AppManager.shared.isRelease == true ? RelaseURL.url : DevelopURL.url
        return "\(domain)\(url)"
    }
    
    class func imageUrl(path: String) -> String {
        let domain: String
        domain = AppManager.shared.isRelease == true ? RelaseURL.imageUrl : DevelopURL.imageUrl
        return "\(domain)\(path)"
    }
    
    class func lsUrl(url: String) -> String {
        let domain: String
        domain = AppManager.shared.isRelease == true ? RelaseURL.lsUrl : DevelopURL.lsUrl
        return "\(domain)\(url)"
    }
}



