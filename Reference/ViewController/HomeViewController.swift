//
//  HomeViewController.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/07.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: BaseViewController {
    
    let blank: BlankView = BlankView(type: .networkErr)
    var barIndex: Int = 0
    let bar: CollectionBarView = CollectionBarView()
    let swap: UICollectionView = UICollectionView(frame: .zero,
                                                  collectionViewLayout: UICollectionViewFlowLayout.init())
    private(set) var disposeBag = DisposeBag()

    let listItem: BehaviorRelay<[SwapModel]> = BehaviorRelay(value: [])
    var swapList: SwapModel? {
        didSet {
            guard let swap = self.swapList else {
                return
            }
            
            var list: [SwapModel] = []
            
            for i in listItem.value {
                if let _ = i.id {
                    list.append(i)
                }
            }
            list.append(swap)
            
            let barCount = bar.categoryList?.count ?? 0
            var max = barCount - list.count
            if max < 0 { max = 0 }
            for _ in 0..<max {
                list.append(SwapModel(id: nil, list: nil))
            }
            self.listItem.accept(list)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        let logo:UIImageView = UIImageView()
        logo.image = UIImage(named: "imgBrandCi")
        self.view.addSubview(logo)
        logo.setLayerBorder()
        
        logo.snp.makeConstraints { view -> Void in
            view.left.equalTo(self.view).offset(16.0)
            view.top.equalTo(self.view).offset(10.0 + 44.0)
            view.width.equalTo(136.0)
            view.height.equalTo(20.0)
        }
        
        self.view.addSubview(bar)
        bar.setLayerBorder()
        
        bar.snp.makeConstraints { view -> Void in
            view.top.equalTo(logo.snp.bottom).offset(18.0)
            view.left.right.equalTo(self.view)
            view.height.equalTo(102.0)
        }
        
        if let layout = swap.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0.0
            layout.minimumInteritemSpacing = 0.0
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        swap.bounces = false
        swap.register(cellType: TableCollectionView.self)
        swap.alwaysBounceVertical = false
        swap.alwaysBounceHorizontal = false
        swap.isScrollEnabled = true
        swap.showsVerticalScrollIndicator = false
        swap.showsHorizontalScrollIndicator = false
        swap.isPagingEnabled = true
        swap.rx.setDelegate(self).disposed(by: self.disposeBag)
        swap.backgroundColor = .colorRGB(240, 240, 240)
        self.view.addSubview(swap)
        swap.setLayerBorder()
        
        
        swap.snp.makeConstraints { view -> Void in
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
            view.top.equalTo(bar.snp.bottom)
        }
        
        self.view.addSubview(blank)
        blank.isHidden = true
                
        blank.snp.makeConstraints { view -> Void in
            view.top.equalTo(logo.snp.bottom).offset(18.0)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
        }
        setDataSource()
        setRXFunction()
        requestData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func requestData() {
        showIndicator()
        FDNetworkManager.getCategoryList { success, categories, error in
            if success, let model = categories {
                self.blank.isHidden = true
                self.swap.isHidden = false
                self.bar.isHidden = false
                
                self.bar.categoryList = model.categories
                if model.categories.count > 0 {
                    FDNetworkManager.getSectionList(id: model.categories[0].id) { success, sections, error in
                        if success, let m = sections {
                            let swap = SwapModel(id: model.categories[0].id, list: m)
                            self.swapList = swap
                        }
                    }
                }
            } else {
                if let err = error as NSError?, err.code == -1 {
                    self.blank.isHidden = false
                    Log.message(to: "Network Fail")
                }
                self.swap.isHidden = true
                self.bar.isHidden = true                
            }
            self.closeIndicator()
        }
    }
}

extension HomeViewController {
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: swap.rx.items) { [weak self] (collectionView, row, element) in
                guard let `self` = self else { return UICollectionViewCell() }
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.swap.dequeueReusableCell(for: indexPath, cellType: TableCollectionView.self)
                //category id와 관련된 array 가지고 온다.
                let id = self.bar.categoryList![indexPath.row].id
                if let dic: SwapModel = self.listItem.value.first(where: { $0.id == id }) {
                    if let list = dic.list {
                        cell.contentList = list.sections
                    }
                } else {
                    cell.contentList = nil
                }
                
                cell.setLayerBorder(color: .randomColor, width: 1)
                return cell
            }.disposed(by: self.disposeBag)
    }
    
    private func setRXFunction() {
        bar.collectionView.rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                self.barViewAction(indexPath: indexPath)
            }).disposed(by: self.disposeBag)
        
        swap.rx
            .didEndDecelerating
            .subscribe { finish in
                Log.message(to: "end collection scrolling")
                self.swapCollectionViewAction()
            }.disposed(by: self.disposeBag)
        
        swap.rx.contentOffset.subscribe { contentOffSet in
            if let offSet: CGPoint = contentOffSet.element {
                Log.message(to: offSet)
            }
        }.disposed(by: self.disposeBag)

        blank.btRetry.rx.tap.subscribe { [weak self] event in
            self?.requestData()
        }.disposed(by: self.disposeBag)
    }
    
    private func barViewAction(indexPath: IndexPath) {
        guard let categoryList = self.bar.categoryList else {
            return
        }
        let list = self.listItem.value
        self.barIndex = indexPath.row
        let model = categoryList[indexPath.row]
        var check: Bool = false

        for i in list {
            let swap: SwapModel = i
            if let id = swap.id {
                if id.contains(model.id) {
                    check = true
                }
            }
        }
        
        DispatchQueue.main.async {
            self.bar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }

        if check {
            self.swap.reloadData()
            //카테고리에 관련되게 콜랙션 뷰의 위치를 이동한다.
            let point = CGPoint(x: UIScreen.width * CGFloat(self.barIndex), y: 0)
            self.swap.setContentOffset(point, animated: true)
        } else {
            FDNetworkManager.getSectionList(id: model.id) { success, sections, error in
                if success, let m = sections {
                    let swap = SwapModel(id: model.id, list: m)
                    self.swapList = swap                    
                    DispatchQueue.main.async {
                        let point = CGPoint(x: UIScreen.width * CGFloat(self.barIndex), y: 0)
                        self.swap.setContentOffset(point, animated: true)
                    }

                }
            }
        }
    }
    
    //손가락으로 화면 이동할때
    private func swapCollectionViewAction() {
        guard let categoryList = self.bar.categoryList else {
            return
        }
        let list = self.listItem.value
        let index: Int = Int(self.swap.contentOffset.x/UIScreen.width)
        self.barIndex = index
        
        let model = categoryList[index]
        var check: Bool = false
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.barIndex, section: 0)
            self.bar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
        
        for i in list {
            let swap: SwapModel = i
            if let id = swap.id {
                if id.contains(model.id) {
                    check = true
                }
            }
        }
        
        if check {
            self.swap.reloadData()
        } else {
            FDNetworkManager.getSectionList(id: model.id) { success, sections, error in
                if success, let m = sections {
                    let swap = SwapModel(id: model.id,
                                         list: m)
                    self.swapList = swap
                }
            }
        }
        
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.view.height - 150.0 - 44.0
        return CGSize(width: UIScreen.width, height: height)
    }
}
