//
//  SearchViewController.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

enum DeleteType: Int {
    case one = 1
    case multi = 2
    case all = 0
}

class SearchViewController: BaseViewController {
    
    let blank: BlankView = BlankView(type: .notFound)
    let networkError: BlankView = BlankView(type: .networkErr)
    let searchBar: UISearchBar = UISearchBar()
    
    lazy var keyword: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = true
        tableView.delaysContentTouches = false
        tableView.isMultipleTouchEnabled = false
        tableView.register(cellType: SearchWordTableViewCell.self)
        return tableView
    }()
    lazy var content: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = true
        tableView.register(cellType: ListTableViewCell.self)
        return tableView
    }()
        
    let contentListItem: BehaviorRelay<[ContentModel]> = BehaviorRelay(value: [])
    var contentList: [ContentModel]? {
        didSet {
            guard let list = self.contentList else {
                return
            }
            
            self.contentListItem.accept(list)
            self.content.reloadData()
        }
    }
    
    var wordListItems = PublishSubject<[SearchWordModel]>()
    private(set) var disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        searchBar.searchTextField.attributedPlaceholder
            = NSAttributedString(string: "Search with title, contents",
                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorRGBA(0, 0, 0, 0.2)])
        searchBar.barTintColor = .white
        searchBar.searchTextField.backgroundColor = .white
        searchBar.delegate = self
        searchBar.searchTextField.textColor = .black
        searchBar.backgroundImage = UIImage()
        searchBar.searchTextField.setLayerBorder()
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        UISearchBar.appearance().setImage(UIImage(named: "icoInputClear"), for: .clear, state: .normal)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .colorRGB(247, 0, 0)
        self.view.addSubview(searchBar)
        
        searchBar.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.view).offset(40.0)
            view.left.equalTo(self.view).offset(6.0)
            view.right.equalTo(self.view).offset(-80.0)
            view.height.equalTo(43.0)
        }

        let searchButton: UIButton = UIButton()
        searchButton.setTitle("SEARCH", for: .normal)
        searchButton.titleLabel?.font = UIFont.PoppinsBold(ofSize: 14.0)
        searchButton.setTitleColor(UIColor.colorRGB(98, 98, 98), for: .normal)
        searchButton.setTitleColor(UIColor.colorRGB(211, 25, 32), for: .selected)
        searchButton.setTitleColor(UIColor.colorRGBA(211, 25, 32, 0.8), for: .highlighted)
        searchButton.titleLabel?.textAlignment = .center
        searchButton.tag = 777
        self.view.addSubview(searchButton)
        searchButton.setLayerBorder()
        
        searchButton.snp.makeConstraints { view -> Void in
            view.width.equalTo(89.0)
            view.right.equalTo(self.view)
            view.top.equalTo(searchBar.snp.top)
            view.bottom.equalTo(searchBar.snp.bottom)
        }
        
        searchButton
            .rx.tap
            .subscribe { [weak self] event in
                Log.message(to: "Search Button Clicked")
                guard let `self` = self else { return }
                guard let text = self.searchBar.searchTextField.text else { return }
                if !text.isEmpty {
                    searchButton.isEnabled = false
                    
                    AppManager.shared.addRecentSearchWord(text: text)
                    let recentList = AppManager.shared.getRecentSearchWordList()
                    let list = [SearchWordModel(sectionType: .history, items: recentList),
                                SearchWordModel(sectionType: .recommend, items: ["Soccer","Baseball","basketball","brazil"])]
                    self.wordListItems.onNext(list)

                    self.keyword.isHidden = !self.keyword.isHidden
                    self.content.isHidden = !self.content.isHidden
                    self.closeView()
                    
                    if let id: String = FDFileManager.getPlistData(word: text) {
                        self.requestData(id)
                    } else {
                        self.content.isHidden = true
                        self.blank.isHidden = false
                        self.networkError.isHidden = true
                    }
                }
            }.disposed(by: self.disposeBag)

        
        let line: UIView = UIView()
        line.backgroundColor = UIColor.colorRGB(230, 230, 230)
        self.view.addSubview(line)
        
        line.snp.makeConstraints { view -> Void in
            view.top.equalTo(searchBar.snp.bottom).offset(3.0)
            view.left.right.equalTo(self.view)
            view.height.equalTo(1.0)
        }
        
        self.view.addSubview(blank)
        blank.isHidden = true
                
        blank.snp.makeConstraints { view -> Void in
            view.top.equalTo(line.snp.bottom)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
        }
        
        self.view.addSubview(networkError)
        networkError.isHidden = true
        networkError.snp.makeConstraints { view -> Void in
            view.top.equalTo(line.snp.bottom)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
        }
        
        keyword.backgroundColor = .colorRGB(240, 240, 240)
        keyword.rx.setDelegate(self).disposed(by: self.disposeBag)
        self.view.addSubview(keyword)
        keyword.setLayerBorder()
        
        keyword.snp.makeConstraints { view -> Void in
            view.top.equalTo(line.snp.bottom)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
        }

        content.backgroundColor = .colorRGB(240, 240, 240)
        content.rx.setDelegate(self).disposed(by: self.disposeBag)
        content.isHidden = true
        self.view.addSubview(content)
        content.setLayerBorder()
        
        content.snp.makeConstraints { view -> Void in
            view.top.left.right.bottom.equalTo(keyword)
        }
                
        setDataSource()
        setRXFunction()
    }
        
    private func setDataSource() {
                
        let recentList = AppManager.shared.getRecentSearchWordList()
        var list: [SearchWordModel] = []
        if recentList.count > 0 {
            list.append(SearchWordModel(sectionType: .history, items: recentList))
        }
        list.append(SearchWordModel(sectionType: .recommend, items: ["Soccer","Baseball","basketball","brazil"]))
        
        
        let dataSource = RxTableViewSectionedReloadDataSource<SearchWordModel>(
            configureCell: { (_, tv, indexPath, element) in
                
                let cell = self.keyword.dequeueReusableCell(for: indexPath,
                                                              cellType: SearchWordTableViewCell.self)
                cell.selectionStyle = .gray
                cell.isUserInteractionEnabled = true
                
                let recentList = AppManager.shared.getRecentSearchWordList()
                if recentList.count > 0 {
                    cell.close.isHidden = indexPath.section == 0 ? false : true
                    cell.glass.image = indexPath.section == 0 ? #imageLiteral(resourceName: "icoHistory.png") : #imageLiteral(resourceName: "icoSearch.png")
                } else {
                    cell.close.isHidden = true
                    cell.glass.image = #imageLiteral(resourceName: "icoSearch.png")
                }
                                                
                cell.close.rx
                    .tap.asDriver()
                    .drive(onNext: { [weak self] in
                        self?.reorder(type: .one, row: indexPath.row)
                    }).disposed(by: cell.disposeBag)
                
                cell.keyword.text = element
                return cell

            },
            titleForHeaderInSection: { dataSource, sectionIndex in                
                return dataSource[sectionIndex].self.sectionType.rawValue
            }
        )
                
        wordListItems
        .bind(to: keyword.rx.items(dataSource: dataSource))
            .disposed(by: self.disposeBag)
        wordListItems.onNext(list)


        contentListItem.asObservable()
            .bind(to: content.rx.items) { (tableView, row, element) in
                let indexPath = IndexPath(item: row, section: 0)
//                guard let list = self.contentList else {
//                    return UITableViewCell()
//                }
                let cell = self.content.dequeueReusableCell(for: indexPath,
                                                              cellType: ListTableViewCell.self)
                cell.selectionStyle = .gray
                let dic: ContentModel = self.contentListItem.value[indexPath.row]
                cell.dic = dic
                return cell
            }
            .disposed(by: self.disposeBag)
        
    }

    private func setRXFunction() {
        
        searchBar
            .rx.searchButtonClicked
            .subscribe { [weak self] event in
                guard let `self` = self else { return }
                guard let text = self.searchBar.searchTextField.text else { return }
                
                AppManager.shared.addRecentSearchWord(text: text)
                
                let recentList = AppManager.shared.getRecentSearchWordList()
                let list = [SearchWordModel(sectionType: .history, items: recentList),
                            SearchWordModel(sectionType: .recommend, items: ["Soccer","Baseball","basketball","brazil"])]
                self.wordListItems.onNext(list)

                self.keyword.isHidden = !self.keyword.isHidden
                self.content.isHidden = !self.content.isHidden
                self.closeView()
                
                if let id: String = FDFileManager.getPlistData(word: text) {
                    self.requestData(id)
                } else {
                    self.content.isHidden = true
                    self.blank.isHidden = false
                    self.networkError.isHidden = true
                }
            }
            .disposed(by: self.disposeBag)
        
        keyword
            .rx.itemSelected
            .subscribe(onNext: {[weak self] indexPath in
                guard let `self` = self else { return }
                guard let cell = self.keyword.cellForRow(at: indexPath) as? SearchWordTableViewCell else {
                    return
                }
                self.searchBar.searchTextField.text = cell.keyword.text
                self.keyword.deselectRow(at: indexPath, animated: false)
                if let text = cell.keyword.text {
                    AppManager.shared.addRecentSearchWord(text: text)
                    let recentList = AppManager.shared.getRecentSearchWordList()
                    let list = [SearchWordModel(sectionType: .history, items: recentList),
                                SearchWordModel(sectionType: .recommend, items: ["Soccer","Baseball","basketball","brazil"])]
                    self.wordListItems.onNext(list)
                }
                
                self.keyword.isHidden = true
                self.content.isHidden = false
                self.closeView()
                
                if let id: String = FDFileManager.getPlistData(word: cell.keyword.text!) {
                    self.requestData(id)
                }
                else {
                    self.content.isHidden = true
                    self.blank.isHidden = false
                    self.networkError.isHidden = true
                }
        })
            .disposed(by: self.disposeBag)
        
        content.rx
            .itemSelected
            .subscribe(onNext:{ indexPath in
                    //your code
                let list = self.contentListItem.value
                let model: ContentModel = list[indexPath.row]
                NavigationManager.navigationToVodViewController(type: .vod, content: model, list: list)
            }).disposed(by: self.disposeBag)
        
        networkError.btRetry.rx.tap.subscribe { [weak self] event in
            guard let text = self?.searchBar.searchTextField.text else { return }
            if let id: String = FDFileManager.getPlistData(word: text) {
                self?.requestData(id)
            }
        }.disposed(by: self.disposeBag)
    }
    
    private func requestData(_ id: String) {
        let model = Content.List.Request(category_id: id)
        FDNetworkManager.getContentList(dic: model) { success, model, error in
            if success, let m = model {
                if m.contents.count > 0 {
                    self.contentList = m.contents
                    self.blank.isHidden = true
                    self.content.isHidden = false
                } else {
                    self.blank.isHidden = false
                    self.content.isHidden = true
                }
                self.networkError.isHidden = true
            } else {
                if let err = error as NSError?, err.code == -1 {
                    self.networkError.isHidden = false
                    Log.message(to: "Network Fail")
                }
                self.content.isHidden = true
            }
        }
    }
    
    private func reorder(type: DeleteType, row: Int = -1) {
        
        let manager = AppManager.shared
        
        var list: [SearchWordModel] = []
        switch type {
        case .all:
            manager.removeRecentSearchWordAll()
            list.append(SearchWordModel(sectionType: .recommend, items: ["Soccer","Baseball","basketball","brazil"]))
            break
        case .one:
            manager.removeRecentSearchWordAt(row)
            let recentList = AppManager.shared.getRecentSearchWordList()
            if recentList.count > 0 {
                list.append(SearchWordModel(sectionType: .history, items: recentList))
            }
            list.append(SearchWordModel(sectionType: .recommend, items: ["Soccer","Baseball","basketball","brazil"]))
            break
            
        case .multi:
            break
        }
        
        self.wordListItems.onNext(list)
        
    }

}

extension SearchViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch tableView {
        case keyword:
            if AppManager.shared.getRecentSearchWordList().count > 0 {
                return 2
            } else {
                return 1
            }
        default:
            
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView()
        let label: UILabel = UILabel()
        let button: UIButton = UIButton()
        
        switch tableView {
        case keyword:
            
            if AppManager.shared.getRecentSearchWordList().count > 0 {
                switch section {
                case 0:
                    label.text = "HISTORY"
                default:
                    label.text = "RECOMMENDED"
                    break
                }
                
            } else {
                label.text = "RECOMMENDED"
            }
            
            label.font = UIFont.PoppinsBold(ofSize: 16.0)
            label.textColor = .black
            break
        default:
            label.text = "\(self.contentListItem.value.count) results found"
            label.font = UIFont.OpenSansSemiBold(ofSize: 14.0)
            label.textColor = .colorRGBA(0, 0, 0, 0.45)
            break
        }
        
        view.addSubview(label)
        label.setLayerBorder()
        label.snp.makeConstraints { v -> Void in
            switch tableView {
            case keyword:
                v.top.equalTo(view).offset(17.0)
                v.height.equalTo(23.0)
                v.bottom.equalTo(view).offset(-4.0)
                break
            default:
                v.top.equalTo(view).offset(20.5)
                v.bottom.equalTo(view).offset(-12.0)
                break
            }
            v.left.equalTo(view).offset(16.0)
            v.right.equalTo(view).offset(-107.0)
            
        }
        
        if section == 0  && label.text == "HISTORY" {
            button.isHidden = false
        } else {
            button.isHidden = true
        }
        
        button.setImage(UIImage(named: "icoBin"), for: .normal)
        button.setTitle("Delete All", for: .normal)
        button.setTitleColor(.colorRGB(98, 98, 98), for: .normal)
        button.titleLabel?.font = .PoppinsMedium(ofSize: 12.0)
        view.addSubview(button)
        button.setLayerBorder()
        button.snp.makeConstraints { v -> Void in
            v.top.equalTo(view).offset(16.0)
            v.right.equalTo(view)
            v.width.equalTo(107.0)
            v.height.equalTo(31.0)
        }
        
        button.rx
            .tap.asDriver()
            .drive(onNext: { [weak self] in
                let actions = [UIAlertAction(title: "No", style: .cancel, handler: nil),
                               UIAlertAction(title: "Yes", style: .default, handler: { action in
                                self?.reorder(type: .all)
                               })]
                self?.alertView(title: "Confirmation",
                                message: "Are you sure you want to delete your search history?",
                                action: actions)
            }).disposed(by: self.disposeBag)

        
        
        view.setLayerBorder()
        
        
        
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch tableView {
        case keyword:
            return 47.0
        default:
            return 51.5
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchButton = self.view.viewWithTag(777) as? UIButton

        if searchText.isEmpty {
            searchButton?.isSelected = false
            Log.message(to: "Empty Text")
            keyword.isHidden = false
            content.isHidden = true
            blank.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
        } else {
            searchButton?.isSelected = true
//            searchButton?.titleColor(for: .selected)
        }
    }
}




