//
//  VodViewController.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/16.
//

import Foundation
import RxCocoa
import RxSwift

enum PlayerType: Int {
    case rtps
    case hls
}

class VodViewListController: BaseViewController {
    
    private var navi = CotentTitleView(type: .subTitle)
    
    private lazy var playerView: ControlPlayerView = {
        let view = ControlPlayerView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var hlsPlayerView: HLSPlayerView = {
        let view = HLSPlayerView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let playerContainerView: UIView = UIView()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelectionDuringEditing = true
        tableView.allowsSelection = true
        tableView.register(cellType: ListTableViewCell.self)
        return tableView
    }()
    private var distanceX: CGFloat = 0.0
    private var distanceY: CGFloat = 0.0
    private var panRecognizer: UIPanGestureRecognizer!

    private(set) var disposeBag = DisposeBag()
    var item: ContentModel?
    let listItem: BehaviorRelay<[ContentModel]> = BehaviorRelay(value: [])
    var contentList: [ContentModel]? {
        didSet {
            guard let content = self.contentList else {
                return
            }
            
            self.listItem.accept(content)
            
            if let data = self.item, let i = content.firstIndex(where: { $0.id == data.id }) {
                self.setData(info: data)
//                openStreaming(data)
                let indexPath = IndexPath(row: i, section: 0)
                if let _ = self.tableView.dataSource {
                    DispatchQueue.main.async {
                        self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setComponent()
        setAutolayOut()
        setDataSource()
        setRXFunction()
    }
}

extension VodViewListController {
    
    private func setData(info: ContentModel) {
        navi.lbTitle.text = info.title
//        navi.lbSubTitle.text = info.description
//        playerView.setStreamTitle(info.title ?? "")
//        playerView.setStreamDescription(info.description ?? "")
//        if let group: GroupModel = info.group_info?.groups.first,
//           group.channel_count < 2 {
//            playerView.isInteractive = false
//        }
    }
    
    private func setComponent() {
        self.view.addSubview(navi)        
//        self.view.addSubview(hlsPlayerView)
//        hlsPlayerView.setLayerBorder()
        
        self.view.addSubview(playerContainerView)
        playerContainerView.setLayerBorder()

        self.view.addSubview(tableView)
        tableView.rx.setDelegate(self).disposed(by: self.disposeBag)
        tableView.setLayerBorder()
        
//        panRecognizer = UIPanGestureRecognizer.init(target: self,
//                                                    action: #selector(playerPanGestureHandler(_: )))
//        panRecognizer.delegate = self
//        self.playerView.addGestureRecognizer(panRecognizer)
    }
    
    private func playerView(type: PlayerType) {
        
    }
    
    private func setAutolayOut() {
        navi.snp.makeConstraints { view in
            view.left.right.equalTo(self.view)
            view.height.equalTo(52.0)
            view.top.equalTo(self.view).offset(44.0)
        }
                
        playerContainerView.snp.makeConstraints { view in
            let height = UIScreen.width * (9.0/16.0)
            view.left.right.equalTo(self.view)
            view.height.equalTo(height)
            view.top.equalTo(navi.snp.bottom).offset(0.0)
        }
        
        tableView.snp.makeConstraints { view in
            view.top.equalTo(playerContainerView.snp.bottom)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
        }
    }
    
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: tableView.rx.items){ [weak self] (tableView, row, element) in
                guard let `self` = self else { return UITableViewCell() }
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.tableView.dequeueReusableCell(for: indexPath,
                                                              cellType: ListTableViewCell.self)
                let dic: ContentModel = self.listItem.value[indexPath.row]
                cell.dic = dic
                return cell
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setRXFunction() {
        navi.btPrvious
            .rx.tap
            .subscribe { [weak self] event in
//                self?.playerView.streamClose()
                self?.dismissController()
            }
            .disposed(by: self.disposeBag)

//        playerView.progressView.btLandscape
//            .rx.tap.subscribe { [weak self] event in
//                self?.onRotate()
//            }
//            .disposed(by: self.disposeBag)
        
        tableView.rx
            .itemSelected
            .subscribe(onNext:{ [weak self] indexPath in
                //your code
                let model = self?.listItem.value[indexPath.row]
                self?.item = model
                self?.setData(info: model!)
//                self?.playerView.streamClose()
//                self?.openStreaming(model!)
                                
                if let cell = self?.tableView.cellForRow(at: indexPath) as? ListTableViewCell {
                    cell.backgroundColor = .colorRGB(239, 239, 239)
                    cell.play.isHidden = false
                }
                
                DispatchQueue.main.async {
                    self?.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
                }
            }).disposed(by: self.disposeBag)
        
        tableView.rx
            .itemDeselected
            .subscribe(onNext:{ [weak self] indexPath in
            if let cell = self?.tableView.cellForRow(at: indexPath) as? ListTableViewCell {
                cell.backgroundColor = .white
                cell.play.isHidden = true
            }
        }).disposed(by: self.disposeBag)
                        
        if let data = self.item, let list = self.contentList, let i = list.firstIndex(where: { $0.id == data.id }) {
            let indexPath = IndexPath(row: i, section: 0)
            DispatchQueue.main.async {
                
                if let cell = self.tableView.cellForRow(at: indexPath) as? ListTableViewCell {
                    cell.backgroundColor = UIColor.colorRGB(239, 239, 239)
                    cell.play.isHidden = false
                }

                self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)                
            }
        }
    }
}

extension VodViewListController {
    private func onRotate() {
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .unknown, .portrait, .portraitUpsideDown, .faceUp, .faceDown:
            AppUtility.lockOrientation(.landscapeRight)
            let value = UIDeviceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            orientaionMode(true)
            break
        case .landscapeLeft, .landscapeRight:
            AppUtility.lockOrientation(.portrait)
            let value = UIDeviceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            orientaionMode(false)
            break
        default:
            break
        }
    }
    
    private func orientaionMode(_ isLandscape: Bool) {
        updateConstraint(isLandscape)
        navi.isHidden = isLandscape
        tableView.isHidden = isLandscape
//        playerView.isLandScape = isLandscape
//        playerView.progressView.btLandscape.isSelected = isLandscape
        panRecognizer.isEnabled = !isLandscape
        self.view.backgroundColor = isLandscape ? .black : .white
    }
    
    private func updateConstraint(_ isLandscape: Bool) {
        
        navi.snp.updateConstraints { view in
            let height = isLandscape ? 0.0 : UIScreen.width * (80.0/390.0)
            let topOffset = isLandscape ? 0.0 : 44.0
            view.left.right.equalTo(self.view)
            view.height.equalTo(height)
            view.top.equalTo(self.view).offset(topOffset)
        }
        
//        playerView.snp.updateConstraints { view in
//            let height = isLandscape ? UIScreen.height : UIScreen.width * (9.0/16.0)
//            let width = UIScreen.width
//            let margin = isLandscape ? ((width - (height * 16.0 / 9.0)) / 2) : 0.0
//            view.top.equalTo(navi.snp.bottom).offset(0.0)
//            view.left.equalTo(self.view).offset(margin)
//            view.right.equalTo(self.view).offset(-margin)
//            view.height.equalTo(height)
//        }
        
//        playerView.fdPlayer.playerView.snp.updateConstraints { view in
//            var height = UIScreen.height
//            var width = (height * 16.0 / 9.0)
//            if isLandscape != true {
//                width = UIScreen.width
//                height = width * (9.0/16.0)
//            }
//            view.width.equalTo(width)
//            view.height.equalTo(height)
//        }
    }
}

extension VodViewListController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let list = self.listItem.value
        return list.count > 0 ? 1 : 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView()
        let label: UILabel = UILabel()
        label.text = "RECOMMEND"
        label.font = UIFont.PoppinsBold(ofSize: 18.0)
        label.textColor = .black
        view.addSubview(label)
        label.setLayerBorder()
        label.snp.makeConstraints { v -> Void in
            v.top.equalTo(view).offset(14.0)
            v.height.equalTo(25.0)
            v.bottom.equalTo(view).offset(-14.0)
            v.left.equalTo(view).offset(15.0)
            v.right.equalTo(view).offset(-15.0)
        }
        
        view.setLayerBorder()
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 61.0
    }
}

extension VodViewListController {
    
    private func openStreaming(_ content: ContentModel) {
        
        if let group: GroupModel = content.group_info?.groups.first {
            if group.channel_count > 1 {
                if let strUrl = content.stream_url,
                   let aUrl = URL(string: strUrl),
                   let scheme = aUrl.scheme {
                    if scheme.contains("rtsp") {
                        if let group: GroupModel = content.group_info?.groups.first {
                            self.playerView.openStream(strUrl, interactive: group.channel_count < 2 ? false: true )
                        }
                    } else {
                        if let group: GroupModel = content.group_info?.groups.first {
                            self.requestStreamUrl(strUrl, interactive: group.channel_count < 2 ? false: true )
                        }
                    }
                }
            } else {
                if let str = content.hls_url {
                    //https://test-hls-dist.4dreplay.io/hls_replay/00000101-Basketball/
                    if let url = URL(string: "\(str)000/master.m3u8") {
                        hlsPlayerView.url = url
                    }
                }
            }
        }
    }
    
    private func requestStreamUrl(_ address: String, interactive: Bool = true) {
        DevelopURL.lsUrl = address
        let request = FourDss.Url.Request.init(type:"", id: "")
        FDNetworkManager.getRtspUrl(dic: request) { isSuccess,  response in
            if let url = response?.value {
                self.playerView.openStream(url, interactive: interactive)
            }
        }
    }
    
    @objc func playerPanGestureHandler(_ recognizer: UIPanGestureRecognizer) {
        let distance = recognizer.translation(in: self.view)
        switch recognizer.state {
        case .changed:
            recognizer.cancelsTouchesInView = false
            let valueX = abs(distance.x)
            let valueY = abs(distance.y)
            if valueX > valueY {
//                playerView.panGestureSwipe(distance: distance)
            }
            break
        case .ended:
            distanceX = 0.0;
            distanceY = 0.0;
            break
        default:
            break
        }
    }
    
    override func enterForeground(noti: NSNotification) {
//        playerView.play()
    }
    
    override internal func enterBackground(noti: NSNotification) {
//        playerView.pause()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if let view = touch.view {
//            return !view.isDescendant(of: playerView.progressView)
//        }
        return true
    }
}
