//
//  LandscapeViewController.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/22.
//

import RxCocoa
import RxSwift

class LandscapeViewController: BaseViewController {
    private(set) var disposeBag = DisposeBag()
    var playerView = ControlPlayerView()
    var btClose = UIButton()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarHidden(true)
        initVariable()
        setupLayout()
        setRXFunction()
    }
    
    override func initVariable() {
        self.view.backgroundColor = .black
            
        playerView.backgroundColor = .black
        playerView.type = .vod
        playerView.isLandScape = true
        playerView.btRepeat.isHidden = true
        playerView.progressView.btLandscape.isHidden = true
        self.view.addSubview(playerView)
        playerView.setLayerBorder()
        
        let image = UIImage(named: "icon_close")
        btClose.setImage(image, for: .normal)
        self.view.addSubview(btClose)
    }
    
    override func setOrientaion() {
        AppUtility.lockOrientation(.landscapeRight)
        UIDevice.current.setValue(UIDeviceOrientation.landscapeRight.rawValue, forKey: "orientation")
    }
    
    override func setupLayout() {
        playerView.snp.makeConstraints { [weak self] ctrl in
            let height = UIScreen.height
            let width = height * (16.0/9.0)
            let margin = (UIScreen.width - width) / 2
            if let view = self?.view {
                ctrl.top.equalTo(view).offset(0.0)
                ctrl.left.equalTo(view).offset(margin)
                ctrl.right.equalTo(view).offset(-margin)
                ctrl.height.equalTo(height)
            }
        }
        
        btClose.snp.makeConstraints { [weak self] btn in
            if let view = self?.view {
                btn.right.equalTo(view).offset(-13)
                btn.top.equalTo(view).offset(13)
                btn.width.height.equalTo(44)
            }
        }
    }
    
    func setRXFunction() {
        let btn = playerView.progressView.btRepeat
        btn.rx.tap
            .scan(false) { [weak self] (lastState, newValue) in
                self?.playerView.fdPlayer.isLoop = lastState
                return !lastState
            }
            .bind(to: btn.rx.isSelected)
            .disposed(by: disposeBag)
        
        btClose.rx.tap.bind { [weak self] in
            AppUtility.lockOrientation(.portrait)
            let value = UIDeviceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            self?.playerView.streamClose()
            self?.dismissController()
        }.disposed(by: self.disposeBag)
    }
}
