//
//  CategoryViewController.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/08.
//

import UIKit
import RxSwift
import RxCocoa


class CategoryViewController: BaseViewController {
    
    let blank: BlankView = BlankView(type: .networkErr)
    let block: UICollectionView = UICollectionView(frame: .zero,
                                                  collectionViewLayout: UICollectionViewFlowLayout.init())
    private(set) var disposeBag = DisposeBag()

    let listItem: BehaviorRelay<[CategoryModel]> = BehaviorRelay(value: [])
    var contentList: [CategoryModel]? {
        didSet {
            guard var content = self.contentList else {
                return
            }
            content.removeFirst()
            self.listItem.accept(content)
            self.block.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
                
        let title: UILabel = UILabel()
        title.text = "Category"
    
        title.font = UIFont.PoppinsBold(ofSize: 20.0)
        title.textColor = .black
        
        self.view.addSubview(title)
        title.setLayerBorder()
        
        title.snp.makeConstraints { view -> Void in
            view.top.equalTo(self.view).offset(44.0 + 8.5)
            view.left.equalTo(self.view).offset(16.0)
            view.right.equalTo(self.view).offset(-16.0)
            view.height.equalTo(28.0)
        }
        
        if let layout = block.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = 4.0
            layout.minimumInteritemSpacing = 0.0
        }
                
        block.bounces = false
        block.register(cellType: BlockCollectionViewCell.self)
        block.alwaysBounceVertical = false
        block.alwaysBounceHorizontal = false
        block.isScrollEnabled = true
        block.showsVerticalScrollIndicator = false
        block.showsHorizontalScrollIndicator = false
        block.isPagingEnabled = true
        block.rx.setDelegate(self).disposed(by: self.disposeBag)
        block.backgroundColor = .colorRGB(240, 240, 240)
        self.view.addSubview(block)
        block.setLayerBorder()
        setDataSource()
        
        block.snp.makeConstraints { view -> Void in
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
            view.top.equalTo(title.snp.bottom).offset(13.5)
        }
        
        self.view.addSubview(blank)
        blank.isHidden = true
                
        blank.snp.makeConstraints { view -> Void in
            view.top.equalTo(title.snp.bottom).offset(13.5)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
        }
        
        block.rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }                                                            
                let vc = ListViewController()
                let dic: CategoryModel = self.listItem.value[indexPath.row]
                vc.titleName = dic.name
                vc.id = dic.id                
                self.navigationController?.pushViewController(vc, animated: true)
            }).disposed(by: self.disposeBag)
        
        blank.btRetry.rx.tap.subscribe { [weak self] event in
            self?.requestData()
        }.disposed(by: self.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestData()
    }
    
    func requestData() {
        showIndicator()
        FDNetworkManager.getCategoryList { success, categories, error in
            self.blank.isHidden = true
            self.block.isHidden = false
            if success, let model = categories {
                self.contentList = model.categories
            } else {
                if let err = error as NSError?, err.code == -1 {
                    self.blank.isHidden = false
                    Log.message(to: "Network Fail")
                }
                self.block.isHidden = true
            }
            self.closeIndicator()
        }
    }
}

extension CategoryViewController {
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: block.rx.items) { (collectionView, row, element) in
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.block.dequeueReusableCell(for: indexPath,
                                                               cellType: BlockCollectionViewCell.self)
                let dic: CategoryModel = self.listItem.value[indexPath.row]
                cell.name.text = dic.name
                cell.setLayerBorder(color: .randomColor, width: 1)
                return cell
                                    
            }.disposed(by: self.disposeBag)
        
    }
}

extension CategoryViewController: UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let list = self.listItem.value
        return list.count
    }
}

extension CategoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 16, bottom: 20, right: 16)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.width - 32.0 - 8.0)/3
        return CGSize(width: width, height: 65.0)
    }
}
