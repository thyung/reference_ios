//
//  LiveViewController.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/08.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Nuke

// TODO: 네트워크 접속 실패시 처리
class LiveViewController: BaseViewController {
    
    let disposeBag: DisposeBag = DisposeBag()
    
    private var navi = CotentTitleView(type: .subTitle)
    private var playerView = LivePlayerView()
    lazy var middleView: BirdContainView = {
        let view = BirdContainView(type: birdType)
        return view
    }()
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelectionDuringEditing = true
        tableView.allowsSelection = true
        tableView.rx.setDelegate(self).disposed(by: self.disposeBag)
        tableView.register(cellType: ListTableViewCell.self)
        return tableView
    }()
    private var tableCotain = UIView()
    private var isRequest = false
    private var timer: Timer?
    private var tableToConstraintToMiddle: Constraint!
    private var tableToConstraintToPlayer: Constraint!
    private var distanceX: CGFloat = 0.0
    private var distanceY: CGFloat = 0.0
    private var panRecognizer: UIPanGestureRecognizer!
    
    var birdType: ContentBirdType = .normal
    var item: ContentModel?
    let listItem: BehaviorRelay<[ContentModel]> = BehaviorRelay(value: [])
    var contentList: [ContentModel]? {
        didSet {
            guard let content = self.contentList else {
                return
            }
            
            self.listItem.accept(content)
            if let data = self.item {
                self.setData(info: data)
                openStreaming(data)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //tabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarHidden(true)
        initVariable()
        setupLayout()
        setDataSource()
        setRXFunction()
        setRXResponse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func initVariable() {
        self.view.addSubview(navi)
        /* player view */
        playerView.setLayerBorder()
        playerView.backgroundColor = .lightGray
        playerView.birdType = birdType
        self.view.addSubview(playerView)
                
        /* bird, mulit */
        middleView.setLayerBorder()
        middleView.backgroundColor = .white
        self.view.addSubview(middleView)
        
        /* table */
        tableCotain.backgroundColor = .white
        tableCotain.addSubview(tableView)
        self.view.addSubview(tableCotain)
        tableView.setLayerBorder()
        
        panRecognizer = UIPanGestureRecognizer.init(target: self,
                                                    action: #selector(playerPanGestureHandler(_: )))
        panRecognizer.delegate = self
        self.playerView.addGestureRecognizer(panRecognizer)
    }
    
    override func setupLayout() {
        /* top */
        navi.snp.makeConstraints { view in
            let height = UIScreen.width * (80.0/390.0)
            view.left.right.equalTo(self.view)
            view.height.equalTo(height)
            view.top.equalTo(self.view).offset(44.0)
        }
        /* player view */
        playerView.snp.makeConstraints { view in
            let height = UIScreen.width * (9.0/16.0)
            view.left.right.equalTo(self.view)
            view.height.equalTo(height)
            view.top.equalTo(navi.snp.bottom).offset(0.0)
        }
        middleView.snp.makeConstraints { view in
            let height = UIScreen.width * (9.0/16.0) + 56
            view.left.right.equalTo(self.view)
            view.height.equalTo(height)
            view.top.equalTo(playerView.snp.bottom).offset(0.0)
        }
        /* table */
        tableCotain.snp.makeConstraints { view in
            tableToConstraintToMiddle = view.top.equalTo(middleView.snp.bottom).constraint
            tableToConstraintToPlayer = view.top.equalTo(playerView.snp.bottom).constraint
            view.left.right.bottom.equalTo(self.view)
        }
        tableToConstraintToMiddle.deactivate()
        //tableToConstraintToPlayer.deactivate()

        tableView.snp.makeConstraints { view in
            view.top.left.right.bottom.equalTo(tableCotain)
        }
    }
    
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: tableView.rx.items){ [weak self] (tableView, row, element) in
                guard let `self` = self else { return UITableViewCell() }
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.tableView.dequeueReusableCell(for: indexPath, cellType: ListTableViewCell.self)
                cell.selectionStyle = .none
                let dic: ContentModel = self.listItem.value[indexPath.row]
                cell.dic = dic
                return cell
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setRXFunction() {
        navi.btPrvious
            .rx.tap
            .subscribe { [weak self] event in
                self?.tabBarHidden(false)
                self?.playerView.streamClose()
                self?.dismissController()
            }
            .disposed(by: self.disposeBag)
        
        playerView.progressView.btLandscape
            .rx.tap.subscribe { [weak self] event in
                self?.onRotate()
            }
            .disposed(by: self.disposeBag)
        
        tableView.rx.itemSelected
          .subscribe(onNext: { [weak self] indexPath in
            if let list = self?.listItem.value {
                let model: ContentModel = list[indexPath.row]
                NavigationManager.navigationToVodViewController(type: .landscape, content: model)
            }
          }).disposed(by: self.disposeBag)
    }
    
    func onRotate() {
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .unknown, .portrait, .portraitUpsideDown, .faceUp, .faceDown:
            AppUtility.lockOrientation(.landscapeRight)
            let value = UIDeviceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            orientaionMode(true)
            break
        case .landscapeLeft, .landscapeRight:
            AppUtility.lockOrientation(.portrait)
            let value = UIDeviceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            orientaionMode(false)
            break
        default:
            break
        }
    }
    
    private func orientaionMode(_ isLandscape: Bool) {
        updateConstraint(isLandscape)
        navi.isHidden = isLandscape
        middleView.isHidden = isLandscape
        //tableTitleView.isHidden = isLandscape
        tableView.isHidden = isLandscape
        playerView.isLandScape = isLandscape
        playerView.progressView.btLandscape.isSelected = isLandscape
        panRecognizer.isEnabled = !isLandscape
        self.view.backgroundColor = isLandscape ? .black : .white
    }
    
    private func updateConstraint(_ isLandscape: Bool) {
        navi.snp.updateConstraints { [weak self] view in
            let height = isLandscape ? 0.0 : UIScreen.width * (80.0/390.0)
            let topOffset = isLandscape ? 0.0 : 44.0
            if let weakSelf = self {
                view.left.right.equalTo(weakSelf.view)
                view.height.equalTo(height)
                view.top.equalTo(weakSelf.view).offset(topOffset)
            }
        }
        /* player */
        playerView.snp.updateConstraints { [weak self] view in
            let height = isLandscape ? UIScreen.height : UIScreen.width * (9.0/16.0)
            let width = UIScreen.width
            let margin = isLandscape ? ((width - (height * 16.0 / 9.0)) / 2) : 0.0
            if let weakSelf = self {
                view.top.equalTo(navi.snp.bottom).offset(0.0)
                view.left.equalTo(weakSelf.view).offset(margin)
                view.right.equalTo(weakSelf.view).offset(-margin)
                view.height.equalTo(height)
            }
        }        
        playerView.fdPlayer.playerView.snp.updateConstraints { view in
            var height = UIScreen.height
            var width = (height * 16.0 / 9.0)
            if isLandscape != true {
                width = UIScreen.width
                height = width * (9.0/16.0)
            }
            view.width.equalTo(width)
            view.height.equalTo(height)
        }
        
//        /* table */
//        tableTitleView.snp.updateConstraints { view in
//            let height = isLandscape ? 0.0 : (UIScreen.width * (60.0/390.0))
//            view.top.left.right.equalTo(tableCotain)
//            view.height.equalTo(height)
//        }
    }
    
    @objc func playerPanGestureHandler(_ recognizer: UIPanGestureRecognizer) {
        let distance = recognizer.translation(in: self.view)
        switch recognizer.state {
        case .changed:
            recognizer.cancelsTouchesInView = false
            let valueX = abs(distance.x)
            let valueY = abs(distance.y)
            if valueX > valueY {
                playerView.panGestureSwipe(distance: distance)
            }
            break
        case .ended:
            distanceX = 0.0;
            distanceY = 0.0;
            break
        default:
            break
        }
    }
    
    private func panGestureHandler(_ gesture: UIPanGestureRecognizer) {
        let location = gesture.translation(in: self.view)
        switch gesture.state {
        case .began:
            break
        case .changed:
            break
        case .ended:
            let absolute = abs(location.y)
            let sign = Int(location.y).signum()
            if absolute > 60 {
                sign > 0 ? dwonSliding() : upSliding()
            } else {
                sign < 0 ? dwonSliding() : upSliding()
            }
            break
        default:
            break
        }
    }
    
    private func upSliding() {
        tableToConstraintToPlayer.activate()
        tableToConstraintToMiddle.deactivate()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    private func dwonSliding() {
        tableToConstraintToMiddle.activate()
        tableToConstraintToPlayer.deactivate()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension LiveViewController {
    override func enterForeground(noti: NSNotification) {
        playerView.playToNow()
    }
    
    override internal func enterBackground(noti: NSNotification) {
        playerView.pause()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view {
            return !view.isDescendant(of: playerView.progressView)
        }
        return true
    }
}

// MARK: - CMS
extension LiveViewController {
    
    private func setData(info: ContentModel) {
        navi.lbTitle.text = info.title
        navi.lbSubTitle.text = info.description
        playerView.setStreamTitle(info.title ?? "")
        playerView.setStreamDescription(info.description ?? "")
        if let startTime = info.started_at {
            let time = Int64(startTime)
            playerView.progressView.atStartTime = time
        }
        
        playerView.progressView.atStartTime = 1625700994 // 1625702994 1622031875
        
        
        if let group: GroupModel = info.group_info?.groups.first {
            if group.channel_count < 2 {
                // TODO: 주석제거할것
                //playerView.isInteractive = false
                //middleView.birdView.isInteractive = false
                                
                playerView.progressView.atStartTime = 1625706148
            }
        }
        // multi, bird 정보에 따른 화면 변경 처리
    }
    
    /*
     rtsp://10.82.16.252/live.4ds?type=live
     rtsp://10.82.5.99/mbc_test03.4ds?type=vod&quality=fhd&target=1
    */
    private func openStreaming(_ content: ContentModel) {
        if let strUrl = content.stream_url,
           let aUrl = URL(string: strUrl),
           let scheme = aUrl.scheme {
            if scheme.contains("rtsp") {
                #if false
                playerView.openStream(strUrl)
                #else
                playerView.fdPlayer.streamOpenStartTS = 2 * 60 * 1000
                playerView.openStream("rtsp://10.82.5.99/mbc_test03.4ds?type=vod&quality=fhd&target=1")
                playerView.type = .live
                #endif
            } else {
                requestStreamUrl(strUrl)
            }
        }
    }
    
    private func requestStreamUrl(_ address: String) {
        DevelopURL.lsUrl = address
        let request = FourDss.Url.Request.init(type:"", id: "")
        FDNetworkManager.getRtspUrl(dic: request) { isSuccess,  response in
            if let url = response?.value {
                self.playerView.openStream(url)
                self.playerView.type = .live
            }
        }
    }
}

// MARK: - Birdview change channel
extension LiveViewController {
    
    private func setRXResponse() {
        playerView.rxChannel
            .scan([]) { (previous, current) in
                    return Array(previous + [current]).suffix(2)
            }
            .asObservable().subscribe { [weak self] value in
                Log.message(to: "\(#function) first: \(String(describing: value.first)),"
                   + "next: \(String(describing: value.last))")
                if let prev = value.first, let next = value.last {
                    /* update camera postion on birdview, from fdplayer channel */
                    let channel = (self?.birdType == .ufc) ? (next - prev) : next                    
                    DispatchQueue.main.async {
                        self?.middleView.birdView.updateCameraPosition(channel)
                        // TODO: 분기 처리 필요
                        //self?.middleView.multiView.changePDStreamChannel(channel)
                    }
                }
            } onError: { error in
                Log.message(to: "\(#function) observe channel \(error)")
            }.disposed(by: self.disposeBag)
        
        switch birdType {
        case .basket:
            basketRXResponse()
            break
        case .baseball:
            baseballRXResponse()
            break
        case .ufc:
            ufcRXResponse()
            break
        case .soccer:
            soccerRXResponse()
            break
        default:
            break
        }
    }
    
    private func baseballRXResponse() {
        let baseballView = middleView.birdView as! BaseballBirdView
        baseballView.rxPosition.asObservable().subscribe { [weak self] value in
            Log.message(to: "\(#function) channel: \(value)")
            if let weakSelf = self {
                weakSelf.playerView.fdPlayer.setPositionChange("\(value)", target: "1")
            }
        }.disposed(by: self.disposeBag)
    }
    
    private func ufcRXResponse() {
        let ufcView = middleView.birdView as! UFCBirdView
        ufcView.wheel.rxIndex.asObservable().subscribe { [weak self] value in
            Log.message(to: "\(#function) index: \(value)")
            // TODO: totoal * index and request channel
            if let weakSelf = self {
                weakSelf.playerView.fdPlayer.setPositionChange("1", target: "\(value)")
            }
        } onError: { error in
            Log.message(to: "\(#function) index: \(error)")
        }.disposed(by: self.disposeBag)
    }
    
    private func basketRXResponse() {
        let basketView = middleView.birdView as! BasketBirdView
        basketView.rxChannel.asObservable().subscribe { [weak self] value in
            Log.message(to: "\(#function) channel: \(value)")
            if let weakSelf = self, let ret = value.element{
                weakSelf.playerView.fdPlayer.setPositionChange("\(ret)", target: "1")
            }
        }.disposed(by: self.disposeBag)
    }
    
    private func soccerRXResponse() {
        let soccerView = middleView.birdView as! SoccerBirdView
        soccerView.rxChannel.asObservable().subscribe { [weak self] value in
            Log.message(to: "\(#function) channel: \(value)")
            if let weakSelf = self, let ret = value.element{
                weakSelf.playerView.fdPlayer.setPositionChange("\(ret)", target: "1")
            }
        }.disposed(by: self.disposeBag)
    }
}

// MARK: - UITable Delegate, DataSource
extension LiveViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView()
        let label: UILabel = UILabel()
        label.text = "RECOMMEND"
        label.font = UIFont.PoppinsBold(ofSize: 16.0)
        label.textColor = .black
        view.backgroundColor = .white
        view.addSubview(label)
        label.setLayerBorder()
        label.snp.makeConstraints { v -> Void in
            v.top.equalTo(view).offset(24.0)
            v.height.equalTo(23.0)
            v.bottom.equalTo(view).offset(-14.0)
            v.left.equalTo(view).offset(15.0)
            v.right.equalTo(view).offset(-15.0)
        }
        
        let panGesture = UIPanGestureRecognizer()
        panGesture.rx.event
            .subscribe(onNext: { [weak self] x in
                self?.panGestureHandler(x)
            })
            .disposed(by: disposeBag)
        view.addGestureRecognizer(panGesture)
        view.setLayerBorder()
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 61.0
    }
}

