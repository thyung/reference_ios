//
//  ListViewController.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/09.
//

import UIKit
import RxSwift
import RxCocoa

// TODO: 네트워크 접속 실패시 처리
class ListViewController: BaseViewController {
    
    let navi: CotentTitleView = CotentTitleView(type: .title)
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .colorRGB(238, 238, 238)
        tableView.allowsSelection = true
        tableView.register(cellType: ListTableViewCell.self)
        return tableView
    }()
    
    private(set) var disposeBag = DisposeBag()
    
    let listItem: BehaviorRelay<[ContentModel]> = BehaviorRelay(value: [])
    var contentList: [ContentModel]? {
        didSet {
            guard let content = self.contentList else {
                return
            }
            
            self.listItem.accept(content)
        }
    }

    var titleName: String?
    var id: String? {
        didSet {
            guard let id = self.id else {
                return
            }
            let model = Content.List.Request(category_id: id)
            FDNetworkManager.getContentList(dic: model) { success, model, error in
                if success, let m = model {
                    self.contentList = m.contents
                }
            }
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        navi.lbTitle.textAlignment = .center
        navi.lbTitle.text = titleName
        self.view.addSubview(navi)
        navi.setLayerBorder()
                
        navi.snp.makeConstraints { view -> Void in
            view.left.right.equalTo(self.view)
            view.top.equalTo(self.view).offset(44.0)
            view.height.equalTo(44.0)
        }
        
        tableView.contentInset.top = 21
        tableView.rx.setDelegate(self).disposed(by: self.disposeBag)
        self.view.addSubview(tableView)
        tableView.setLayerBorder()
        
        tableView.snp.makeConstraints { view -> Void in
            view.top.equalTo(navi.snp.bottom)
            view.left.right.equalTo(self.view)
            view.bottom.equalTo(self.view)
            
        }
        
        setRXFunction()
        setDataSource()
        
    }
        
    private func setRXFunction() {
        navi.btPrvious
            .rx.tap
            .subscribe { [weak self] event in
                self?.navigationController?.popViewController(animated: true)}
            .disposed(by: self.disposeBag)
        
        tableView.rx
            .itemSelected
            .subscribe(onNext:{ indexPath in
                    //your code
                let list = self.listItem.value
                let model: ContentModel = list[indexPath.row]
                NavigationManager.navigationToVodViewController(type: .vod, content: model, list: list)
            }).disposed(by: self.disposeBag)
        
        tableView.rx
            .itemHighlighted
            .subscribe(onNext:{ indexPath in
                if let cell = self.tableView.cellForRow(at: indexPath) as? ListTableViewCell {
                    cell.backgroundColor = .colorRGB(230, 230, 230)
                }
        }).disposed(by: self.disposeBag)

        tableView.rx
            .itemUnhighlighted
            .subscribe(onNext:{ indexPath in
                if let cell = self.tableView.cellForRow(at: indexPath) as? ListTableViewCell {
                    cell.backgroundColor = .clear
                }
        }).disposed(by: self.disposeBag)

    }
    
    private func setDataSource() {
        listItem.asObservable()
            .bind(to: tableView.rx.items) { (tableView, row, element) in
                let indexPath = IndexPath(item: row, section: 0)
                let cell = self.tableView.dequeueReusableCell(for: indexPath,
                                                              cellType: ListTableViewCell.self)
                cell.selectionStyle = .none
                let dic: ContentModel = self.listItem.value[indexPath.row]
                cell.dic = dic
                return cell
            }
            .disposed(by: self.disposeBag)
                
    }
    
}

extension ListViewController: UITableViewDelegate {
}
