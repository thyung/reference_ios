//
//  GroupModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct GroupModel: Codable {
    let id: String
    let name: String
    let description: String?
    let gop: Int?
    let fps: Float?
    let width: Int?
    let height: Int?
    let bitrate: Int?
    let camera_fps: Float?
    let camera_width: Int?
    let camera_height: Int?
    let is_replay: Bool?
    let is_pdview: Bool?
    let is_multiview: Bool?
    let is_interactive: Bool?
    let is_timemachine: Bool?
    let channel_count: Int
    let default_channel_id: String
    let subchannels: [SubchannelModel]
    let extrachannels: [SubchannelModel] //? TODO: 맞나 나중에 확인
}
