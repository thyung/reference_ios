//
//  FourDssModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/21.
//

import Foundation

enum FourDss {
    // MARK: 4DSS Model
    enum Url {
        struct Request: Codable {
            let type: String
            let id: String
        }
        struct Response: Codable {
            let value: String
            let results: String
        }
        struct ViewModel {
            
        }
    }
    
}

