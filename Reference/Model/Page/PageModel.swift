//
//  PageModel.swift
//  Reference
//
//  Created by 4dreplay on 2021/06/21.
//

import Foundation

struct PageModel: Codable {
    let prev: String?
    let next: String?
}
