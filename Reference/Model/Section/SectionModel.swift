//
//  SectionModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/23.
//

import Foundation

struct SectionModel: Codable {
    let id: String
    let name: String
    let layout: String?
    let is_live: Bool?
    let has_live: Bool?
    let contents: [ContentModel]?
}
