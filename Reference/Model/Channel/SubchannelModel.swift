//
//  SubchannelModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct SubchannelModel: Codable {
    let id: String
    let index: String
}
