//
//  AlertViewModel.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit

enum AlertType: Int {
    case titleMessage
    case title
    case titleImage
    case titleMessageImage
}


struct AlertViewModel {
    let type: AlertType
    let title: String
    let message: String
    let buttonTitle: [String]
    let image: String
    let height: CFloat 
    let width: CFloat
}

