//
//  Atomic.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/04.
//

import Foundation

@propertyWrapper
struct Atomic<T> {
    
    private var t: T
    private let lock = NSLock()
    
    init(wrappedValue t: T) {
        self.t = t
    }
    
    var wrappedValue: T {
        get { return load()}
        set { store(new: newValue) }
    }
    
    func load() -> T {
        lock.lock()
        defer { lock.unlock() }
        return t
    }
    
    mutating func store(new: T) {
        lock.lock()
        defer { lock.unlock() }
        t = new
    }
}

