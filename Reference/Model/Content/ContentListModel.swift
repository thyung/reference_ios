//
//  ContentListModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

enum Content {
    // MARK: Content Model
    enum List {
        struct Request: Codable {
            var event_id: String? = nil               //id
            var event_type: String? = nil             //vod or live
            var content_type: String? = nil           //vod or live
            var category_id: String? = nil            //id
            var section_id: String? = nil             //id
            var next: String? = nil
        }
        struct Response: Codable {
            let contents: [ContentModel]
            let page: PageModel
        }
        struct ViewModel {
            
        }
    }
    
}
