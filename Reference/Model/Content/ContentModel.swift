//
//  ContentModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct ContentModel: Codable {
    let id: String?
    let event_id: String?
    let content_type : String?
    let title: String?
    let description: String?
    let duration: Double?
    let hls_url: String?
    let stream_url: String?
    let is_live: Bool
    let thumbnail_url: String?
    let started_at: Double?
    let updated_at: String?
    let registered_at: String?
    let event_status: String?
    let categories: [CategoryModel]?
    let sections: [SectionModel]?
    let group_info: GroupInfoModel?
}

