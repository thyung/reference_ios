//
//  GroupInfoModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct GroupInfoModel: Codable {
    let default_group_id: String
    let group_count: Int?
    let subchannel_group_count: Int?
    let groups: [GroupModel]
}


