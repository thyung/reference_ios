//
//  CategoryModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct CategoryModel: Codable {
    let id: String
    let name: String
    let has_live: Bool?
    let selected_icon_url: String?
    let unselected_icon_url: String?
}
