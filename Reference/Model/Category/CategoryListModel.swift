//
//  CategoryListModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/18.
//

import Foundation

struct CategoryListModel: Codable {
    let categories: [CategoryModel]    
}
