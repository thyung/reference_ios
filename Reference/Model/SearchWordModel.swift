//
//  SearchWordModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/16.
//

import Foundation
import RxDataSources

struct SearchWordModel{
    
    public enum SectionType: String {
        case history = "history"
        case recommend = "recommend"
    }
    
    var sectionType: SectionType
    var items: [String]
}


extension SearchWordModel: SectionModelType {
    
    typealias  Item = String
    
    init(original: SearchWordModel, items: [String]) {
        self = original
        self.items = items
    }
}
