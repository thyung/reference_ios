//
//  PopViewModel.swift
//  poc
//
//  Created by 4Dreplay on 2021/05/27.
//

import UIKit

struct PopUpViewModel {
    let alpha: CGFloat
    let titles: [String]
    let height: CGFloat
    let gap: CGFloat
    let width: CGFloat
    let yCoordinate: CGFloat
}
