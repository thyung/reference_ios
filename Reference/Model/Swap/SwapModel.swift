//
//  SwapModel.swift
//  Reference
//
//  Created by 4Dreplay on 2021/06/23.
//

import Foundation

struct SwapModel: Codable {
    let id: String?
    let list: SectionListModel?
}

